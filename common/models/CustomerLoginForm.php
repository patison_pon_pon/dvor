<?php
namespace common\models;

use Yii;
use yii\base\Model;

/**
 * Login form
 */
class CustomerLoginForm extends Model
{
    public $email;
    public $password;
    public $rememberMe = true;

    private $_user;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['email', 'password'], 'required'],
            // rememberMe must be a boolean value
            ['email', 'email'],
            ['rememberMe', 'boolean'],
            // password is validated by validatePassword()
            ['password', 'validatePassword'],
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getCustomer();
            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, 'Ошибка входа.');
            }
        }
    }

    /**
     * Logs in a Customer using the provided username and password.
     *
     * @return bool whether the Customer is logged in successfully
     */
    public function login()
    {
        if ($this->validate()) {
            return Yii::$app->customer->login($this->getCustomer(), $this->rememberMe ? 3600 * 24 * 30 : 0);
        } else {
            return false;
        }
    }

    /**
     * Finds Customer by [[username]]
     *
     * @return Customer|null
     */
    protected function getCustomer()
    {
        if ($this->_user === null) {
            $this->_user = Customer::findByEmail($this->email);
        }

        return $this->_user;
    }
}
