<?php

namespace common\tables;

use Yii;

/**
 * This is the model class for table "option_value".
 *
 * @property integer $id
 * @property integer $option_id
 * @property string $name
 * @property string $url
 * @property integer $sort
 */
class OptionValue extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'option_value';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['option_id', 'name', 'url'], 'required'],
            [['option_id', 'sort'], 'integer'],
            [['name', 'url'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'option_id' => 'Опция',
            'name' => 'Название',
            'url' => 'Url',
            'sort' => 'Сортировка',
        ];
    }
}
