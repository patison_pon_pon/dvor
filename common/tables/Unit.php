<?php

namespace common\tables;

use Yii;

/**
 * This is the model class for table "unit".
 *
 * @property integer $id
 * @property string $name
 * @property integer $additional_product_name
 * @property integer $additional_product_price
 */
class Unit extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'unit';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['additional_product_price'], 'number'],
            [['name', 'additional_product_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'additional_product_name' => 'Название товара связанного с единицей измерения (напр: Поддон)',
            'additional_product_price' => 'Цена товара связанного с единицей измерения',
        ];
    }
}
