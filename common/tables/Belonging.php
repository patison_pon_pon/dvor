<?php

namespace common\tables;


/**
 * This is the model class for table "belonging".
 *
 * @property integer $id
 * @property string $name
 * @property string $url
 * @property string $meta_h1
 * @property string $meta_t
 * @property string $meta_k
 * @property string $meta_d
 * @property string $description
 */
class Belonging extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'belonging';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'meta_t', 'meta_h1'], 'required'],
            [['name', 'url', 'meta_t', 'meta_k', 'meta_d', 'meta_h1'], 'string', 'max' => 255],
            [['description'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Имя',
            'url' => 'Url (Алиас)',
            'meta_h1' => 'Meta H1',
            'meta_t' => 'Meta Title',
            'meta_k' => 'Meta Keywords',
            'meta_d' => 'Meta Description',
            'description' => 'Описание',
        ];
    }
}
