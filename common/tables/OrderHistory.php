<?php

namespace common\tables;

use Yii;

/**
 * This is the model class for table "order_history".
 *
 * @property integer $id
 * @property integer $order_id
 * @property integer $order_status_id
 * @property integer $comment
 * @property integer $created_at
 */
class OrderHistory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'order_history';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_status_id', 'order_id', 'created_at'], 'required'],
            [['order_status_id', 'order_id', 'created_at'], 'integer'],
            [['comment'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_id' => 'Заказ №',
            'order_status_id' => 'Статус заказа',
            'comment' => 'Комментарий',
            'created_at' => 'Дата',
        ];
    }
}
