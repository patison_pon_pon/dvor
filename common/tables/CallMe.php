<?php

namespace common\tables;

use Yii;

/**
 * This is the model class for table "call_me".
 *
 * @property integer $id
 * @property string $phone
 * @property integer $answered
 * @property integer $created_at
 */
class CallMe extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'call_me';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['phone', 'created_at'], 'required'],
            [['answered', 'created_at'], 'integer'],
            [['phone'], 'string', 'min' => 8, 'max' => 24],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'phone' => 'Телефон',
            'answered' => 'Был ответ?',
            'created_at' => 'Дата',
        ];
    }
}
