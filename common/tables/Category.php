<?php

namespace common\tables;

use Yii;

/**
 * This is the model class for table "category".
 *
 * @property integer $id
 * @property integer $parent_id
 * @property integer $belonging_id
 * @property string $name
 * @property string $meta_h1
 * @property string $meta_t
 * @property string $meta_k
 * @property string $meta_d
 * @property string $description
 * @property integer $status
 * @property string $url
 */
class Category extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parent_id', 'belonging_id', 'status'], 'integer'],
            [['belonging_id', 'name', 'meta_t', 'meta_h1'], 'required'],
            [['name', 'url', 'meta_t', 'meta_k', 'meta_d', 'meta_h1'], 'string', 'max' => 255],
            [['description'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'parent_id' => 'Родитель',
            'belonging_id' => 'Принадлежность',
            'name' => 'Имя',
            'meta_h1' => 'Meta H1',
            'meta_t' => 'Meta Title',
            'meta_k' => 'Meta Keywords',
            'meta_d' => 'Meta Description',
            'description' => 'Описание',
            'status' => 'Статус',
            'url' => 'Url',
        ];
    }
}
