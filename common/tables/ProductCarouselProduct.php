<?php

namespace common\tables;

use Yii;

/**
 * This is the model class for table "product_carousel_product".
 *
 * @property integer $id
 * @property integer $product_carousel_id
 * @property integer $product_id
 * @property integer $sort
 */
class ProductCarouselProduct extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_carousel_product';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_carousel_id', 'product_id', 'sort'], 'required'],
            [['product_carousel_id', 'product_id', 'sort'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_carousel_id' => 'ID Карусели',
            'product_id' => 'Товар',
            'sort' => 'Сортировка',
        ];
    }
}
