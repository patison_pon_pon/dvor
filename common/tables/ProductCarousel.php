<?php

namespace common\tables;

use Yii;

/**
 * This is the model class for table "product_carousel".
 *
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property integer $limit
 * @property integer $item_limit
 * @property integer $created_at
 */
class ProductCarousel extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_carousel';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'description', 'limit', 'item_limit', 'created_at'], 'required'],
            [['limit', 'item_limit', 'created_at'], 'integer'],
            [['name', 'description'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'description' => 'Описание',
            'limit' => 'Лимит товаров',
            'item_limit' => 'Лимит товаров за 1 показ',
            'created_at' => 'Создано',
        ];
    }
}
