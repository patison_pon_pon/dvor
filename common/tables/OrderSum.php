<?php

namespace common\tables;

use Yii;

/**
 * This is the model class for table "order_sum".
 *
 * @property integer $id
 * @property integer $order_id
 * @property string $action
 * @property string $description
 * @property string $value
 * @property integer $created_at
 */
class OrderSum extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'order_sum';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_id', 'action', 'description', 'value', 'created_at'], 'required'],
            [['order_id', 'created_at'], 'integer'],
            [['value'], 'number'],
            [['action', 'description'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_id' => 'Order ID',
            'action' => 'Action',
            'description' => 'Description',
            'value' => 'Value',
            'created_at' => 'Created At',
        ];
    }
}
