<?php

namespace common\tables;

use Yii;

/**
 * This is the model class for table "info".
 *
 * @property integer $id
 * @property string $name
 * @property string $url
 * @property string $meta_t
 * @property string $meta_k
 * @property string $meta_d
 * @property string $description
 * @property integer $created_at
 */
class Info extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'info';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'meta_t', 'description', 'created_at'], 'required'],
            [['description'], 'string'],
            [['created_at'], 'integer'],
            [['name', 'url', 'meta_t', 'meta_k', 'meta_d'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'url' => 'Url (Алиас)',
            'meta_t' => 'Meta Title',
            'meta_k' => 'Meta Keywords',
            'meta_d' => 'Meta Description',
            'description' => 'Полный текст',
            'created_at' => 'Дата',
        ];
    }
}
