<?php

namespace common\tables;

use Yii;

/**
 * This is the model class for table "product_option_value".
 *
 * @property integer $id
 * @property integer $product_id
 * @property integer $product_option_id
 * @property integer $option_id
 * @property integer $option_value_id
 * @property integer $sort
 * @property integer $quantity
 * @property string $price
 */
class ProductOptionValue extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_option_value';
    }

    public function getDescription()
    {
        return $this->hasOne(OptionValue::className(), ['id' => 'option_value_id']);
    }
    public function getName()
    {
        return $this->description->name;
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id', 'product_option_id', 'option_id', 'option_value_id'], 'required'],
            [['product_id', 'product_option_id', 'option_id', 'option_value_id', 'sort', 'quantity'], 'integer'],
            [['price'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_id' => 'product_id',
            'product_option_id' => 'Product Option ID',
            'option_id' => 'Option ID',
            'option_value_id' => 'Option Value ID',
            'sort' => 'Sort',
            'quantity' => 'Quantity',
            'price' => 'Price',
        ];
    }
}
