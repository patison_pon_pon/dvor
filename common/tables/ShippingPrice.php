<?php

namespace common\tables;

use Yii;

/**
 * This is the model class for table "shipping_price".
 *
 * @property integer $id
 * @property integer $supply_id
 * @property integer $shipping_id
 * @property integer $weight_price
 * @property integer $pack_weight
 * @property integer $pack_price
 */
class ShippingPrice extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'shipping_price';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['supply_id', 'shipping_id'], 'required'],
            [['supply_id', 'shipping_id'], 'integer'],
            [['weight_price', 'pack_weight', 'pack_price'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'supply_id' => 'Поставка',
            'shipping_id' => 'Способ доставки',
            'weight_price' => 'Цена за 1 кг',
            'pack_weight' => 'Вес одной упаковки',
            'pack_price' => 'Цена за одну упаковку',
        ];
    }
}
