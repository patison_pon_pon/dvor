<?php

namespace common\tables;


/**
 * This is the model class for table "order".
 *
 * @property integer $id
 * @property integer $customer_id
 * @property integer $unit_id
 * @property string $name
 * @property string $phone
 * @property string $region
 * @property string $city
 * @property string $address
 * @property string $comment
 * @property integer $quantity
 * @property integer $weight
 * @property double $sum
 * @property string $currency_id
 * @property integer $order_status_id
 * @property integer $created_at
 */
class Order extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'order';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['customer_id', 'name', 'city', 'quantity', 'sum', 'currency_id', 'order_status_id', 'created_at'], 'required'],
            [['customer_id', 'quantity', 'order_status_id', 'created_at', 'unit_id', 'currency_id'], 'integer'],
            [['address', 'comment'], 'string'],
            [['sum', 'weight'], 'number'],
            [['name', 'phone', 'region', 'city'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'customer_id' => 'Покупатель',
            'unit_id' => 'Единица измерения',
            'name' => 'ФИО',
            'phone' => 'Телефон',
            'region' => 'Область/Регион',
            'city' => 'Город',
            'address' => 'Адрес',
            'comment' => 'Комментарий',
            'quantity' => 'Количество',
            'weight' => 'Общий вес',
            'sum' => 'Сумма заказа',
            'currency_id' => 'Валюта заказа',
            'order_status_id' => 'Статус заказа',
            'created_at' => 'Создан',
        ];
    }
}
