<?php

namespace common\tables;

use Yii;

/**
 * This is the model class for table "product".
 *
 * @property integer $id
 * @property integer $belonging_id
 * @property integer $category_id
 * @property integer $unit_id
 * @property string $name
 * @property string $url
 * @property string $vendor_code
 * @property string $description
 * @property string $price
 * @property integer $quantity
 * @property integer $minimum
 * @property string $weight
 * @property string $stock
 * @property integer $status
 * @property integer $created_at
 */
class Product extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['belonging_id', 'category_id', 'unit_id', 'name', 'price', 'minimum', 'created_at'], 'required'],
            [['belonging_id', 'category_id', 'unit_id', 'quantity', 'minimum', 'status', 'stock', 'created_at'], 'integer'],
            [['description'], 'string'],
            [['price', 'weight'], 'number'],
            [['name', 'vendor_code'], 'string', 'max' => 255],
            [['url'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'belonging_id' => 'Принадлежность',
            'category_id' => 'Категория',
            'unit_id' => 'Единица измерения',
            'name' => 'Имя',
            'vendor_code' => 'Артикул',
            'description' => 'Описание',
            'price' => 'Цена ($)',
            'quantity' => 'Количество',
            'minimum' => 'Мин. к-во',
            'weight' => 'Вес/обьем (% от ЕИ)',
            'stock' => 'Наличие',
            'status' => 'Статус',
            'created_at' => 'Дата',
            'url' => 'Url',
        ];
    }
}
