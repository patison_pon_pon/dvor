<?php

namespace common\tables;

use Yii;

/**
 * This is the model class for table "customer".
 *
 * @property integer $id
 * @property string $username
 * @property string $phone
 * @property string $business
 * @property string $geo
 * @property string $manager_id
 * @property string $auth_key
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property integer $status
 * @property integer $discount
 * @property integer $created_at
 * @property integer $updated_at
 */
class Customer extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'customer';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username', 'phone', 'manager_id', 'auth_key', 'password_hash', 'email', 'created_at', 'updated_at'], 'required'],
            [['geo'], 'string'],
            [['discount'], 'number'],
            [['manager_id', 'status', 'created_at', 'updated_at'], 'integer'],
            [['username', 'phone', 'business', 'password_hash', 'password_reset_token', 'email'], 'string', 'max' => 255],
            [['auth_key'], 'string', 'max' => 32],
            [['email'], 'unique'],
            [['password_reset_token'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'ФИО',
            'phone' => 'Телефон',
            'business' => 'Товарная группа',
            'geo' => 'Територия бизнеса',
            'manager_id' => 'Менеджер',
            'auth_key' => 'Auth Key',
            'password_hash' => 'Password Hash',
            'password_reset_token' => 'Password Reset Token',
            'email' => 'E-mail',
            'status' => 'Статус',
            'discount' => 'Персональная скидка (%)',
            'created_at' => 'Создан',
            'updated_at' => 'Updated At',
        ];
    }
}
