<?php

namespace common\tables;

use Yii;

/**
 * This is the model class for table "order_spare".
 *
 * @property integer $id
 * @property integer $order_id
 * @property integer $product_id
 * @property integer $quantity
 * @property double $price
 * @property double $sum
 * @property double $options
 */
class OrderProduct extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'order_product';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_id', 'product_id', 'quantity', 'price', 'sum'], 'required'],
            [['order_id', 'product_id', 'quantity'], 'integer'],
            [['price', 'sum'], 'number'],
            [['options'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_id' => 'Заказа № ',
            'product_id' => 'Товар',
            'quantity' => 'Количество',
            'price' => 'Цена',
            'sum' => 'Итого',
            'options' => 'Опции',
        ];
    }
}
