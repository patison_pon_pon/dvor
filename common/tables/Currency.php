<?php

namespace common\tables;

use Yii;

/**
 * This is the model class for table "currency".
 *
 * @property integer $id
 * @property string $name
 * @property string $code
 * @property string $value
 * @property string $symbol
 */
class Currency extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'currency';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'code', 'value', 'symbol'], 'required'],
            [['value'], 'number'],
            [['name'], 'string', 'max' => 255],
            [['code'], 'string', 'max' => 3],
            [['symbol'], 'string', 'max' => 16],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'code' => 'Код',
            'value' => 'Значение',
            'symbol' => 'Символ',
        ];
    }
}
