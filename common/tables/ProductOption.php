<?php

namespace common\tables;

use Yii;

/**
 * This is the model class for table "product_option".
 *
 * @property integer $id
 * @property integer $product_id
 * @property integer $option_id
 * @property integer $parent_product_option_value_id
 * @property integer $sort
 */
class ProductOption extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_option';
    }

    public function getDescription()
    {
        return $this->hasOne(Option::className(), ['id' => 'option_id']);
    }
    public function getName()
    {
        return $this->description->name;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['option_id', 'product_id'], 'required'],
            [['option_id', 'product_id', 'parent_product_option_value_id', 'sort'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_id' => 'product_id',
            'name' => 'Имя',
            'option_id' => 'Option ID',
            'parent_product_option_value_id' => 'parent_product_option_value_id',
            'sort' => 'Sort',
        ];
    }
}
