<?php

/** @var $this \yii\web\View */
/** @var $link string */
/** @var $paramExample string */

?>
<h3>Зарегестрирован новый покупатель</h3>

<p>Имя: <b><?= $this->params['params']->username ?></b></p>

<p>E-mail: <b><?= $this->params['params']->email ?></b></p>

<p>Телефон: <b><?= $this->params['params']->phone ?></b></p>