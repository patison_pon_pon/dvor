<?php

namespace common\components;

use Yii;
use yii\base\Component;

class Mail extends Component
{

    public function MailAdminInform($params, $action, $subject, $from = 'feedback@tosopt.com'){
        $emails = explode(';', Yii::$app->my->settings()['admin_inform_emails']);
        foreach ($emails as $email) {
            Yii::$app->mailer->getView()->params['params'] = $params;
            Yii::$app->mailer->compose([
                'html' => 'views/' . $action . '_html',
                'text' => 'views/' . $action . '_text',
            ])
                ->setFrom($from)
                ->setTo($email)
                ->setSubject($subject) // тема письма
                ->send();
            Yii::$app->mailer->getView()->params = null;
        }
        return true;
    }

    public function MailCustomer($params, $action, $subject, $from = 'feedback@tosopt.com'){
        $emails = explode(';', Yii::$app->my->settings()['admin_inform_emails']);
        foreach ($emails as $email) {
            Yii::$app->mailer->getView()->params['params'] = $params;
            Yii::$app->mailer->compose([
                'html' => 'views/' . $action . '_html',
                'text' => 'views/' . $action . '_text',
            ])
                ->setFrom($from)
                ->setTo($email)
                ->setSubject($subject) // тема письма
                ->send();
            Yii::$app->mailer->getView()->params = null;
        }
        return true;
    }
}