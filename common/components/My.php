<?php

namespace common\components;

use common\tables\Currency;
use frontend\models\catalog\Unit;
use frontend\models\shop\Shipping;
use frontend\models\shop\ShippingPrice;
use Yii;
use yii\base\Component;
use yii\helpers\ArrayHelper;

class My extends Component
{
    public function host(){
        if ($_SERVER['HTTP_HOST'] == 'tosopt-backend'){
            $host = 'http://tosopt';
        }else{
            $host = 'http://tosopt.com';
        }
        return $host;
    }

    public function nf($number, $symbol = false){
        return number_format($number, 2, '.', '&nbsp;')
            . ($symbol
                ? "&nbsp;" . Yii::$app->session->get('currency_symbol')
                : ''
            );
    }

    public function removeDirectory($dir) {
        if ($objs = glob($dir."/*")) {
            foreach($objs as $obj) {
                is_dir($obj) ? $this->removeDirectory($obj) : unlink($obj);
            }
        }
        rmdir($dir);
    }

    public function currencies(){
        $currency = Currency::find()->asArray()->all();
        return $currency;
    }

    public function shipments(){
        $shipping = Shipping::find()->asArray()->all();
        return $shipping;
    }

    public function showDate($date) // $date --> время в формате Unix time
    {
        //$date = date('d.m.Y H:i', $d);
        $stf      = 0;
        $cur_time = time();
        $diff     = $cur_time - $date;

        $seconds = array( 'секунда', 'секунды', 'секунд' );
        $minutes = array( 'минута', 'минуты', 'минут' );
        $hours   = array( 'час', 'часа', 'часов' );
        $days    = array( 'день', 'дня', 'дней' );
        $weeks   = array( 'неделя', 'недели', 'недель' );
        $months  = array( 'месяц', 'месяца', 'месяцев' );
        $years   = array( 'год', 'года', 'лет' );
        $decades = array( 'десятилетие', 'десятилетия', 'десятилетий' );

        $phrase = array( $seconds, $minutes, $hours, $days, $weeks, $months, $years, $decades );
        $length = array( 1, 60, 3600, 86400, 604800, 2630880, 31570560, 315705600 );

        for ( $i = sizeof( $length ) - 1; ( $i >= 0 ) && ( ( $no = $diff / $length[ $i ] ) <= 1 ); $i -- ) {
            ;
        }
        if ( $i < 0 ) {
            $i = 0;
        }
        $_time = $cur_time - ( $diff % $length[ $i ] );
        $no = floor($no);
        $value = sprintf( "%d %s ", $no, $this->getPhrase( $no, $phrase[ $i ] ) );

        if ( ( $stf == 1 ) && ( $i >= 1 ) && ( ( $cur_time - $_time ) > 0 ) ) {
            $value .= time_ago( $_time );
        }

        return $value . ' назад';
    }

    public function getPhrase( $number, $titles ) {
        $cases = array( 2, 0, 1, 1, 1, 2 );

        return $titles[ ( $number % 100 > 4 && $number % 100 < 20 ) ? 2 : $cases[ min( $number % 10, 5 ) ] ];
    }

    public function settings(){
        $s = \common\tables\Settings::find()->asArray()->all();
        $settings = ArrayHelper::map($s, 'key','value');
        return $settings;
    }

    public function short($string, $q){
        $string = strip_tags($string);
        if (strlen($string) > $q) {
            $string = substr($string, 0, $q);
            $string = rtrim($string, "!,.-");
            $string = substr($string, 0, strrpos($string, ' '));
            echo $string."… ";
        }else{
            echo $string;
        }
    }

    public function start($c = false, $s = false){

        if (!$c && Yii::$app->session['currency']){
            echo '';
        }else{
            $session = Yii::$app->session;
            $session->open();
            $cookies = Yii::$app->response->cookies;
            $currency = ($c ? $c : $this->settings()['default_currency']);
            $cookies->remove('currency');
            $cookies->add(new \yii\web\Cookie([
                'name' => 'currency',
                'value' => $currency,
            ]));
            $cur = Currency::findOne($currency);
            $session->set('currency', $currency);
            $session->set('currency_value', $cur->value);
            $session->set('currency_symbol', $cur->symbol);
        }
    }

    public function conversion($p, $symbol = true, $nf = true, $discount = true){

        $price = ($p * Yii::$app->session->get('currency_value'));

        if (Yii::$app->customer->id && $discount){
            $price -= ($price/100)*Yii::$app->customer->identity->discount;
        }

        if ($nf){
            $price = $this->nf($price);
        }

        if ($symbol){
            $price .= "&nbsp;" . Yii::$app->session->get('currency_symbol');
        }

        return $price;
    }

    public function puckPrice($weight, $unit_id){
        $unit = Unit::findOne($unit_id);
        if (isset($unit->additional_product_price) && $unit->additional_product_price){
            $pack['name'] = $unit->additional_product_name;
            $pack['quantity'] = ceil($weight);
            $pack['price'] = (ceil($weight) * $unit->additional_product_price);
        }else
            $pack = 0;

        return $pack;
    }

    public function drawTotals($sum, $weight, $unit_id){

        $pack = $this->puckPrice($weight[$unit_id]['value'], $unit_id);
        if ($pack){
            echo "<tr><td colspan='5' class='text-right text-upper'>На сумму:</td><td><b>"
                . $this->conversion($sum[$unit_id]['value'])
                . "</b></td></tr>";

            echo "<tr>
                <td colspan='5' class='text-right text-upper'>
                    {$pack['name']} (x{$pack['quantity']}):
                </td>
                <td>
                    <b>".$this->conversion($pack['price'])."</b>
                </td>
            </tr>";
            $sum[$unit_id]['value'] += $pack['price'];
        }

        echo "<tr><td colspan='5' class='text-right text-upper'>Итого:</td><td><b>"
            . $this->conversion($sum[$unit_id]['value'])
            . "</b></td></tr>";
    }
}