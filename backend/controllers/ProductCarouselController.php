<?php

namespace backend\controllers;

use backend\models\catalog\Product;
use backend\models\module\ProductCarouselProduct;
use Yii;
use backend\models\module\ProductCarousel;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ProductCarouselController implements the CRUD actions for ProductCarousel model.
 */
class ProductCarouselController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all ProductCarousel models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => ProductCarousel::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreate()
    {
        $model = new ProductCarousel();

        if ($model->load(Yii::$app->request->post())) {
            $model->created_at = date('U');
            $model->save();
            return $this->redirect(['update', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    public function actionFind(){
        if (Yii::$app->request->isAjax) {
            $result = '';
            $products = Product::find()->where(['like', 'name', $_GET['product']])->limit('10')->asArray()->all();
            if (empty($products)){
                $result = "Нет соответствий";
            }else{
                foreach ($products as $product){
                    $result .= Html::button(
                        $product['name'] . ' (' . $product['vendor_code'] . ')',
                        [
                            'class' => 'btn btn-block btn-info',
                            'onclick' => "addProductToCarousel('{$product['id']}', '{$_GET['carousel_id']}')",
                        ]
                    );
                }
            }
            return $result;
        }else
            return 'Нет соответствий';
    }

    public function actionAdd(){
        if (Yii::$app->request->isAjax) {
            $product = new ProductCarouselProduct();
            $product->product_id = $_GET['product_id'];
            $product->product_carousel_id = $_GET['carousel_id'];
            $product->sort = 5;
            $product->save();
            $result = "<script>showProducts('{$_GET['carousel_id']}')</script>";
            return $result;
        }

    }

    public function actionProducts(){
        if (Yii::$app->request->isAjax) {
            $products = ProductCarouselProduct::find()->where(['product_carousel_id' => $_GET['carousel_id']])->orderBy(['sort' => SORT_ASC])->all();
            return $this->renderPartial('products',[
                'products' => $products
            ]);
        }
    }

    public function actionPlus(){
        if (Yii::$app->request->isAjax) {
            $product = ProductCarouselProduct::findOne($_GET['id']);
            $product->updateCounters(['sort' => +1]);
            $result = "<script>showProducts('{$product->product_carousel_id}')</script>";
            return $result;
        }
    }

    public function actionMinus(){
        if (Yii::$app->request->isAjax) {
            $product = ProductCarouselProduct::findOne($_GET['id']);
            $product->updateCounters(['sort' => -1]);
            $result = "<script>showProducts('{$product->product_carousel_id}')</script>";
            return $result;
        }
    }

    public function actionRemove(){
        if (Yii::$app->request->isAjax) {
            $product = ProductCarouselProduct::findOne($_GET['id']);
            $result = "<script>showProducts('{$product->product_carousel_id}')</script>";
            ProductCarouselProduct::deleteAll(['id' => $_GET['id']]);
            return $result;
        }
    }

    /**
     * Updates an existing ProductCarousel model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['update', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing ProductCarousel model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ProductCarousel model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ProductCarousel the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ProductCarousel::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
