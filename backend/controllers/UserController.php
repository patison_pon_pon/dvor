<?php

namespace backend\controllers;

use backend\models\shop\AuthAssignment;
use backend\models\shop\AuthItem;
use backend\models\shop\SignupForm;
use Yii;
use backend\models\shop\User;
use backend\models\shop\UserSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        /*$role = Yii::$app->authManager->createRole('ADMINISTRATOR');
        $role->description = 'Администратор';
        Yii::$app->authManager->add($role);

        $role = Yii::$app->authManager->createRole('MANAGER');
        $role->description = 'Менеджер';
        Yii::$app->authManager->add($role);*/

        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $roles = AuthItem::find()->all();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'roles' => $roles,
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $roles = AuthItem::find()->all();
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                $auth = new AuthAssignment();
                $auth->item_name = $user->role;
                $auth->user_id = $user->id;
                $auth->created_at = date('U');
                if(!$auth->save()){
                    var_dump($auth->errors);
                    die();
                }
                return $this->redirect(['update', 'id' => $user->id]);
            }
        }

        return $this->render('create', [
            'model' => $model,
            'roles' => $roles,
        ]);
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        //$password = $this->findModel($id);
        $roles = AuthItem::find()->all();

        if ($model->load(Yii::$app->request->post())) {
            $model->username = Yii::$app->request->post('User')['username'];
            $model->email = Yii::$app->request->post('User')['email'];
            $model->role = Yii::$app->request->post('User')['role'];
            if(!$model->save()){
                var_dump($model->errors);
                die();
            }
            if (($auth = AuthAssignment::findOne(['user_id' => $id])) !== null)
                $auth->delete();
            $auth = new AuthAssignment();
            $auth->item_name = $model->role;
            $auth->user_id = $id;
            $auth->created_at = date('U');
            $auth->save();
            return $this->redirect(['index']);
        }

        if (Yii::$app->request->post('password')) {
            $model->password_hash = Yii::$app->security->generatePasswordHash(Yii::$app->request->post('password'));
            $model->save();
            return $this->redirect(['index']);
        }

        return $this->render('update', [
            'model' => $model,
            'roles' => $roles,
        ]);
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
