<?php

namespace backend\controllers;

use backend\classes\SortList;
use backend\models\catalog\Belonging;
use Yii;
use backend\models\catalog\Category;
use backend\models\catalog\CategorySearch;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CategoryController implements the CRUD actions for Category model.
 */
class CategoryController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Category models.
     * @return mixed
     */
    public function actionIndex()
    {
        $belongings = Belonging::find()->all();
        $searchModel = new CategorySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->setSort([
            'defaultOrder' => ['name'=>SORT_ASC]
        ]);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'belongings' => $belongings,
        ]);
    }

    public function actionAdd()
    {
        $model = new Category();
        $model->loadDefaultValues();
        $id = Yii::$app->request->get('id');
        $belonging_id = Yii::$app->request->get('belonging_id');
        $model->parent_id = $id;
        $model->belonging_id = $belonging_id;
        $belongings = Belonging::find()->all();

        if ($model->load(Yii::$app->request->post()) && $model->save()){
            return $this->redirect(['index']);
        }else{
            return $this->render('create', [
                'model' => $model,
                'belongings' => $belongings,
            ]);
        }
    }

    /**
     * Creates a new Category model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Category();
        $belongings = Belonging::find()->all();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'belongings' => $belongings,
            ]);
        }
    }

    /**
     * Updates an existing Category model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $categories = $this->getList($model->belonging_id);
        $belongings = Belonging::find()->all();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
                'categories' => $categories,
                'belongings' => $belongings,
            ]);
        }
    }

    public static function getList($belonging_id)
    {
        $data = Category::find()
            ->select(['id', 'parent_id', 'name', 'belonging_id'])
            ->where("belonging_id = '$belonging_id'")
            ->orderBy('parent_id ASC')
            ->asArray()
            ->all();

        $sort = new SortList([
            'data' => $data,
            'prefix' => '------',
        ]);
        $sortList = ArrayHelper::map($sort->getList(), 'id', 'name');
        return $sortList;
    }

    /**
     * Deletes an existing Category model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Category model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Category the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Category::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
