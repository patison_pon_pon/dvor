<?php

namespace backend\controllers;

use backend\models\catalog\Supply;
use backend\models\catalog\Unit;
use backend\models\order\OrderHistory;
use backend\models\order\OrderProduct;
use backend\models\order\OrderStatus;
use backend\models\order\OrderSum;
use Yii;
use backend\models\order\Order;
use backend\models\order\OrderSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * OrderController implements the CRUD actions for Order model.
 */
class OrderController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Order models.
     * @return mixed
     */
    public function actionIndex()
    {
        $units = Unit::find()->all();
        $searchModel = new OrderSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'units' => $units,
        ]);
    }

    /**
     * Updates an existing Order model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $units = Unit::find()->all();
        $statuses = OrderStatus::find()->all();
        $products = OrderProduct::find()->where(['order_id' => $id])->with('image')->all();
        $histories = OrderHistory::find()->where(['order_id' => $id])->orderBy(['created_at' => SORT_DESC])->all();
        $new_history = new OrderHistory();
        $totals = OrderSum::find()->where(['order_id' => $model->id])->all();

        if ($new_history->load(Yii::$app->request->post())) {
            $new_history->order_id = $model->id;
            $new_history->created_at = date('U');
            if (!$new_history->save()){
                var_dump($new_history->errors);
                die();
            }
            $model->order_status_id = $new_history->order_status_id;
            $model->save();
            $success = 'Изменения сохранены';
            Yii::$app->session->setFlash('success', $success);
            return $this->refresh();
        } else {
            return $this->render('update', [
                'model' => $model,
                'units' => $units,
                'statuses' => $statuses,
                'products' => $products,
                'histories' => $histories,
                'new_history' => $new_history,
                'totals' => $totals,
            ]);
        }
    }

    /**
     * Deletes an existing Order model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Order model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Order the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Order::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
