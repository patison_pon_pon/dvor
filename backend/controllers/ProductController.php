<?php

namespace backend\controllers;

use backend\classes\SortList;
use backend\models\catalog\Belonging;
use backend\models\catalog\Category;
use backend\models\catalog\Option;
use backend\models\catalog\OptionValue;
use backend\models\catalog\ProductImage;
use backend\models\catalog\ProductOption;
use backend\models\catalog\ProductOptionValue;
use backend\models\catalog\Unit;
use rmrevin\yii\fontawesome\FA;
use Yii;
use backend\models\catalog\Product;
use backend\models\catalog\ProductSearch;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * ProductController implements the CRUD actions for Product model.
 */
class ProductController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Product models.
     * @return mixed
     */
    public function actionIndex()
    {
        $belongings = Belonging::find()->all();
        $categories = $this->getList();
        $searchModel = new ProductSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->setSort([
            'defaultOrder' => ['created_at'=>SORT_DESC]
        ]);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'belongings' => $belongings,
            'categories' => $categories,
        ]);
    }
    /**
     * Creates a new Product model.
     * If creation is successful, the browser will be redirected to the 'update' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Product();
        $belongings = Belonging::find()->all();
        $categories = $this->getList();
        $units = Unit::find()->all();

        if ($model->load(Yii::$app->request->post())) {
            $model->created_at = time();
            if(!$model->save()){
                return $model->errors;
            }
            return $this->redirect(['update', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'belongings' => $belongings,
                'categories' => $categories,
                'units' => $units,
            ]);
        }
    }

    public static function getList()
    {
        $data = Category::find()
            ->select(['id', 'parent_id', 'name'])
            ->orderBy('parent_id ASC')
            ->asArray()
            ->all();

        $sort = new SortList([
            'data' => $data,
            'prefix' => '------',
        ]);
        $sortList = ArrayHelper::map($sort->getList(), 'id', 'name');
        return $sortList;
    }

    /**
     * Updates an existing Product model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $belongings = Belonging::find()->all();
        $categories = $this->getList();
        $image = new ProductImage();
        $units = Unit::find()->all();

        //получили ajax с изображением, обновили его
        if ($image->load(Yii::$app->request->post())) {

            if (!file_exists(Yii::getAlias('@frontend') . '/web/image/catalog/Product/' . $model->id)) {
                mkdir(Yii::getAlias('@frontend') . '/web/image/catalog/Product/' . $model->id);
            }

            $i = UploadedFile::getInstance($image, 'image');
            $image->image = $i;
            $image->product_id = $model->id;
            $image_name = time();
            $image->src = $image_name . '.' . $image->image->extension;
            $image->image_name = $image_name;
            if(!$image->save()){
                return $image->errors;
            }
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            var_dump($model);
            die();
            return $this->redirect(['update', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'belongings' => $belongings,
                'categories' => $categories,
                'image' => $image,
                'units' => $units,
            ]);
        }
    }

    public function actionImage(){
        if (Yii::$app->request->isAjax) {
            $images = new ActiveDataProvider([
                'query' => ProductImage::find()->where(['product_id' => $_GET['product_id']])->orderBy(['sort' => SORT_ASC]),
                'pagination' => [
                    'pageSize' => 999,
                ],
            ]);
            return $this->renderPartial('image_list',[
                'images' => $images
            ]);
        }
        return false;
    }


    public function actionDeleteImage(){
        if (Yii::$app->request->isAjax) {
            $image = ProductImage::findOne($_GET['id']);
            if ($image->src) {
                if (file_exists(Yii::getAlias(Yii::getAlias('@frontend') . '/web/image/catalog/Product/') . $image->product_id . '/' . $image->src))
                    unlink(Yii::getAlias(Yii::getAlias('@frontend') . '/web/image/catalog/Product/')  . $image->product_id . '/'. $image->src);
                if (file_exists(Yii::getAlias(Yii::getAlias('@frontend') . '/web/image/catalog/Product/') . $image->product_id . '/' . 'thumb_' . $image->src))
                    unlink(Yii::getAlias(Yii::getAlias('@frontend') . '/web/image/catalog/Product/') . $image->product_id . '/' . 'thumb_' .
                        $image->src);
            }
            ProductImage::deleteAll(['id' => $_GET['id']]);
            $i = "<script>showImage('{$_GET['product_id']}')</script>";
            if(!$image->save()){
                return var_dump($image->errors);
            }else{
                return $i;
            }
        }
        return false;
    }

    public function actionSortPlus(){
        if (Yii::$app->request->isAjax) {
            $image = ProductImage::findOne($_GET['id']);
            $image->updateCounters(['sort' => +1]);
            $i = "<script>showImage('{$_GET['product_id']}')</script>";
            return $i;
        }
        return false;
    }

    public function actionSortMinus(){
        if (Yii::$app->request->isAjax) {
            $image = ProductImage::findOne($_GET['id']);
            $image->updateCounters(['sort' => -1]);
            $i = "<script>showImage('{$_GET['product_id']}')</script>";
            return $i;
        }
        return false;
    }

    public function actionOption($product_id = 0){

        if (Yii::$app->request->isAjax) {
            $product_id = $_GET['product_id'];
        }

        $product_options = ProductOption::find()->where(['product_id' => $product_id, 'parent_product_option_value_id' => '0'])->orderBy(['sort' => SORT_ASC])->all();
        $option_list = ArrayHelper::map(Option::find()->orderBy('sort')->all(), 'id', 'name');
        $web = '';

        foreach ($product_options as $product_option) {
            $web .= $this->drawProductOption($product_id, $product_option, $option_list, 0);
        }

        return $this->renderPartial('option_list',[
            'product_options' => $product_options,
            'product_id' => $product_id,
            'option_list' => $option_list,
            'web' => $web,
        ]);
    }


    //Рисуем опцию
    public function drawProductOption($product_id, $product_option, $option_list, $level, $parent_product_option_value_id = 0, $hidden = 0){
        $web = "<div class='col-xs-12 col-sm-12 col-md-12 col-lg-12'>";
        ($hidden ? $web .= "<div class='collapse option-block-{$parent_product_option_value_id}'" : '');
            $web .= "<div class='level-{$level}' style='padding: 0 0 5px " . ($level * 15) . "px; border-left:1px solid #ccc;'>";
                $web .= "<div class='row values'>";
                    $web .= "<div class='col-xs-6'>";
                        $web .= Html::dropDownList('', (isset($product_option->option_id) ? $product_option->option_id : ''), $option_list, ['class' => 'form-control', 'prompt' => 'Выберите опцию']);
                    $web .= "</div>";
                    $web .= "<div class='col-xs-3'>";
                    $web .= Html::input(
                        'number',
                        'sort',
                        (isset($product_option->sort) ? $product_option->sort : ''),
                        [
                            'class' => 'form-control sort',
                            'placeholder' => 'Сортировка',
                        ]
                    );
                    $web .= "</div>";
                    $web .= "<div class='col-xs-3 text-right'>";
                    if ($product_option):
                        $web .= Html::button(
                            FA::icon('edit'),
                            [
                                'class' => 'btn btn-primary',
                                'onclick' => "option_edit({$product_option->id}, {$product_id}, $(this).parents('.values').find('select').val(), {$product_option->parent_product_option_value_id}, $(this).parents('.values').find('.sort').val())"
                            ]
                        );
                        $web .= Html::button(
                            FA::icon('remove'),
                            [
                                'class' => 'btn btn-danger',
                                'onclick' => "return confirm('Уверены что хотите удалить?') ? option_delete({$product_option->id}, {$product_id}) : false"
                            ]
                        );
                    else:
                        $web .= Html::button(
                            'Добавить',
                            [
                                'class' => 'btn btn-success',
                                'onclick' => "option_add($(this).parents('.values').find('select').val(), $parent_product_option_value_id, {$product_id})"
                            ]
                        );
                    endif;
                    $web .= "</div>";
                $web .= "</div>";
            $web .= "</div>";
        //($hidden ? $web .= "</div>" : '');
        $web .= "</div>";

        $level++;
        if ($product_option){
            $option_value_list = ArrayHelper::map(OptionValue::find()->where(['option_id' => $product_option->option_id])->orderBy('sort')->all(), 'id', 'name');
            $product_option_values = ProductOptionValue::find()->where(['product_id' => $product_id, 'product_option_id' => $product_option->id])->orderBy('sort')->all();
            foreach ($product_option_values as $product_option_value) {
                $web .= $this->drawProductOptionValue($product_id, $product_option, $product_option_value, $option_value_list, $option_list, $level);
            }

            $web .= $this->drawNewProductOptionValue($product_id, $product_option, $option_value_list, $level);
        }


        return $web;
    }

    //Рисуем значения опции
    public function drawProductOptionValue($product_id, $product_option, $product_option_value, $option_value_list, $option_list, $level){
        $web = "<div class='col-xs-12 col-sm-12 col-md-12 col-lg-12'>";
            $web .= "<div class='level-{$level}' style='padding: 5px 0 5px " . ($level * 15) . "px; border-left:1px solid #ccc;'>";
                $web .= "<div class='row values'>";
                    $web .= "<div class='col-xs-6'>";
                        $web .= Html::dropDownList('', $product_option_value->option_value_id, $option_value_list, ['class' => 'form-control', 'prompt' => 'Выберите значение опции']);
                    $web .= "</div>";
                    $web .= "<div class='col-xs-2'>";
                        $web .= Html::input(
                            'number',
                            'quantity',
                            $product_option_value->quantity,
                            [
                                'class' => 'form-control quantity',
                                'placeholder' => 'Количество',
                            ]
                        );
                    $web .= "</div>";
                    $web .= "<div class='col-xs-1'>";
                        $web .= Html::input(
                            'number',
                            'sort',
                            $product_option_value->sort,
                            [
                                'class' => 'form-control sort',
                                'placeholder' => 'Сортировка',
                            ]
                        );
                    $web .= "</div>";
                    $web .= "<div class='col-xs-3 text-right'>";
                        $web .= Html::button(
                            FA::icon('plus'),
                            [
                                'class' => 'btn btn-warning',
                                'data-toggle' => 'collapse',
                                'data-target' => ".option-block-{$product_option_value->id}",
                            ]
                        );
                        $web .= Html::button(
                            FA::icon('edit'),
                            [
                                'class' => 'btn btn-primary',
                                'onclick' => "option_value_edit({$product_option_value->id}, {$product_id}, {$product_option->id}, {$product_option->option_id}, $(this).parents('.values').find('select').val(), $(this).parents('.values').find('.quantity').val(), $(this).parents('.values').find('.sort').val())"
                            ]
                        );
                        $web .= Html::button(
                            FA::icon('remove'),
                            [
                                'class' => 'btn btn-danger',
                                'onclick' => "return confirm('Уверены что хотите удалить?') ? option_value_delete({$product_option_value->id}, {$product_id}) : false"
                            ]
                        );
                    $web .= "</div>";
                $web .= "</div>";
            $web .= "</div>";
        $web .= "</div>";

        $hidden = 0;
        if (($product_option = ProductOption::findOne(['product_id' => $_GET['product_id'], 'parent_product_option_value_id' => $product_option_value->id])) == null){
            $product_option = 0;
            $hidden = 1;
        }
        $web .= $this->drawProductOption($product_id, $product_option, $option_list, $level, $product_option_value->id, $hidden);
        return $web;
    }

    //Рисуем чистую форму значения опции
    public function drawNewProductOptionValue($product_id, $product_option, $option_value_list, $level){
        //var_dump($product_option);
        $web = "<div class='col-xs-12 col-sm-12 col-md-12 col-lg-12'>";
            $web .= "<div class='level-{$level}' style='padding: 5px 0 15px " . ($level * 15) . "px; border-left:1px solid #ccc; border-bottom: 1px solid #ccc;margin-bottom: 15px'>";
                $web .= "<div class='row values'>";
                    $web .= "<div class='col-xs-6'>";
                        $web .= Html::dropDownList('', '', $option_value_list, ['class' => 'form-control', 'prompt' => 'Выберите значение опции']);
                    $web .= "</div>";
                    $web .= "<div class='col-xs-2'>";
                        $web .= Html::input(
                            'number',
                            'quantity',
                            '',
                            [
                                'class' => 'form-control quantity',
                                'placeholder' => 'Количество',
                            ]
                        );
                    $web .= "</div>";
                    $web .= "<div class='col-xs-2'>";
                        $web .= Html::input(
                            'number',
                            'sort',
                            '',
                            [
                                'class' => 'form-control sort',
                                'placeholder' => 'Сортировка',
                            ]
                        );
                    $web .= "</div>";
                    $web .= "<div class='col-xs-2 text-right'>";
                        $web .= Html::button(
                            FA::icon('arrow-right'),
                            [
                                'class' => 'btn btn-success',
                                'onclick' => "option_value_add({$product_id}, {$product_option->id}, {$product_option->option_id}, $(this).parents('.values').find('select').val(), $(this).parents('.values').find('.quantity').val(), $(this).parents('.values').find('.sort').val())"
                            ]
                        );
                    $web .= "</div>";
                $web .= "</div>";
            $web .= "</div>";
        $web .= "</div>";
        return $web;
    }

    //добавили опцию
    public function actionOptionAdd(){
        if (Yii::$app->request->isAjax) {
            if (($option = ProductOption::findOne(['product_id' => $_GET['product_id'], 'option_id' => $_GET['option_id'], 'parent_product_option_value_id' => $_GET['parent_product_option_value_id']])) == null){
                $option = new ProductOption();
                $option->product_id = $_GET['product_id'];
                $option->option_id = $_GET['option_id'];
                $option->parent_product_option_value_id = $_GET['parent_product_option_value_id'];
                $option->save();
                //$this->actionOption($_GET['product_id']);
                return true;
            }else{
                Yii::$app->session->setFlash('error', "Ошибка! Такая опция уже есть у товара");
            }
        }else{
            Yii::$app->session->setFlash('error', "Неизвестная ошибка");
        }
        return false;
    }

    //добавили значение опции
    public function actionOptionValueAdd(){
        if (Yii::$app->request->isAjax) {
            if (($option = ProductOptionValue::findOne(['product_id' => $_GET['product_id'], 'option_id' => $_GET['option_id'], 'option_value_id' => $_GET['option_value_id'], 'product_option_id' => $_GET['product_option_id']])) == null){
                $option = new ProductOptionValue();
                $option->product_id = $_GET['product_id'];
                $option->product_option_id = $_GET['product_option_id'];
                $option->option_value_id = $_GET['option_value_id'];
                $option->option_id = $_GET['option_id'];
                $option->quantity = ($_GET['quantity'] ? $_GET['quantity'] : 999);
                $option->sort = ($_GET['sort'] ? $_GET['sort'] : 5);
                if(!$option->save())
                    var_dump($option->errors);
                return true;
            }else{
                Yii::$app->session->setFlash('error', "Ошибка! Такое значение опции уже есть у товара");
            }
        }else{
            Yii::$app->session->setFlash('error', "Неизвестная ошибка");
        }
        return false;
    }

    //удалили опцию
    public function actionOptionDelete($product_option_id = 0){

        if (Yii::$app->request->isAjax && !$product_option_id) {
            $product_option_id = $_GET['product_option_id'];
        }

        $product_option = ProductOption::findOne($product_option_id);

        //проверили есть ли дочерние значения опций
        if (($product_option_values = ProductOptionValue::find()->where(['product_option_id' => $product_option_id])->all()) !== null) {
            foreach ($product_option_values as $product_option_value) {
                $this->actionOptionValueDelete($product_option_value->id);
            }
        }

        $product_option->delete();
        return true;
    }

    //удалили значение опцию
    public function actionOptionValueDelete($product_option_value_id = 0){

        if (Yii::$app->request->isAjax && !$product_option_value_id) {
            $product_option_value_id = $_GET['product_option_value_id'];
        }

        $product_option_value = ProductOptionValue::findOne($product_option_value_id);

        //проверили есть ли дочерняя опция
        if (($product_option = ProductOption::findOne(['parent_product_option_value_id' => $product_option_value_id])) !== null) {
            $this->actionOptionDelete($product_option->id);
        }

        $product_option_value->delete();
        return true;
    }

    //удалили опцию
    public function actionOptionEdit(){
        if (Yii::$app->request->isAjax) {
                $option = ProductOption::findOne($_GET['product_option_id']);
                $option->product_id = $_GET['product_id'];
                if ($option->option_id != $_GET['option_id']) {
                    if ((ProductOption::findOne(['product_id' => $_GET['product_id'], 'option_id' => $_GET['option_id']])) !== null) {
                        Yii::$app->session->setFlash('error', "Ошибка! Такая опция уже есть у товара");
                        return false;
                    }
                    if (($product_option_values = ProductOptionValue::find()->where(['product_option_id' => $option->id])->all()) !== null) {
                        foreach ($product_option_values as $product_option_value) {
                            $this->actionOptionValueDelete($product_option_value->id);
                        }
                    }
                }
                $option->option_id = $_GET['option_id'];
                $option->parent_product_option_value_id = $_GET['parent_product_option_value_id'];
                $option->sort = ($_GET['sort'] ? $_GET['sort'] : 5);
                if (!$option->save())
                    var_dump($option->errors);
                return true;
        }
        return false;
    }

    //удалили опцию
    public function actionOptionValueEdit(){
        if (Yii::$app->request->isAjax) {
            $option_value = ProductOptionValue::findOne($_GET['product_option_value_id']);
            $option_value->product_id = $_GET['product_id'];
            $option_value->product_option_id = $_GET['product_option_id'];
            $option_value->option_id = $_GET['option_id'];
            if ($option_value->option_value_id != $_GET['option_value_id']){
                if ((ProductOptionValue::findOne(['product_id' => $_GET['product_id'], 'option_value_id' => $_GET['option_value_id']])) !== null) {
                    Yii::$app->session->setFlash('error', "Ошибка! Такое значение опции уже есть у товара");
                    return false;
                }
                if (($product_option = ProductOption::findOne(['parent_product_option_value_id' => $option_value->id])) !== null) {
                        $this->actionOptionDelete($product_option->id);
                }
            }
            $option_value->option_value_id = $_GET['option_value_id'];
            $option_value->quantity = $_GET['quantity'];
            $option_value->sort = ($_GET['sort'] ? $_GET['sort'] : 5);
            if(!$option_value->save())
                var_dump($option_value->errors);
            return true;
        }
        return false;
    }

    /**
     * Deletes an existing Product model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        if (is_dir(Yii::getAlias('@frontend') . '/web/image/catalog/Product/' . $id))
            Yii::$app->my->removeDirectory(Yii::getAlias('@frontend') . '/web/image/catalog/Product/' . $id . '/');

        ProductImage::deleteAll(['product_id' => $id]);
        $this->findModel($id)->delete();

        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * Finds the Product model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Product the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Product::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
