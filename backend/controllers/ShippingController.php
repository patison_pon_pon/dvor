<?php

namespace backend\controllers;

use backend\models\catalog\Supply;
use backend\models\shop\ShippingPrice;
use Yii;
use backend\models\shop\Shipping;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ShippingController implements the CRUD actions for Shipping model.
 */
class ShippingController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Shipping models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Shipping::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new Shipping model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Shipping();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['update', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Shipping model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $supplies = Supply::find()->asArray()->all();
        $prices = '';
        foreach ($supplies as $supply) {
            if($p = ShippingPrice::findOne(['supply_id' => $supply['id'], 'shipping_id' => $model->id]) == null){
                $p = new ShippingPrice();
                $p->supply_id = $supply['id'];
                $p->shipping_id = $model->id;
                $p->weight_price = 0;
                $p->pack_weight = 0;
                $p->pack_price = 0;
                if(!$p->save()){
                    return $p->errors;
                }
                $price = ['id' => $p->primaryKey, 'supply_id' => $supply['id'], 'supply_name' => $supply['name'], 'shipping_id' => $model->id, 'weight_price' => '0', 'pack_weight' => '0', 'pack_price' => '0'];
            }else{
                $p = ShippingPrice::find()->where(['supply_id' => $supply['id'], 'shipping_id' => $model->id])->asArray()->one();
                $price = $p;
                $price['supply_name'] = $supply['name'];
            }
            /*var_dump($price);
            die();*/
            $prices[] = $price;
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['update', 'id' => $model->id]);
        }elseif (isset(Yii::$app->request->post()['price'])){
            foreach (Yii::$app->request->post('price') as $price) {
                $p = ShippingPrice::findOne($price['id']);
                $p->weight_price = $price['weight_price'];
                $p->pack_weight = $price['pack_weight'];
                $p->pack_price = $price['pack_price'];
                if (!$p->save()){
                    var_dump($p->errors);
                    die();
                }
            }
            Yii::$app->session->setFlash('success', 'Цены сохранены');
            return $this->refresh();
        } else {
            return $this->render('update', [
                'model' => $model,
                'prices' => $prices,
                'supplies' => $supplies,
            ]);
        }
    }

    /**
     * Deletes an existing Shipping model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Shipping model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Shipping the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Shipping::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
