<?php
/**
 * Created by PhpStorm.
 * User: patison
 * Date: 02.11.17
 * Time: 16:33
 */

namespace backend\classes;


use Yii;
use yii\grid\DataColumn;

class NumberColumn extends DataColumn
{
    private $_total = 0;

    public function getDataCellValue($model, $key, $index)
    {
        $value = str_replace(" ","",parent::getDataCellValue($model, $key, $index));
        $this->_total += $value;
        return Yii::$app->my->nf($value);
    }

    protected function renderFooterCellContent()
    {
        return 'Итого <b>' . Yii::$app->my->nf($this->grid->formatter->format($this->_total, $this->format)) . '</b>';
    }
}