<?php

/* @var $this yii\web\View */

$this->title = 'TOSOPT Admin-panel';
?>
<div class="site-index">
    <h2 class="text-center">Приветствуем вас в админке</h2>
</div>
<div class="container">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        	<div class="panel panel-info">
        		  <div class="panel-heading">
        				<h3 class="panel-title">Важные моменты</h3>
        		  </div>
        		  <div class="panel-body">
                      <ul class="list-group">
                      	<li class="list-group-item">Когда создается/редактируется способ доставки - обязательно надо задать вес упаковки, а то вылезут ошибки на сайте. Я пока поставил автоматически 50 кг</li>
                      	<li class="list-group-item">Товары желательно не удалять, а просто ставить статус "Отключен", при этом статусе он исчезнет с сайта. Если хотите удалить товар - убедитесь что он не учавствует в модулях и его нет в заказах. Возможно я вообще уберу кнопку удалить</li>
                      	<li class="list-group-item">Не добавляйте хлам в текстовый редактор, все ваши добавленные изображения сохраняются и засоряют сервак</li>
                      	<li class="list-group-item">Следите за списком категорий. После того как я создал тестовые категории, у вас так и висит категория футболки---теплые. Это забавно)</li>
                      	<li class="list-group-item">Все свои дальнейшие пожелания буду писать сюда</li>
                      </ul>
        		  </div>
        	</div>
        </div>
    </div>
</div>