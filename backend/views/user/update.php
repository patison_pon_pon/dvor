<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\company\User */

$this->title = 'Редактировать пользователя: ' . $model->username;
$this->params['breadcrumbs'][] = ['label' => 'Пользователи', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Редактировать';
?>
<div class="user-update">

    <ul class="nav nav-tabs nav-justified" role="tablist">
        <li class="active"><a href="#tab1" role="tab" data-toggle="tab">Основная информация</a></li>
        <li><a href="#tab2" role="tab" data-toggle="tab">Пароль</a></li>
    </ul>
    <!-- TAB CONTENT -->
    <div class="tab-content">
        <div class="active tab-pane fade in" id="tab1">
            <br>


            <?php $form = ActiveForm::begin(['id' => 'form-user']); ?>

            <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>

            <?= $form->field($model, 'role')->dropDownList(\yii\helpers\ArrayHelper::map($roles, 'name', 'description'), ['prompt' => 'Выберите роль']) ?>

            <?= $form->field($model, 'email') ?>

            <div class="form-group">
                <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary', 'name' => 'signup-button']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
        <div class="tab-pane fade" id="tab2">
            <br>
            <?php $form2 = ActiveForm::begin(['id' => 'form-password']); ?>

            <div class="form-group">
                <?= Html::input('password', 'password', '', ['class' => 'form-control', 'placeholder' => 'Новый пароль']) ?>
            </div>

            <div class="form-group">
                <?= Html::submitButton('Изменить пароль', ['class' => 'btn btn-primary', 'name' => 'signup-button']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>


</div>
