<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\catalog\ProductSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Товары';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-index table-responsive">

    <p class="text-right">
        <?= Html::a('Создать товар', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'label' => 'Фото',
                'headerOptions' => ['width' => '80'],
                'format' => 'raw',
                'value' => function ($model) {
                    return (isset($model->image->src)
                        ? Html::img(Yii::getAlias('@mysite') . '/image/catalog/Product/' . $model->id . '/thumb_' . $model->image->src, ['class' => 'img-thumbnail'])
                        : ''
                    );
                },
            ],

            [
                'attribute' => 'id',
                'headerOptions' => ['width' => '80'],
            ],
            [
                'attribute' => 'belonging_id',
                'value' => function ($model) {
                    return $model->belonging->name;
                },
                'filter' => ArrayHelper::map($belongings, 'id', 'name'),
            ],
            [
                'attribute' => 'category_id',
                'value' => function ($model) {
                    return $model->category->name;
                },
                'filter' => $categories,
            ],
            [
                'attribute' => 'vendor_code',
                'value' => function ($model) {
                    return $model->vendor_code;
                },
            ],
            'name',
            // 'description:ntext',
            [
                'attribute' => 'price',
                'value' => function ($model) {
                    return '$ ' . $model->price;
                },
                'filter' => '',
            ],
            // 'quantity',
            // 'minimum',
            // 'weight',
            // 'color:ntext',
            // 'size:ntext',
            [
                'attribute' => 'status',
                'format' => 'raw',
                'value' => function ($model) {
                    return (
                    $model->status
                        ? "<span class='label label-success'>Включен</span>"
                        : "<span class='label label-danger'>Отключен</span>"
                    );
                },
                'filter' => ['1' => 'Включен', '0' => 'Отключен'],
            ],
            [
                'attribute' => 'created_at',
                'format' => 'datetime',
                'value' => function ($model) {
                    return $model->created_at;
                },
                'filter' => '',
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'header'=>'Действия',
                'headerOptions' => ['width' => '90'],
                'template' => '{update}&nbsp;{delete}',
                'buttons' => [
                    'update' => function($url, $model){
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', ['update', 'id' => $model->id], [
                            'class' => 'btn btn-primary btn-sm',
                            'title' => 'Редактировать',
                        ]);
                    },
                    'view' => function($url, $model){
                        return Html::a('<span class="fa fa-eye"></span>', ['view', 'id' => $model->id], [
                            'class' => 'btn btn-success btn-sm',
                            'title' => 'Редактировать',
                        ]);
                    },
                    'delete' => function($url, $model){
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', ['delete', 'id' => $model->id], [
                            'class' => 'btn btn-danger btn-sm',
                            'title' => 'Удалить',
                            'data' => [
                                'confirm' => 'Уверены что хотите удлить?',
                                'method' => 'post',
                            ],
                        ]);
                    }
                ],
            ],
        ],
    ]); ?>
</div>
