<?php

use yii\helpers\Url;
//Изображения
$image_path = Url::to(["/product/image"]);
$image_delete_path = Url::to(["/product/delete-image"]);
$image_sort_plus_path = Url::to(["/product/sort-plus"]);
$image_sort_minus_path = Url::to(["/product/sort-minus"]);
$script = <<< JS
    $(document).ready(function () {
        $(".language-tabs li:first a").click();
    });

    function showImage(product_id) {
        $.ajax({
            url: '$image_path',
            cache: false,
            type: "GET",
            data: { 'product_id': product_id},
            success: function (data) {
                $(".product-image").html(data);
            }
        });
    };
    
    function deleteImage(id, product_id) {
        $.ajax({
            url: '$image_delete_path',
            cache: false,
            type: "GET",
            data: { 'product_id': product_id, 'id': id},
            success: function (data) {
                $(".product-image").html(data);
            }
        });
    };
    
    function sortPlus(id, product_id) {
        $.ajax({
            url: '$image_sort_plus_path',
            cache: false,
            type: "GET",
            data: { 'product_id': product_id, 'id': id},
            success: function (data) {
                $(".product-image").html(data);
            }
        });
    };
    
    function sortMinus(id, product_id) {
        $.ajax({
            url: '$image_sort_minus_path',
            cache: false,
            type: "GET",
            data: { 'product_id': product_id, 'id': id},
            success: function (data) {
                $(".product-image").html(data);
            }
        });
    };
JS;
$this->registerJs($script, yii\web\View::POS_HEAD);

//опции
$option_path = Url::to(["/product/option"]);
$option_add_new = Url::to(["/product/option-add"]);
$option_value_add_new = Url::to(["/product/option-value-add"]);
$option_delete = Url::to(["/product/option-delete"]);
$option_value_delete = Url::to(["/product/option-value-delete"]);
$option_edit = Url::to(["/product/option-edit"]);
$option_value_edit = Url::to(["/product/option-value-edit"]);
$script = <<< JS
    
    function showOption(product_id) {
        $.ajax({
            url: '$option_path',
            cache: false,
            type: "GET",
            data: { 'product_id': product_id},
            success: function (data) {
                $("#option-container").html(data);
            }
        });
    };
    function option_add(option_id, parent_product_option_value_id, product_id) {
        $.ajax({
            url: '$option_add_new',
            cache: false,
            type: "GET",
            data: { 'option_id': option_id, 'parent_product_option_value_id': parent_product_option_value_id, 'product_id': product_id},
            success: function (data) {
                $("#option-container").html(data);
                showOption(product_id);
            }
        });
    };
    function option_value_add(product_id, product_option_id, option_id, option_value_id, quantity, sort) {
        $.ajax({
            url: '$option_value_add_new',
            cache: false,
            type: "GET",
            data: { 'product_id': product_id, 'product_option_id': product_option_id, 'option_value_id': option_value_id, 'option_id': option_id,  'quantity': quantity, 'sort': sort},
            success: function (data) {
                $("#option-container").html(data);
                showOption(product_id);
            }
        });
    };
    
    function option_delete(product_option_id, product_id) {
        $.ajax({
            url: '$option_delete',
            cache: false,
            type: "GET",
            data: { 'product_option_id': product_option_id},
            success: function (data) {
                $("#option-container").html(data);
                showOption(product_id);
            }
        });
    };
    
    function option_value_delete(product_option_value_id, product_id) {
        $.ajax({
            url: '$option_value_delete',
            cache: false,
            type: "GET",
            data: { 'product_option_value_id': product_option_value_id},
            success: function (data) {
                $("#option-container").html(data);
                showOption(product_id);
            }
        });
    };
    
    function option_edit(product_option_id, product_id, option_id, parent_product_option_value_id, sort) {
        $.ajax({
            url: '$option_edit',
            cache: false,
            type: "GET",
            data: { 'product_option_id': product_option_id, 'product_id': product_id, 'option_id': option_id, 'parent_product_option_value_id': parent_product_option_value_id, 'sort': sort},
            success: function (data) {
                $("#option-container").html(data);
                showOption(product_id);
            }
        });
    };
    
    function option_value_edit(product_option_value_id, product_id, product_option_id, option_id, option_value_id, quantity, sort) {
        console.log(product_option_value_id);
        console.log(product_id);
        console.log(product_option_id);
        console.log(option_id);
        console.log(option_value_id);
        console.log(quantity);
        console.log(sort);
        $.ajax({
            url: '$option_value_edit',
            cache: false,
            type: "GET",
            data: { 'product_option_value_id': product_option_value_id, 'product_id': product_id, 'product_option_id': product_option_id, 'option_id': option_id, 'option_value_id': option_value_id, 'quantity': quantity, 'sort': sort},
            success: function (data) {
                $("#option-container").html(data);
                showOption(product_id);
            }
        });
    };
JS;
$this->registerJs($script, yii\web\View::POS_HEAD);