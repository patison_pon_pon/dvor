<?php

use common\widgets\Alert;
use yii\helpers\Html;
?>
<?= Alert::widget() ?>
<div class="row">

    <?=$web?>

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <hr>
        <div class="input-group">
            <?=Html::dropDownList(
                'option_id',
                '',
                $option_list,
                ['class' => 'form-control', 'prompt' => 'Выберите опцию']
            )?>
            <span class="input-group-btn">
                <?= Html::button(
                    'Создать новую опцию',
                    [
                        'class' => 'btn btn-success',
                        'onclick' => "option_add($(this).parents('.input-group').find('select').val(), 0, {$product_id})"
                    ]
                ) ?>
            </span>
        </div>
    </div>
</div>
