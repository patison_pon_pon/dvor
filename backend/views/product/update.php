<?php

use bupy7\cropbox\CropboxWidget;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model backend\models\catalog\Product */

$this->title = 'Редактировать товар: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Товары', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Редактировать';
include('scripts.php');
?>
<div class="product-update">

    <!-- MAIN TAB NAVIGATION -->
    <ul class="nav nav-tabs nav-justified main-tabs" role="tablist">
        <li class="active"><a href="#tab1" role="tab" data-toggle="tab">Основные настройки</a></li>
        <li><a href="#tab2" role="tab" data-toggle="tab">Изображения</a></li>
        <li><a href="#tab3" role="tab" data-toggle="tab">Опции</a></li>
    </ul>

    <!-- MAIN TAB CONTENT -->
    <div class="tab-content">
        <div class="active tab-pane fade in" id="tab1">

            <div class="product-form">

                <?php $form = ActiveForm::begin(); ?>

                <?= $form->field($model, 'belonging_id')->dropDownList(ArrayHelper::map($belongings, 'id', 'name'), ['prompt' => 'Выберите принадлежность']) ?>

                <?= $form->field($model, 'unit_id')->dropDownList(ArrayHelper::map($units, 'id', 'name'), ['prompt' => 'Выберите единицу измерения']) ?>

                <?= $form->field($model, 'category_id')->dropDownList($categories, ['prompt' => 'Выберите категорию']) ?>

                <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'vendor_code')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'price')->input('number', ['step' => '.01']) ?>

                <?= $form->field($model, 'minimum')->input('number') ?>

                <?= $form->field($model, 'weight')->input('number', ['step' => '.01']) ?>

                <?= $form->field($model, 'stock')->dropDownList(['1' => "В наличии", "0" => "Нет в наличии"]) ?>

                <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

                <?= $form->field($model, 'status')->dropDownList(['1' => 'Включен', '0' => 'Отключен']) ?>

                <div class="form-group">
                    <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
                </div>

                <?php ActiveForm::end(); ?>

            </div>
        </div>


        <div class="tab-pane fade" id="tab2">
            <br>
            <div class="add-image-block col-xs-12 col-sm-4">
                <?php Pjax::begin(); ?>
                <?php
                $form2 = ActiveForm::begin([
                    'options' => [
                        'enctype' => 'multipart/form-data',
                        'data-pjax' => '1',
                        'id' => 'spare-image-form'
                    ]
                ]);
                ?>
                <table>
                    <tr>
                        <td>
                            <?= $form2->field($image, 'image')->widget(CropboxWidget::className(), [
                                'croppedDataAttribute' => 'crop_info',
                                'pluginOptions' => [
                                    'variants' => [
                                        [
                                            'width' => 400,
                                            'height' => 400,
                                            'minWidth' => 400,
                                            'minHeight' => 400,
                                            'maxWidth' => 400,
                                            'maxHeight' => 400,
                                        ],
                                    ],
                                ],
                            ]); ?>
                        </td>
                    </tr>
                </table>
                <div class="form-group">
                    <?= Html::submitButton('Добавить', ['class' => 'btn btn-primary']) ?>
                </div>
                <?php ActiveForm::end(); ?>
                <script>showImage('<?=$_GET['id']?>')</script>
                <?php Pjax::end(); ?>
            </div>
            <script>showImage('<?=$_GET['id']?>')</script>
            <div class="product-image show-images text-center col-xs-12 col-sm-8"></div>
            <div class="clearfix"></div>
        </div>
        <div class="tab-pane fade" id="tab3">
            <br>
            <script>showOption('<?=$_GET['id']?>')</script>
            <div id="option-container"></div>
            <div class="clearfix"></div>
        </div>
    </div>

</div>
