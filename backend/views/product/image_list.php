<?php

use yii\widgets\ListView;
echo ListView::widget([
    'dataProvider' => $images,
    'itemView' => '_list',
    'layout' => "{items}",
    'emptyText' => 'Список пуст',
    'emptyTextOptions' => [
        'tag' => 'p'
    ],
]);