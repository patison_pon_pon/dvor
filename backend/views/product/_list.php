<?php
use yii\bootstrap\Html;
?>
<div class='image-preview'>
    <?=Html::img(Yii::getAlias('@mysite') . '/image/catalog/Product/' . $model->product_id . '/thumb_' . $model->src, ['class' => 'img-thumbnail'])?>
    <?=Html::button('<i class="glyphicon glyphicon-remove"></i>', ['class' => 'btn btn-danger', 'onclick' => "deleteImage('{$model->id}', '{$model->product_id}')"])?>
    <div>
        <div class="btn-group">
        <?=Html::button("<i class=\"fa fa-arrow-circle-left\"></i>", ['class' => 'btn btn-primary', 'onclick' => "sortMinus('{$model->id}', '{$model->product_id}')"])?>
            <?=Html::button($model->sort, ['class' => 'btn btn-default', 'disabled' => 1])?>
        <?=Html::button("<i class=\"fa fa-arrow-circle-right\"></i>", ['class' => 'btn btn-primary', 'onclick' => "sortPlus('{$model->id}', '{$model->product_id}')"])?>
        </div>
    </div>
</div>