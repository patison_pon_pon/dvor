<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\catalog\Product */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="product-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'belonging_id')->dropDownList(ArrayHelper::map($belongings, 'id', 'name'), ['prompt' => 'Выберите принадлежность']) ?>

    <?= $form->field($model, 'unit_id')->dropDownList(ArrayHelper::map($units, 'id', 'name'), ['prompt' => 'Выберите единицу измерения']) ?>

    <?= $form->field($model, 'category_id')->dropDownList($categories, ['prompt' => 'Выберите категорию']) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'vendor_code')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'price')->input('number', ['step' => '.01']) ?>

    <?= $form->field($model, 'minimum')->input('number') ?>

    <?= $form->field($model, 'weight')->input('number', ['step' => '.01']) ?>

    <?= $form->field($model, 'stock')->dropDownList(['1' => "В наличии", "0" => "Нет в наличии"]) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'status')->dropDownList(['1' => 'Включен', '0' => 'Отключен']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
