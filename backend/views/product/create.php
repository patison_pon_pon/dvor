<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;


/* @var $this yii\web\View */
/* @var $model backend\models\catalog\Product */

$this->title = 'Создать товар';
$this->params['breadcrumbs'][] = ['label' => 'Товары', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-create">

    <div class="product-form">

        <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'belonging_id')->dropDownList(ArrayHelper::map($belongings, 'id', 'name'), ['prompt' => 'Выберите принадлежность']) ?>

        <?= $form->field($model, 'unit_id')->dropDownList(ArrayHelper::map($units, 'id', 'name'), ['prompt' => 'Выберите единицу измерения']) ?>

        <?= $form->field($model, 'category_id')->dropDownList($categories, ['prompt' => 'Выберите категорию']) ?>

        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'vendor_code')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'price')->input('number', ['step' => '.01']) ?>

        <?= $form->field($model, 'minimum')->input('number') ?>

        <?= $form->field($model, 'weight')->input('number', ['step' => '.01', 'value' => '0']) ?>

        <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

        <div class="form-group">
            <?= Html::submitButton('Создать', ['class' => 'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>

</div>
