<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\shop\Shipping */

$this->title = 'Редактировать доставку: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Доставки', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Редактировать';
?>
<div class="shipping-update">

    <!-- MAIN TAB NAVIGATION -->
    <ul class="nav nav-tabs nav-justified main-tabs" role="tablist">
        <li class="active"><a href="#tab1" role="tab" data-toggle="tab">Основные настройки</a></li>
        <li><a href="#tab2" role="tab" data-toggle="tab">Цены</a></li>
    </ul>

    <!-- MAIN TAB CONTENT -->
    <div class="tab-content">
        <div class="active tab-pane fade in" id="tab1">
            <br>
            <div class="shipping-form">

                <?php $form = ActiveForm::begin(); ?>

                <?= $form->field($model, 'name')->textInput() ?>

                <div class="form-group">
                    <?= Html::submitButton($model->isNewRecord ? 'Создать' : ' Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>

                <?php ActiveForm::end(); ?>

            </div>
        </div>

        <div class="tab-pane fade" id="tab2">
            <br>
            <?php $form = ActiveForm::begin(); ?>
            <?php foreach ($prices as $price):?>
                <h4><?=$price['supply_name']?></h4>
                <div class="form-group">
                    <?=Html::label('Цена доставки за 1 кг: ' . $price['supply_name'], '', ['class' => 'control-label'])?>
                    <?=Html::input('text', "price[{$price['id']}][weight_price]", $price['weight_price'], ['class' => 'form-control', 'required' => 1] )?>
                    <?=Html::input('hidden', "price[{$price['id']}][id]", $price['id'], ['class' => 'form-control', 'required' => 1] )?>
                </div>
                <div class="form-group">
                    <?=Html::label('Вес одной упаковки: ' . $price['supply_name'], '', ['class' => 'control-label'])?>
                    <?=Html::input('text', "price[{$price['id']}][pack_weight]", $price['pack_weight'], ['class' => 'form-control', 'required' => 1] )?>
                </div>
                <div class="form-group">
                    <?=Html::label('Цена за одну упаковку: ' . $price['supply_name'], '', ['class' => 'control-label'])?>
                    <?=Html::input('text', "price[{$price['id']}][pack_price]", $price['pack_price'], ['class' => 'form-control', 'required' => 1] )?>
                </div>
                <hr>
            <?php endforeach; ?>

            <div class="form-group">
                <?= Html::submitButton(' Сохранить', ['class' => 'btn btn-primary']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>


</div>
