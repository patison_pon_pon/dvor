<aside class="main-sidebar">

    <section class="sidebar">

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu'],
                'items' => [
                    ['label' => 'Главное меню', 'options' => ['class' => 'header']],

                    ['label' => 'Товары', 'icon' => 'ticket', 'url' => ['/product/index']],

                    [
                        'label' => 'Каталог',
                        'icon' => 'folder-open',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Категории', 'icon' => 'circle-o', 'url' => ['/category/index'],],
                            ['label' => 'Опции', 'icon' => 'circle-o', 'url' => ['/option/index'],],
                            //['label' => 'Поставки', 'icon' => 'circle-o', 'url' => ['/supply/index'],],
                            ['label' => 'Принадлежности', 'icon' => 'circle-o', 'url' => ['/belonging/index'],],
                            ['label' => 'Единицы измерения', 'icon' => 'circle-o', 'url' => ['/unit/index'],],
                        ],
                    ],
                    [
                        'label' => 'Магазин',
                        'icon' => 'gears',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Покупатели', 'icon' => 'circle-o', 'url' => ['/customer/index'],],
                            ['label' => 'Информационные страницы', 'icon' => 'circle-o', 'url' => ['/info/index'],],
                            //['label' => 'Доставка', 'icon' => 'circle-o', 'url' => ['/shipping/index'],],
                            ['label' => 'Валюты', 'icon' => 'circle-o', 'url' => ['/currency/index'],],
                            ['label' => 'Пользователи', 'icon' => 'circle-o', 'url' => ['/user/index'],],
                            //['label' => 'Менеджеры', 'icon' => 'circle-o', 'url' => ['/manager/index'],],
                        ],
                    ],
                    [
                        'label' => 'Заказы',
                        'icon' => 'shopping-cart',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Заказы', 'icon' => 'circle-o', 'url' => ['/order/index'],],
                            ['label' => 'Статусы заказа', 'icon' => 'circle-o', 'url' => ['/order-status/index'],],
                        ],
                    ],
                    [
                        'label' => 'Модули',
                        'icon' => 'wrench',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Карусель товаров', 'icon' => 'circle-o', 'url' => ['/product-carousel/index'],],
                            ['label' => 'Перезвоните мне', 'icon' => 'circle-o', 'url' => ['/call-me/index'],],
                        ],
                    ],
                    //['label' => 'Gii', 'icon' => 'file-code-o', 'url' => ['/gii']],
                    ['label' => 'Login', 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest],
                ],
            ]
        ) ?>

    </section>

</aside>
