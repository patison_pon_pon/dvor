<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\shop\InfoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Информационные страницы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="info-index">

    <p class="text-right">
        <?= Html::a('Создать страницу', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',
            'url',
            //'meta_t',
            //'meta_k',
            // 'meta_d',
            // 'description:ntext',
            [
                'attribute' => 'created_at',
                'format' => 'datetime',
                'value' => function ($model) {
                    return $model->created_at;
                },
                'filter' => '',
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'header'=>'Действия',
                'headerOptions' => ['width' => '90'],
                'template' => '{update}&nbsp;{delete}',
                'buttons' => [
                    'update' => function($url, $model){
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', ['update', 'id' => $model->id], [
                            'class' => 'btn btn-primary btn-sm',
                            'title' => 'Редактировать',
                        ]);
                    },
                    'view' => function($url, $model){
                        return Html::a('<span class="fa fa-eye"></span>', ['view', 'id' => $model->id], [
                            'class' => 'btn btn-success btn-sm',
                            'title' => 'Редактировать',
                        ]);
                    },
                    'delete' => function($url, $model){
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', ['delete', 'id' => $model->id], [
                            'class' => 'btn btn-danger btn-sm',
                            'title' => 'Удалить',
                            'data' => [
                                'confirm' => 'Уверены что хотите удлить?',
                                'method' => 'post',
                            ],
                        ]);
                    }
                ],
            ],
        ],
    ]); ?>
</div>
