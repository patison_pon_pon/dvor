<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\shop\Info */

$this->title = 'Создать странцу';
$this->params['breadcrumbs'][] = ['label' => 'Информационные страницы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="info-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
