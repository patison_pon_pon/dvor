<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\shop\Info */

$this->title = 'Редактировать страницу: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Информационные страницы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->name;
?>
<div class="info-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
