<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\order\OrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Заказы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-index">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [

            'id',
            [
                'attribute' => 'customer_id',
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::a('<b>' . $model->customer->username . '</b>&nbsp;(' . $model->customer_id . ')', ['/customer/update', 'id' => $model->customer_id]);
                },
            ],
            [
                'attribute' => 'unit_id',
                'value' => function ($model) {
                    return $model->unit->name;
                },
                'filter' => ArrayHelper::map($units, 'id', 'name'),
            ],
            // 'region',
            // 'city',
            // 'address',
            // 'comment:ntext',
            [
                'attribute' => 'quantity',
                'format' => 'raw',
                'value' => function ($model) {
                    return Yii::$app->my->nf($model->quantity);
                },
                'filter' => ''
            ],
            [
                'attribute' => 'sum',
                'format' => 'raw',
                'value' => function ($model) {
                    return Yii::$app->my->nf($model->sum) . '&nbsp;' . $model->currency->symbol;
                },
                'filter' => ''
            ],
            // 'sum',
            // 'currency',
            // 'order_status_id',
            // 'created_at',
            [
                'attribute' => 'created_at',
                'format' => 'datetime',
                'value' => function ($model) {
                    return $model->created_at;
                },
                'filter' => ''
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'header'=>'Действия',
                'headerOptions' => ['width' => '90'],
                'template' => '{update}&nbsp;{delete}',
                'buttons' => [
                    'update' => function($url, $model){
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', ['update', 'id' => $model->id], [
                            'class' => 'btn btn-primary btn-sm',
                            'title' => 'Редактировать',
                        ]);
                    },
                    'view' => function($url, $model){
                        return Html::a('<span class="fa fa-eye"></span>', ['view', 'id' => $model->id], [
                            'class' => 'btn btn-success btn-sm',
                            'title' => 'Редактировать',
                        ]);
                    },
                    'delete' => function($url, $model){
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', ['delete', 'id' => $model->id], [
                            'class' => 'btn btn-danger btn-sm',
                            'title' => 'Удалить',
                            'data' => [
                                'confirm' => 'Уверены что хотите удлить?',
                                'method' => 'post',
                            ],
                        ]);
                    }
                ],
            ],
        ],
    ]); ?>
</div>
