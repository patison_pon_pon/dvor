<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\order\Order */

$this->title = 'Редактировать заказ №: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Заказы', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Заказ №' . $model->name;
?>
<div class="order-update">

    <div class="order-form">

        <!-- TAB NAVIGATION -->
        <ul class="nav nav-tabs nav-justified" role="tablist">
            <li class="active"><a href="#tab1" role="tab" data-toggle="tab">Общая инфа о заказе</a></li>
            <li><a href="#tab2" role="tab" data-toggle="tab">Товары</a></li>
            <li><a href="#tab3" role="tab" data-toggle="tab">История</a></li>
        </ul>
        <!-- TAB CONTENT -->
        <div class="tab-content">
            <div class="active tab-pane fade in" id="tab1">
                <br>
                <?php $form = ActiveForm::begin(); ?>
                <div class="row">
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                        <?= $form->field($model, 'customer_id')->textInput(['name' => '', 'disabled' => 1, 'value' => $model->customer->username . ' (' . $model->customer_id . ')']) ?>

                        <?= $form->field($model, 'unit_id')->dropDownList(\yii\helpers\ArrayHelper::map($units, 'id', 'name'), ['disabled' => 1]) ?>

                        <?= $form->field($model, 'quantity')->textInput(['name' => '', 'disabled' => 1,'maxlength' => true]) ?>

                        <?= $form->field($model, 'sum')->textInput(['name' => '', 'disabled' => 1,'maxlength' => true, 'value' => htmlspecialchars_decode($model->sum . ' ' . $model->currency->symbol)]) ?>

                        <?= $form->field($model, 'currency')->textInput(['name' => '', 'disabled' => 1,'maxlength' => true]) ?>
                    </div>
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                        <?= $form->field($model, 'name')->textInput(['name' => '', 'disabled' => 1]) ?>

                        <?= $form->field($model, 'phone')->textInput(['name' => '', 'disabled' => 1]) ?>

                        <?= $form->field($model, 'region')->textInput(['name' => '', 'disabled' => 1]) ?>

                        <?= $form->field($model, 'city')->textInput(['name' => '', 'disabled' => 1]) ?>

                        <?= $form->field($model, 'address')->textInput(['name' => '', 'disabled' => 1]) ?>
                    </div>
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                        <?= $form->field($model, 'comment')->textarea(['name' => '', 'disabled' => 1, 'rows' => 6]) ?>

                        <?= $form->field($model, 'order_status_id')->dropDownList(\yii\helpers\ArrayHelper::map($statuses, 'id', 'name'), ['name' => '', 'disabled' => 1]) ?>
                    </div>
                </div>




                <?php ActiveForm::end(); ?>
            </div>
            <div class="tab-pane fade" id="tab2">
                <br>
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>Фото</th>
                            <th>Имя</th>
                            <th>Цена</th>
                            <th>Количество</th>
                            <th>Сумма</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($products as $product):?>
                            <tr>
                                <td>
                                    <?=
                                    Html::a(
                                        Html::img(Yii::getAlias('@mysite') . '/image/catalog/Product/' . $product->product_id . '/thumb_' . $product->image->src, ['class' => 'img-thumbnail', 'width' => '100']),
                                        ['/product/update', 'id' => $product->product_id],
                                        ['target' => '_blank']
                                    )
                                    ;?>
                                </td>
                                <td>
                                    <big><?=$product->product->name?></big>
                                    <?php
                                    $options = unserialize($product->options);
                                    if (is_array($options)){
                                    foreach (unserialize($product->options) as $option){?>
                                        <br><small><i><?=$option['product_option_name']?>: <b><?=$option['product_option_value_name']?></b></i></small>
                                    <?php }
                                    }
                                    ?>
                                </td>
                                <td><?=Yii::$app->my->nf($product->price) . '&nbsp;' . $model->currency->symbol?></td>
                                <td><?=$product->quantity?></td>
                                <td><?=Yii::$app->my->nf($product->sum) . '&nbsp;' . $model->currency->symbol?></td>
                            </tr>
                        <?php endforeach; ?>
                        <?php foreach ($totals as $total):?>
                        <tr>
                            <td colspan="4" class="text-right"><?=$total->description?>:</td>
                            <td><b><?=Yii::$app->my->nf($total->value) . '&nbsp;' . $model->currency->symbol?></b></td>
                        </tr>
                        <?php endforeach; ?>
                        <tr>
                            <td colspan="4" class="text-right">Итого:</td>
                            <td><b><?=Yii::$app->my->nf($model->sum) . '&nbsp;' . $model->currency->symbol?></b></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="tab-pane fade" id="tab3">
                <br>
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>Статус</th>
                            <th>Комментарий</th>
                            <th>Дата</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($histories as $history):?>
                            <tr>
                                <td><?=$history->status->name?></td>
                                <td><?=$history->comment?></td>
                                <td><?=Yii::$app->formatter->asDatetime($history->created_at)?></td>
                            </tr>
                        <?php endforeach; ?>

                        </tbody>
                    </table>
                </div>
                <?php $form2 = ActiveForm::begin(); ?>

                <?= $form2->field($new_history, 'comment')->textarea() ?>

                <?= $form2->field($new_history, 'order_status_id')->dropDownList(\yii\helpers\ArrayHelper::map($statuses, 'id', 'name'), ['prompt' => 'Выберите статус заказа']) ?>

                <div class="form-group">
                    <?= Html::submitButton('Сохранить', ['class' => $new_history->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>

                <?php ActiveForm::end(); ?>
            </div>
        </div>




    </div>

</div>
