<?php

use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\module\ProductCarousel */

$this->title = 'Редактировать карусель товаров: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Карусель товаров', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Редактировать';

$path = Url::to(["product-carousel/products"]);
$script = <<< JS
	$('.find-product-container').hide();

    function showProducts(carousel_id) {
        $.ajax({
            url: '$path',
            cache: false,
            type: "GET",
            data: { 'carousel_id': carousel_id},
            success: function (data) {
                $(".products").html(data);
            }
        });
    };
JS;
$this->registerJs($script, yii\web\View::POS_HEAD);

$path = Url::to(["product-carousel/find"]);
$script = <<< JS
    
    $('#find-product').on('keyup', function () {   
		$('.find-product-container').show(300); 
		$('.find-product-container .loader').show();
		$('.loader').show();
		var product = $(this).val();
		var carousel_id = {$model->id};
		console.log(product);
     
        $.ajax({
            url: '$path',
            type: "GET",
            data: {'product': product, 'carousel_id': carousel_id},
            success: function (data) {
                $(".find-product-container div").html(data);
		        $('.find-product-container .loader').hide();
                $(".products").html(data);
		        $('.find-product-container').hide(300); 
                
            }
        });
    });

    $(function(){
      $(document).click(function(event) {
        if ($(event.target).closest(".find-product-container").length) return;
        $(".find-product-container").hide(300);
        event.stopPropagation();
      });
    });
JS;
$this->registerJs($script, yii\web\View::POS_READY);

$path = Url::to(["product-carousel/add"]);
$script = <<< JS
    function addProductToCarousel(product_id, carousel_id) {  
        $.ajax({
            url: '$path',
            type: "GET",
            data: { 'product_id': product_id, 'carousel_id': carousel_id},
            success: function (data) {
                console.log(data);
                $(".find-product-container div").html(data);
		        $('#find-product').val('');
            }
        });
    }
JS;
$this->registerJs($script, yii\web\View::POS_HEAD);

$script = <<< JS
    showProducts('{$model->id}');
JS;
$this->registerJs($script, yii\web\View::POS_READY);

$path = Url::to(["product-carousel/plus"]);
$script = <<< JS
	$('.find-product-container').hide();

    function productPlus(id) {
        $.ajax({
            url: '$path',
            cache: false,
            type: "GET",
            data: { 'id': id},
            success: function (data) {
                $(".products").html(data);
            }
        });
    };
JS;
$this->registerJs($script, yii\web\View::POS_HEAD);

$path = Url::to(["product-carousel/minus"]);
$script = <<< JS
	$('.find-product-container').hide();

    function productMinus(id) {
        $.ajax({
            url: '$path',
            cache: false,
            type: "GET",
            data: { 'id': id},
            success: function (data) {
                $(".products").html(data);
            }
        });
    };
JS;
$this->registerJs($script, yii\web\View::POS_HEAD);

$path = Url::to(["product-carousel/remove"]);
$script = <<< JS
	$('.find-product-container').hide();

    function productRemove(id) {
        $.ajax({
            url: '$path',
            cache: false,
            type: "GET",
            data: { 'id': id},
            success: function (data) {
                $(".products").html(data);
            }
        });
    };
JS;
$this->registerJs($script, yii\web\View::POS_HEAD);
?>
<div class="product-carousel-update">

    <!-- TAB NAVIGATION -->
    <ul class="nav nav-tabs nav-justified" role="tablist">
        <li class="active"><a href="#tab1" role="tab" data-toggle="tab">Настройки</a></li>
        <li><a href="#tab2" role="tab" data-toggle="tab">Товары</a></li>
    </ul>

    <!-- TAB CONTENT -->
    <div class="tab-content">

        <!--TAB 1-->
        <div class="active tab-pane fade in" id="tab1">

            <?php $form = ActiveForm::begin(); ?>

            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'limit')->input('number') ?>

            <?= $form->field($model, 'item_limit')->input('number') ?>

            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>

        <!--TAB 2-->
        <div class="tab-pane fade" id="tab2">
            <br>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="form-group find-product">
                        <?=Html::label('Название товара', '', ['class' => 'control-label'])?>
                        <?=Html::input('text', "", '', ['class' => 'form-control', 'required' => 1, 'id' => 'find-product'])?>
                        <div class="find-product-container">
                            <p class="loader text-center">
                                <?=Html::img("/image/design/loader.gif", ['class' => 'img-thumbnail'])?>
                            </p>
                            <div></div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="products">
                <!--Контейнер для отображения списка твоаров-->
            </div>
        </div>

    </div>


    <div class="product-carousel-form">

    </div>

</div>
