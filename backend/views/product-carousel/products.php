<?php

use rmrevin\yii\fontawesome\FA;
use yii\helpers\Html;
use yii\helpers\Url;

?>
<div class="table-responsive">
	<table class="table table-hover">
		<thead>
			<tr>
				<th>Фото</th>
				<th>Название</th>
				<th>Артикул</th>
				<th>Сортировка</th>
				<th>Удалить</th>
			</tr>
		</thead>
		<tbody>
        <?php foreach ($products AS $product): ?>
			<tr>
				<td>
                    <?=Html::a(
                        Html::img(
                            Yii::getAlias('@mysite') . '/image/catalog/Product/' . $product->product_id . '/thumb_' . $product->product->image->src,
                            ['class' => 'img-thumbnail', 'width' => '100']
                        ),
                        ['/product/index', 'product_id' => $product->product_id]
                    )
                    ?>
                </td>
				<td><?=$product->product->name?></td>
                <td><?=$product->product->vendor_code?></td>
                <td>
                    <div class="btn-group">
                        <?=Html::button(FA::icon('plus'), ['class' => 'btn btn-sm btn-info', 'onclick' => "productMinus('{$product->id}')"])?>
                        <?=Html::button($product->sort, ['class' => 'btn btn-sm btn-default', 'readonly' => "1"])?>
                        <?=Html::button(FA::icon('plus'), ['class' => 'btn btn-sm btn-info', 'onclick' => "productPlus('{$product->id}')"])?>
                    </div>
                </td>
				<td>
                    <?=Html::button(FA::icon('remove'), ['class' => 'btn btn-sm btn-danger', 'onclick' => "productRemove('{$product->id}')"])?>
                </td>
			</tr>
        <?php endforeach; ?>
		</tbody>
	</table>
</div>
