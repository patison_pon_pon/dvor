<?php

use leandrogehlen\treegrid\TreeGrid;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\catalog\CategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Категории';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="category-index">

    <p class="text-right">
        <?= Html::a('Создать корневую категорию', ['add'], ['class' => 'btn btn-success']) ?>
    </p>
    <?=
    TreeGrid::widget([
        'dataProvider' => $dataProvider,
        'keyColumnName' => 'id',
        'showOnEmpty' => FALSE,
        'parentColumnName' => 'parent_id',
        'pluginOptions' => [
            'initialState' => 'collapsed',
        ],
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

            //'id',
            //'parent_id',
            'name',
            [
                'attribute' => 'belonging_id',
                'format' => 'raw',
                'value' => function ($model) {
                    return $model->belonging->name;
                },
            ],
            [
                'attribute' => 'status',
                'format' => 'raw',
                'value' => function ($model) {
                    return (
                    $model->status
                        ? "<span class='label label-success'>Включен</span>"
                        : "<span class='label label-danger'>Отключен</span>"
                    );
                },
            ],

            ['class' => 'yii\grid\ActionColumn',
                'template' => '{add}&nbsp;{update}&nbsp;{delete}',
                'buttons' => [
                    'add' => function ($url, $model, $key){
                        return Html::a('<span class="glyphicon glyphicon-plus"></span>', ['add', 'id' => $model->id, 'belonging_id' => $model->belonging_id], [
                            'class' => 'btn btn-warning btn-sm',
                            'title' => 'Добавить дочернюю',
                        ]);
                    },
                    'update' => function($url, $model){
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', ['update', 'id' => $model->id], [
                            'class' => 'btn btn-primary btn-sm',
                            'title' => 'Редактировать',
                        ]);
                    },
                    'view' => function($url, $model){
                        return Html::a('<span class="fa fa-eye"></span>', ['view', 'id' => $model->id], [
                            'class' => 'btn btn-success btn-sm',
                            'title' => 'Редактировать',
                        ]);
                    },
                    'delete' => function($url, $model){
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', ['delete', 'id' => $model->id], [
                            'class' => 'btn btn-danger btn-sm',
                            'title' => 'Удалить',
                            'data' => [
                                'confirm' => 'Уверены что хотите удлить?',
                                'method' => 'post',
                            ],
                        ]);
                    }
                ]
            ]
        ],
    ]); ?>
</div>
