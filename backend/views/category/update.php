<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\catalog\Category */

$this->title = 'Редактировать категорию: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Категории', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Редактировать';
?>
<div class="category-update">

    <div class="category-form">

        <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'parent_id')->dropDownList($categories, ['prompt' => 'Выберите родителя']) ?>

        <?= $form->field($model, 'belonging_id')->dropDownList(\yii\helpers\ArrayHelper::map($belongings, 'id', 'name'), ['prompt' => 'Выберите принадлежность']) ?>

        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'meta_h1')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'meta_t')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'meta_k')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'meta_d')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

        <?= $form->field($model, 'status')->dropDownList(['1' => 'Включен', '0' => 'Отключен']) ?>

        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>

</div>
