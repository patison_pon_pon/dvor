<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $model backend\models\catalog\Belonging */

$this->title = 'Создать поставку';
$this->params['breadcrumbs'][] = ['label' => 'Поставки', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="belonging-create">

    <div class="belonging-form">

        <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'meta_h1')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'meta_t')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'meta_k')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'meta_d')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>

</div>
