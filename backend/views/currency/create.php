<?php



/* @var $this yii\web\View */
/* @var $model backend\models\shop\Currency */

$this->title = 'Создать валюту';
$this->params['breadcrumbs'][] = ['label' => 'Валюты', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="currency-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
