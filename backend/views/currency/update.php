<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\shop\Currency */

$this->title = 'Редактировать валюту: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Валюты', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Редактировать';
?>
<div class="currency-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
