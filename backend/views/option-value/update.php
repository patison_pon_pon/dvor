<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\catalog\OptionValue */

$this->title = 'Редактировать значение опции: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Опции', 'url' => ['option/index']];
$this->params['breadcrumbs'][] = ['label' => $model->option->name, 'url' => ['option/updtae', 'id' => $model->option->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="option-value-update">

    <div class="option-form">

        <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'sort')->input('number', ['value' => 5]) ?>

        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>

</div>
