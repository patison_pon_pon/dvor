<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\shop\CustomerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Покупатели';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="customer-index">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [

            [
                'attribute' => 'id',
                'headerOptions' => ['width' => '80'],
            ],
            'username',
            'email:email',
            'phone',
            [
                'attribute' => 'manager_id',
                'format' => 'ntext',
                'value' => function ($model) {
                    return $model->manager->name;
                },
                'filter' => '',
            ],
            [
                'attribute' => 'discount',
                'format' => 'ntext',
                'value' => function ($model) {
                    return $model->discount . ' %';
                },
                'filter' => '',
            ],
            [
                'attribute' => 'business',
                'format' => 'ntext',
                'value' => function ($model) {
                    return $model->business;
                },
                'filter' => '',
            ],
            [
                'attribute' => 'geo',
                'format' => 'ntext',
                'value' => function ($model) {
                    return $model->geo;
                },
                'filter' => '',
            ],
            // 'auth_key',
            // 'password_hash',
            // 'password_reset_token',
            // 'email:email',
            // 'status',
            [
                'attribute' => 'created_at',
                'format' => 'datetime',
                'value' => function ($model) {
                    return $model->created_at;
                },
                'filter' => '',
            ],
            // 'updated_at',

            [
                'class' => 'yii\grid\ActionColumn',
                'header'=>'Действия',
                'headerOptions' => ['width' => '90'],
                'template' => '{update}&nbsp;{delete}',
                'buttons' => [
                    'update' => function($url, $model){
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', ['update', 'id' => $model->id], [
                            'class' => 'btn btn-primary btn-sm',
                            'title' => 'Редактировать',
                        ]);
                    },
                    'view' => function($url, $model){
                        return Html::a('<span class="fa fa-eye"></span>', ['view', 'id' => $model->id], [
                            'class' => 'btn btn-success btn-sm',
                            'title' => 'Редактировать',
                        ]);
                    },
                    'delete' => function($url, $model){
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', ['delete', 'id' => $model->id], [
                            'class' => 'btn btn-danger btn-sm',
                            'title' => 'Удалить',
                            'data' => [
                                'confirm' => 'Уверены что хотите удлить?',
                                'method' => 'post',
                            ],
                        ]);
                    }
                ],
            ],
        ],
    ]); ?>
</div>
