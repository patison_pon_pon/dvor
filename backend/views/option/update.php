<?php

use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model backend\models\catalog\Option */

$this->title = 'Редактировать опцию: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Опции', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Редактировать';
$script = <<< JS
    $("document").ready(function(){
            $("#new_option_value").on("pjax:end", function() {
            $.pjax.reload({container:"#option_values"});  //Reload GridView
        });
    });
JS;
$this->registerJs($script, yii\web\View::POS_READY);
?>
<div class="option-update">

    <!-- TAB NAVIGATION -->
    <ul class="nav nav-tabs nav-justified" role="tablist">
        <li class="active"><a href="#tab1" role="tab" data-toggle="tab">Значения опции</a></li>
        <li><a href="#tab2" role="tab" data-toggle="tab">Настройки опции</a></li>
    </ul>

    <!-- TAB CONTENT -->
    <div class="tab-content">
        <div class="active tab-pane fade in" id="tab1">
            <h4>Значения опции</h4>
            <?php Pjax::begin(['id' => 'new_option_value']) ?>

            <?php $form2 = ActiveForm::begin(['options' => ['data-pjax' => true]]); ?>

            <?= $form2->field($value, 'name')->textInput() ?>

            <?= $form2->field($value, 'sort')->input('number', ['value' => 5]) ?>

            <div class="form-group">
                <?= Html::submitButton('Создать', ['class' => 'btn btn-primary']) ?>
            </div>

            <?php ActiveForm::end(); ?>
            <?php Pjax::end() ?>

            <?php Pjax::begin(['id' => 'option_values']) ?>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    //'id',
                    'name',
                    //'url:url',
                    'sort',


                    [
                        'class' => 'yii\grid\ActionColumn',
                        'header'=>'Действия',
                        'headerOptions' => ['width' => '90'],
                        'template' => '{update}&nbsp;{delete}',
                        'buttons' => [
                            'update' => function($url, $model){
                                return Html::a('<span class="glyphicon glyphicon-pencil"></span>', ['option-value/update', 'id' => $model->id], [
                                    'class' => 'btn btn-primary btn-sm',
                                    'title' => 'Редактировать',
                                ]);
                            },
                            'delete' => function($url, $model){
                                return Html::a('<span class="glyphicon glyphicon-trash"></span>', ['option-value/delete', 'id' => $model->id], [
                                    'class' => 'btn btn-danger btn-sm',
                                    'title' => 'Удалить',
                                    'data' => [
                                        'confirm' => 'Уверены что хотите удлить?',
                                        'method' => 'post',
                                    ],
                                ]);
                            }
                        ],
                    ],
                ],
            ]); ?>
            <?php Pjax::end() ?>
        </div>
        <div class="tab-pane fade" id="tab2">
            <h4>Настройки опции</h4>
            <div class="option-form">

                <?php $form = ActiveForm::begin(); ?>

                <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'sort')->input('number', ['value' => 5]) ?>

                <div class="form-group">
                    <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>

                <?php ActiveForm::end(); ?>

            </div>
        </div>
    </div>



</div>
