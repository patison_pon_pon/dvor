<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\shop\Manager */

$this->title = 'Создать менеджера';
$this->params['breadcrumbs'][] = ['label' => 'Менеджеры', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="manager-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
