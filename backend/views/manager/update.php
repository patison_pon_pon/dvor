<?php

/* @var $this yii\web\View */
/* @var $model backend\models\shop\Manager */

$this->title = 'Редактировать менеджера: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Менеджеры', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Редактировать';
?>
<div class="manager-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
