<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\module\CallMe */

$this->title = 'Редактировать: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Перезвоните мне', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Редактировать';
?>
<div class="call-me-update">

    <div class="call-me-form">

        <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'answered')->dropDownList(['1' => 'Отвечен' , '0' => 'Не отвечен']) ?>

        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>

</div>
