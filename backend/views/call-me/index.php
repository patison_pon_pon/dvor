<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\module\CallMeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Перезвоните мне';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="call-me-index">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

            //'id',
            'phone',
            [
                'attribute' => 'answered',
                'format' => 'raw',
                'value' => function ($model) {
                    return ($model->answered
                            ? "<span class='label label-success'>Отвечен</span>"
                            : "<span class='label label-danger'>Не отвечен</span>"
                    );
                },
                'filter' => ['1' => 'Отвечен' , '0' => 'Не отвечен'],
            ],
            'created_at:datetime',

            [
                'class' => 'yii\grid\ActionColumn',
                'header'=>'Действия',
                'headerOptions' => ['width' => '90'],
                'template' => '{update}&nbsp;{delete}',
                'buttons' => [
                    'update' => function($url, $model){
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', ['update', 'id' => $model->id], [
                            'class' => 'btn btn-primary btn-sm',
                            'title' => 'Редактировать',
                        ]);
                    },
                    'view' => function($url, $model){
                        return Html::a('<span class="fa fa-eye"></span>', ['view', 'id' => $model->id], [
                            'class' => 'btn btn-success btn-sm',
                            'title' => 'Редактировать',
                        ]);
                    },
                    'delete' => function($url, $model){
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', ['delete', 'id' => $model->id], [
                            'class' => 'btn btn-danger btn-sm',
                            'title' => 'Удалить',
                            'data' => [
                                'confirm' => 'Уверены что хотите удлить?',
                                'method' => 'post',
                            ],
                        ]);
                    }
                ],
            ],
        ],
    ]); ?>
</div>
