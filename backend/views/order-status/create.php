<?php



/* @var $this yii\web\View */
/* @var $model backend\models\order\OrderStatus */

$this->title = 'Создать статус заказ';
$this->params['breadcrumbs'][] = ['label' => 'Статусы заказа', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-status-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
