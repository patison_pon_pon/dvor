<?php

/* @var $this yii\web\View */
/* @var $model backend\models\order\OrderStatus */

$this->title = 'Редактировать статус заказа: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Статусы заказа', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Редактировать';
?>
<div class="order-status-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
