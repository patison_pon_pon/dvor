<?php
/**
 * Created by PhpStorm.
 * User: patison
 * Date: 25.12.17
 * Time: 15:25
 */

namespace backend\models\module;


use backend\models\catalog\Product;

class ProductCarouselProduct extends \common\tables\ProductCarouselProduct
{
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }
}