<?php
/**
 * Created by PhpStorm.
 * User: patison
 * Date: 24.11.17
 * Time: 17:54
 */

namespace backend\models\order;


use backend\models\catalog\Supply;
use backend\models\catalog\Unit;
use backend\models\shop\Currency;
use backend\models\shop\Customer;

class Order extends \common\tables\Order
{
    public function getUnit()
    {
        return $this->hasOne(Unit::className(), ['id' => 'unit_id']);
    }
    public function getCustomer()
    {
        return $this->hasOne(Customer::className(), ['id' => 'customer_id']);
    }
    public function getStatus()
    {
        return $this->hasOne(OrderStatus::className(), ['id' => 'order_status_id']);
    }
    public function getCurrency()
    {
        return $this->hasOne(Currency::className(), ['id' => 'currency_id']);
    }
}