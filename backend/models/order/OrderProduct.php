<?php
/**
 * Created by PhpStorm.
 * User: patison
 * Date: 24.11.17
 * Time: 17:58
 */

namespace backend\models\order;


use backend\models\catalog\Product;
use backend\models\catalog\ProductImage;
use backend\models\catalog\Supply;

class OrderProduct extends \common\tables\OrderProduct
{

    public function getSupply()
    {
        return $this->hasOne(Supply::className(), ['id' => 'supply_id']);
    }
    public function getImage()
    {
        return $this->hasOne(ProductImage::className(), ['product_id' => 'product_id'])->orderBy(['sort' => SORT_ASC]);
    }
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }
}