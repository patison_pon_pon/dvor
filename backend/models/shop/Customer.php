<?php
/**
 * Created by PhpStorm.
 * User: patison
 * Date: 06.12.17
 * Time: 17:56
 */

namespace backend\models\shop;


class Customer extends \common\tables\Customer
{

    public function getManager()
    {
        return $this->hasOne(Manager::className(), ['id' => 'manager_id']);
    }
}