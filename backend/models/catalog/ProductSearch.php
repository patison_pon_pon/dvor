<?php

namespace backend\models\catalog;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\catalog\Product;

/**
 * ProductSearch represents the model behind the search form about `backend\models\catalog\Product`.
 */
class ProductSearch extends Product
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'belonging_id', 'category_id', 'quantity', 'minimum', 'status', 'created_at'], 'integer'],
            [['name', 'vendor_code', 'description'], 'safe'],
            [['price', 'weight'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Product::find()->with('category', 'belonging');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'belonging_id' => $this->belonging_id,
            'category_id' => $this->category_id,
            'price' => $this->price,
            'vendor_code' => $this->vendor_code,
            'quantity' => $this->quantity,
            'minimum' => $this->minimum,
            'weight' => $this->weight,
            'status' => $this->status,
            'created_at' => $this->created_at,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'description', $this->description]);

        return $dataProvider;
    }
}
