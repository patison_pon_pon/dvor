<?php
/**
 * Created by PhpStorm.
 * User: patison
 * Date: 07.11.17
 * Time: 16:54
 */

namespace backend\models\catalog;


class Product extends \common\tables\Product
{
    public function behaviors(){
        return [
            'slug' => [
                'class' => 'Zelenin\yii\behaviors\Slug',
                'slugAttribute' => 'url',
                'attribute' => 'name',
                // optional params
                'ensureUnique' => true,
                'replacement' => '-',
                'lowercase' => true,
                'immutable' => false,
                // If intl extension is enabled, see http://userguide.icu-project.org/transforms/general.
                'transliterateOptions' => 'Russian-Latin/BGN; Any-Latin; Latin-ASCII; NFD; [:Nonspacing Mark:] Remove; NFC;'
            ]
        ];
    }
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }

    public function getBelonging()
    {
        return $this->hasOne(Belonging::className(), ['id' => 'belonging_id']);
    }

    public function getImage()
    {
        return $this->hasOne(ProductImage::className(), ['product_id' => 'id'])->orderBy(['sort' => SORT_ASC]);
    }

}