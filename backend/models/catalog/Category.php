<?php
/**
 * Created by PhpStorm.
 * User: patison
 * Date: 07.11.17
 * Time: 16:53
 */

namespace backend\models\catalog;


class Category extends \common\tables\Category
{

    public function behaviors(){
        return [
            'slug' => [
                'class' => 'Zelenin\yii\behaviors\Slug',
                'slugAttribute' => 'url',
                'attribute' => ($this->isNewRecord ? 'name' : 'url'),
                // optional params
                'ensureUnique' => true,
                'replacement' => '-',
                'lowercase' => true,
                'immutable' => false,
                // If intl extension is enabled, see http://userguide.icu-project.org/transforms/general.
                'transliterateOptions' => 'Russian-Latin/BGN; Any-Latin; Latin-ASCII; NFD; [:Nonspacing Mark:] Remove; NFC;'
            ]
        ];
    }

    public function myname($id)
    {
        $model = Category::find()
            ->where(['id' => $id])
            ->one()->name;
        return $model;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTree()
    {
        return $this->hasOne(Category::className(), ['id' => 'parent_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTrees()
    {
        return $this->hasMany(Category::className(), ['parent_id' => 'id']);
    }
    public function getBelonging()
    {
        return $this->hasOne(Belonging::className(), ['id' => 'belonging_id']);
    }
}