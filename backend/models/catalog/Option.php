<?php
/**
 * Created by PhpStorm.
 * User: patison
 * Date: 22.02.18
 * Time: 10:21
 */

namespace backend\models\catalog;


class Option extends \common\tables\Option
{
    public function behaviors(){
        return [
            'slug' => [
                'class' => 'Zelenin\yii\behaviors\Slug',
                'slugAttribute' => 'url',
                'attribute' => ($this->isNewRecord ? 'name' : 'url'),
                // optional params
                'ensureUnique' => true,
                'replacement' => '-',
                'lowercase' => true,
                'immutable' => false,
                // If intl extension is enabled, see http://userguide.icu-project.org/transforms/general.
                'transliterateOptions' => 'Russian-Latin/BGN; Any-Latin; Latin-ASCII; NFD; [:Nonspacing Mark:] Remove; NFC;'
            ]
        ];
    }

}