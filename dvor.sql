-- phpMyAdmin SQL Dump
-- version 4.7.3
-- https://www.phpmyadmin.net/
--
-- Хост: localhost
-- Время создания: Апр 18 2018 г., 18:14
-- Версия сервера: 10.1.26-MariaDB-0+deb9u1
-- Версия PHP: 7.0.27-0+deb9u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `dvor`
--

-- --------------------------------------------------------

--
-- Структура таблицы `auth_assignment`
--

CREATE TABLE `auth_assignment` (
  `item_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `auth_assignment`
--

INSERT INTO `auth_assignment` (`item_name`, `user_id`, `created_at`) VALUES
('ADMINISTRATOR', '1', 1512652774);

-- --------------------------------------------------------

--
-- Структура таблицы `auth_item`
--

CREATE TABLE `auth_item` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `type` smallint(6) NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `rule_name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` blob,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `auth_item`
--

INSERT INTO `auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES
('ADMINISTRATOR', 1, 'Администратор', NULL, NULL, 1512652461, 1512652461),
('MANAGER', 1, 'Менеджер', NULL, NULL, 1512652461, 1512652461);

-- --------------------------------------------------------

--
-- Структура таблицы `auth_item_child`
--

CREATE TABLE `auth_item_child` (
  `parent` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `child` varchar(64) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `auth_rule`
--

CREATE TABLE `auth_rule` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `data` blob,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `belonging`
--

CREATE TABLE `belonging` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `meta_h1` varchar(255) NOT NULL,
  `meta_t` varchar(255) NOT NULL,
  `meta_k` varchar(255) NOT NULL,
  `meta_d` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `url` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `belonging`
--

INSERT INTO `belonging` (`id`, `name`, `meta_h1`, `meta_t`, `meta_k`, `meta_d`, `description`, `url`) VALUES
(1, 'Товары', 'Товары', 'Товары', '', '', '', 'tovary'),
(2, 'Материалы', 'Материалы', 'Материалы', '', '', '', 'materialy'),
(3, 'Услуги', 'Услуги', 'Услуги', '', '', '', 'uslugi');

-- --------------------------------------------------------

--
-- Структура таблицы `call_me`
--

CREATE TABLE `call_me` (
  `id` int(11) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `answered` int(11) NOT NULL,
  `created_at` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `belonging_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `meta_h1` varchar(255) NOT NULL,
  `meta_t` varchar(255) NOT NULL,
  `meta_k` varchar(255) NOT NULL,
  `meta_d` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `url` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `category`
--

INSERT INTO `category` (`id`, `parent_id`, `belonging_id`, `name`, `meta_h1`, `meta_t`, `meta_k`, `meta_d`, `description`, `url`, `status`) VALUES
(1, NULL, 1, 'Плитка', 'Плитка', 'Плитка', '', '', '', 'plitka', 1),
(10, NULL, 1, 'Кирпич', 'Кирпич', 'Кирпич', '', '', '', 'kirpich', 1),
(11, NULL, 1, 'Пенобетон', 'Пенобетон', 'Пенобетон', '', '', '', 'penobeton', 1),
(12, NULL, 2, 'Песок', 'Песок', 'Песок', '', '', '', 'pesok', 1),
(13, NULL, 2, 'Щебень', 'Щебень', 'Щебень', '', '', '', 'shcheben', 1),
(25, NULL, 2, 'Вспомогательные материалы', 'Вспомогательные материалы', 'Вспомогательные материалы', '', '', '', 'vspomogatel-nyye-materialy', 1),
(26, NULL, 3, 'Монтажные работы', 'Монтажные работы', 'Монтажные работы', 'Монтажные работы', '', '', 'montazhnyye-raboty', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `currency`
--

CREATE TABLE `currency` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `code` varchar(3) NOT NULL,
  `value` decimal(10,3) NOT NULL,
  `symbol` varchar(16) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `currency`
--

INSERT INTO `currency` (`id`, `name`, `code`, `value`, `symbol`) VALUES
(1, 'Доллар США', 'USD', '1.000', '$'),
(2, 'Евро', 'EUR', '0.890', '€'),
(3, 'Гривна', 'UAH', '28.000', 'грн.');

-- --------------------------------------------------------

--
-- Структура таблицы `customer`
--

CREATE TABLE `customer` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `business` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `geo` text COLLATE utf8_unicode_ci,
  `manager_id` int(11) DEFAULT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `discount` decimal(10,2) NOT NULL DEFAULT '1.00',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `customer`
--

INSERT INTO `customer` (`id`, `username`, `phone`, `business`, `geo`, `manager_id`, `auth_key`, `password_hash`, `password_reset_token`, `email`, `status`, `discount`, `created_at`, `updated_at`) VALUES
(9, 'Артем', '0937802230', 'Белье', 'awdawdwa', 1, 'Nb5x0MfVk3-EOLz8cNmk9v_Gva206GRv', '96e79218965eb72c92a549dd5a330112', NULL, 'patison.pon.pon@mail.ru', 10, '1.00', 1520423864, 1520423864);

-- --------------------------------------------------------

--
-- Структура таблицы `info`
--

CREATE TABLE `info` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `meta_h1` varchar(255) NOT NULL,
  `meta_t` varchar(255) NOT NULL,
  `meta_k` varchar(255) NOT NULL,
  `meta_d` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `created_at` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `info`
--

INSERT INTO `info` (`id`, `name`, `url`, `meta_h1`, `meta_t`, `meta_k`, `meta_d`, `description`, `created_at`) VALUES
(1, 'О нас', 'o-nas', '', 'О нас', '', '', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p><p><img src=\"http://tosopt/image/catalog/info/5a37c2ec1c4a8.jpg\" style=\"width: 547px; height: 314px; float: right; margin: 0px 0px 10px 10px;\" width=\"547\" height=\"314\" alt=\"\"><span class=\"redactor-invisible-space\">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.<br></span></p><p><span class=\"redactor-invisible-space\"><span style=\"color: #548dd4;\">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.<span class=\"redactor-invisible-space\"></span><br></span></span></p><p><span class=\"redactor-invisible-space\"><span style=\"color: #548dd4;\"><span class=\"redactor-invisible-space\"><span style=\"background-color: #ffff00;\"><span style=\"color: #000000;\">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.<span class=\"redactor-invisible-space\"></span></span><br></span></span></span></span></p><p><span class=\"redactor-invisible-space\"><span style=\"color: #548dd4;\"><span class=\"redactor-invisible-space\"><span style=\"background-color: #ffff00;\"><span style=\"color: #000000;\"><img src=\"http://tosopt/image/catalog/info/5a37d142e91b1.jpg\" style=\"width: 439px; height: 248px; display: block; margin: auto;\" width=\"439\" height=\"248\" alt=\"\"><br></span></span></span></span></span></p>', 1510672811),
(2, 'Доставка', 'dostavka', '', 'Способы доставки', '', '', '<p><img src=\"http://tosopt/image/catalog/info/5a37c204578f7.jpg\" width=\"445\" height=\"252\" alt=\"\" style=\"width: 445px; height: 252px; float: left; margin: 0px 10px 10px 0px;\"><span style=\"background-color: rgb(255, 255, 0);\">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Culpa doloribus tempora cupiditate quod dolorum sapiente fuga, blanditiis recusandae suscipit assumenda odio sit. Perspiciatis dolor nesciunt esse repellat eos, atque quidem.</span>\r\n</p><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Culpa doloribus tempora cupiditate quod dolorum sapiente fuga, blanditiis recusandae suscipit assumenda odio sit. Perspiciatis dolor nesciunt esse repellat eos, atque quidem.<br>\r\n</p><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Culpa doloribus tempora cupiditate quod dolorum sapiente fuga, blanditiis recusandae suscipit assumenda odio sit. Perspiciatis dolor nesciunt esse repellat eos, atque quidem.\r\n</p>', 1512389966),
(3, 'Контакты', 'kontakty', '', 'Контакты', '', '', '<p>Контакты</p>', 1514477125);

-- --------------------------------------------------------

--
-- Структура таблицы `manager`
--

CREATE TABLE `manager` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `manager`
--

INSERT INTO `manager` (`id`, `name`, `email`, `phone`) VALUES
(1, 'Артем', 'patison.pon.pon@mail.ru', '0937802230'),
(2, 'Дмитрий Смиренский', 'sale@tosopt.com', '098 575 80 85');

-- --------------------------------------------------------

--
-- Структура таблицы `migration`
--

CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1509619044),
('m130524_201442_init', 1509619049),
('m140506_102106_rbac_init', 1512652321),
('m170907_052038_rbac_add_index_on_auth_assignment_user_id', 1512652321),
('m180406_114807_unit', 1523020215);

-- --------------------------------------------------------

--
-- Структура таблицы `option`
--

CREATE TABLE `option` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `sort` int(11) NOT NULL DEFAULT '5'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `option`
--

INSERT INTO `option` (`id`, `name`, `url`, `sort`) VALUES
(1, 'Цвет', 'tsvet', 5),
(2, 'Размер', 'razmer', 6),
(3, 'Размер штаны', 'razmer-shtany', 5);

-- --------------------------------------------------------

--
-- Структура таблицы `option_value`
--

CREATE TABLE `option_value` (
  `id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `sort` int(11) NOT NULL DEFAULT '5'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `option_value`
--

INSERT INTO `option_value` (`id`, `option_id`, `name`, `url`, `sort`) VALUES
(3, 1, 'Белый', 'belyy', 5),
(4, 1, 'Зеленый', 'zelenyy', 5),
(5, 2, 'XS', 'xs', 5),
(6, 2, 'S', 's', 6),
(7, 2, 'M', 'm', 7),
(8, 2, 'L', 'l', 8),
(9, 2, 'XL', 'xl', 9),
(10, 2, 'XXL', 'xxl', 10),
(11, 1, 'Желтый', 'zheltyy', 5),
(12, 1, 'Красный', 'krasnyy', 5),
(13, 1, 'Черный', 'chernyy', 5),
(14, 1, 'Бежевый', 'bezhevyy', 5),
(15, 3, '32', '32', 5),
(16, 3, '34', '34', 5),
(17, 3, '36', '36', 5),
(18, 3, '38', '38', 5),
(19, 1, 'Бирюзовый', 'biryuzovyy', 5),
(20, 1, 'Серый', 'seryy', 5);

-- --------------------------------------------------------

--
-- Структура таблицы `order`
--

CREATE TABLE `order` (
  `id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `unit_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `region` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `comment` text NOT NULL,
  `quantity` int(11) NOT NULL,
  `weight` decimal(10,2) NOT NULL,
  `sum` decimal(10,3) NOT NULL,
  `currency_id` int(11) NOT NULL,
  `order_status_id` int(11) NOT NULL,
  `created_at` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `order`
--

INSERT INTO `order` (`id`, `customer_id`, `unit_id`, `name`, `phone`, `region`, `city`, `address`, `comment`, `quantity`, `weight`, `sum`, `currency_id`, `order_status_id`, `created_at`) VALUES
(1, 9, 1, 'Я лично', '000', 'йцвйцв', 'йцвйцвйц', 'wdaw', 'dawdawd', 40, '31.20', '374.236', 1, 1, 1524060260),
(2, 9, 1, 'Я лично', '000', 'йцвйцв', 'йцвйцвйц', 'wadwa', 'dawdawd', 20, '15.60', '5239.304', 3, 1, 1524060333),
(3, 9, 1, 'Я лично', '000', 'йцвйцв', 'йцвйцвйц', 'awdwadwa', 'awdawdawd', 20, '15.60', '166.535', 2, 1, 1524060401),
(4, 9, 2, 'Я лично', '000', 'йцвйцв', 'йцвйцвйц', 'awdwadwa', 'awdawdawd', 20, '20.00', '176.220', 2, 1, 1524060401);

-- --------------------------------------------------------

--
-- Структура таблицы `order_history`
--

CREATE TABLE `order_history` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `order_status_id` int(11) NOT NULL,
  `comment` text NOT NULL,
  `created_at` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `order_history`
--

INSERT INTO `order_history` (`id`, `order_id`, `order_status_id`, `comment`, `created_at`) VALUES
(1, 1, 1, 'Заказ оформлен', 1524060260),
(2, 2, 1, 'Заказ оформлен', 1524060333),
(3, 3, 1, 'Заказ оформлен', 1524060401),
(4, 4, 1, 'Заказ оформлен', 1524060401);

-- --------------------------------------------------------

--
-- Структура таблицы `order_product`
--

CREATE TABLE `order_product` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `price` decimal(10,3) NOT NULL,
  `sum` decimal(10,3) NOT NULL,
  `options` longtext
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `order_product`
--

INSERT INTO `order_product` (`id`, `order_id`, `product_id`, `quantity`, `price`, `sum`, `options`) VALUES
(1, 1, 23, 40, '5.356', '214.236', 's:0:\"\";'),
(2, 2, 23, 20, '149.965', '2999.304', 's:0:\"\";'),
(3, 3, 23, 20, '4.767', '95.335', 's:0:\"\";'),
(4, 4, 20, 20, '8.811', '176.220', 'a:1:{i:39;a:4:{s:17:\"product_option_id\";i:39;s:19:\"product_option_name\";s:8:\"Цвет\";s:23:\"product_option_value_id\";i:93;s:25:\"product_option_value_name\";s:10:\"Белый\";}}');

-- --------------------------------------------------------

--
-- Структура таблицы `order_status`
--

CREATE TABLE `order_status` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `order_status`
--

INSERT INTO `order_status` (`id`, `name`) VALUES
(1, 'Ожидание'),
(2, 'В обработке');

-- --------------------------------------------------------

--
-- Структура таблицы `order_sum`
--

CREATE TABLE `order_sum` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `action` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `value` decimal(10,3) NOT NULL,
  `created_at` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `order_sum`
--

INSERT INTO `order_sum` (`id`, `order_id`, `action`, `description`, `value`, `created_at`) VALUES
(1, 1, 'plus', 'Сумма', '214.236', 1524060260),
(2, 1, 'plus', 'Поддон(x32)', '160.000', 1524060260),
(3, 2, 'plus', 'Сумма', '2999.304', 1524060333),
(4, 2, 'plus', 'Поддон(x16)', '2240.000', 1524060333),
(5, 3, 'plus', 'Сумма', '95.335', 1524060401),
(6, 3, 'plus', 'Поддон(x16)', '71.200', 1524060401),
(7, 4, 'plus', 'Сумма', '176.220', 1524060401);

-- --------------------------------------------------------

--
-- Структура таблицы `product`
--

CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `belonging_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `unit_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `vendor_code` varchar(255) DEFAULT NULL,
  `description` text NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `quantity` int(11) NOT NULL DEFAULT '1000',
  `minimum` int(11) NOT NULL,
  `weight` decimal(10,2) NOT NULL,
  `stock` int(11) NOT NULL DEFAULT '1',
  `status` int(1) NOT NULL DEFAULT '1',
  `created_at` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `product`
--

INSERT INTO `product` (`id`, `belonging_id`, `category_id`, `unit_id`, `name`, `url`, `vendor_code`, `description`, `price`, `quantity`, `minimum`, `weight`, `stock`, `status`, `created_at`) VALUES
(7, 1, 1, 1, 'Плитка 1', 'plitka-1', 'piastrella_1', '', '54.48', 1000, 20, '0.20', 0, 1, 1510156278),
(8, 1, 1, 1, 'Плитка 2', 'plitka-2', 'piastrella_2', '', '2.50', 1000, 20, '0.20', 1, 1, 1510670949),
(9, 1, 1, 1, 'Плитка 3', 'plitka-3', 'piastrella_3', '', '4.10', 1000, 20, '0.20', 1, 1, 1510670995),
(10, 1, 1, 1, 'Плитка 4', 'plitka-4', 'piastrella_4', '', '1.10', 1000, 20, '0.20', 1, 1, 1510671060),
(11, 1, 1, 1, 'Плитка 5', 'plitka-5', 'piastrella_5', '', '0.50', 1000, 20, '0.20', 0, 0, 1510671120),
(12, 1, 1, 1, 'Плитка 6', 'plitka-6', 'piastrella_6', 'Очень интересное описание', '2.00', 1000, 20, '0.20', 1, 1, 1510671195),
(13, 1, 1, 1, 'Плитка 7', 'plitka-7', 'piastrella_7', '', '4.80', 1000, 20, '0.20', 1, 1, 1510671286),
(14, 1, 1, 1, 'Плитка 8', 'plitka-8', 'piastrella_8', '', '0.07', 1000, 20, '0.23', 1, 1, 1510921314),
(15, 1, 1, 3, 'Плитка 9', 'plitka-9', 'piastrella_9', '', '5.47', 1000, 20, '0.33', 1, 1, 1510921468),
(16, 1, 1, 1, 'Плитка 10', 'plitka-10', 'piastrella_10', '', '10.00', 1000, 20, '1.00', 1, 1, 1511886857),
(17, 1, 1, 1, 'Плитка 11', 'plitka-11', 'piastrella_11', '', '5.00', 1000, 20, '0.50', 1, 1, 1511886922),
(18, 1, 1, 1, 'Плитка 12', 'plitka-12', 'piastrella_12', '', '20.00', 1000, 20, '0.70', 1, 1, 1511886966),
(19, 1, 1, 3, 'Плитка 13', 'plitka-13', 'piastrella_13', '', '5.40', 1000, 20, '0.28', 1, 1, 1511965290),
(20, 1, 1, 1, 'Плитка 14', 'plitka-14', 'piastrella_14', '', '10.00', 1000, 10, '1.00', 1, 1, 1512389590),
(21, 1, 1, 1, 'Плитка 15', 'plitka-15', 'piastrella_15', '', '10.00', 1000, 3, '1.00', 1, 1, 1512389623),
(22, 1, 1, 1, 'Плитка 16', 'plitka-16', 'piastrella_16', 'Из говна оно сделано', '43.00', 1000, 1, '0.57', 0, 1, 1517398146),
(23, 1, 1, 1, 'Плитка 17', 'plitka-17', 'piastrella_17', '1', '5.41', 1000, 5, '0.78', 1, 1, 1517909966),
(24, 3, 26, 3, 'Монтаж окон', 'montazh-okon', '', '', '200.00', 1000, 1, '0.00', 1, 1, 1524064210),
(26, 2, 12, 2, 'Песок желтый', 'pesok-zheltyy', '', '', '2.00', 1000, 1, '0.70', 1, 1, 1524064311),
(27, 2, 13, 2, 'Щебень мелкий', 'shcheben-melkiy', '', '', '3.00', 1000, 1, '0.80', 1, 1, 1524064362);

-- --------------------------------------------------------

--
-- Структура таблицы `product_carousel`
--

CREATE TABLE `product_carousel` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `limit` int(11) NOT NULL,
  `item_limit` int(11) NOT NULL,
  `created_at` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `product_carousel`
--

INSERT INTO `product_carousel` (`id`, `name`, `description`, `limit`, `item_limit`, `created_at`) VALUES
(2, 'Новинки на главной', 'Новинки', 9, 3, 1514467174);

-- --------------------------------------------------------

--
-- Структура таблицы `product_carousel_product`
--

CREATE TABLE `product_carousel_product` (
  `id` int(11) NOT NULL,
  `product_carousel_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `sort` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `product_carousel_product`
--

INSERT INTO `product_carousel_product` (`id`, `product_carousel_id`, `product_id`, `sort`) VALUES
(17, 2, 10, 4),
(18, 2, 16, 3),
(19, 2, 17, 5),
(20, 2, 7, 7),
(21, 2, 8, 6);

-- --------------------------------------------------------

--
-- Структура таблицы `product_image`
--

CREATE TABLE `product_image` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `src` varchar(255) NOT NULL,
  `sort` int(11) NOT NULL DEFAULT '3'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `product_image`
--

INSERT INTO `product_image` (`id`, `product_id`, `image`, `src`, `sort`) VALUES
(6, 7, '', '1510232989.jpg', 3),
(7, 7, '', '1510233003.jpg', 3),
(8, 7, '', '1510233341.jpg', 3),
(9, 7, '', '1510233358.jpg', 3),
(10, 7, '', '1510233374.jpg', 3),
(12, 7, '', '1510234032.jpg', 3),
(13, 8, '', '1510670958.jpg', 3),
(14, 8, '', '1510670973.jpg', 3),
(15, 9, '', '1510671023.jpeg', 3),
(16, 9, '', '1510671032.jpg', 3),
(17, 10, '', '1510671071.jpg', 3),
(18, 10, '', '1510671086.jpg', 3),
(19, 10, '', '1510671099.jpg', 3),
(20, 11, '', '1510671129.jpeg', 3),
(21, 11, '', '1510671143.jpeg', 3),
(22, 11, '', '1510671152.jpeg', 3),
(23, 11, '', '1510671165.jpeg', 3),
(24, 12, '', '1510671219.jpg', 3),
(25, 12, '', '1510671225.jpg', 3),
(26, 12, '', '1510671244.jpg', 3),
(27, 12, '', '1510671255.jpg', 2),
(28, 13, '', '1510671299.jpg', 3),
(29, 13, '', '1510671315.jpg', 3),
(30, 13, '', '1510671329.jpg', 3),
(31, 13, '', '1510671353.jpg', 3),
(40, 7, '', '1510921702.jpg', 3),
(41, 16, '', '1511886877.jpg', 3),
(42, 17, '', '1511886944.jpg', 3),
(43, 18, '', '1511886993.jpg', 3),
(44, 14, '', '1511955627.jpg', 3),
(45, 14, '', '1511955641.jpg', 2),
(46, 14, '', '1511955654.jpg', 3),
(47, 15, '', '1511955676.jpg', 3),
(48, 15, '', '1511955703.jpg', 3),
(49, 15, '', '1511955714.jpg', 3),
(50, 13, '', '1511955814.jpg', 2),
(51, 19, '', '1511965328.jpg', 3),
(52, 19, '', '1511965338.jpg', 3),
(53, 19, '', '1511965350.jpg', 2),
(55, 21, '', '1512389633.jpg', 3),
(56, 20, '', '1512566148.jpg', 3),
(57, 20, '', '1512566223.jpg', 3),
(58, 20, '', '1512566233.jpg', 3),
(59, 21, '', '1512566259.jpg', 3),
(60, 21, '', '1512566272.jpg', 3),
(61, 20, '', '1512999917.jpg', 2),
(62, 21, '', '1513000220.jpg', 2),
(63, 7, '', '1517398071.jpg', 2),
(64, 22, '', '1517398173.jpg', 3),
(65, 22, '', '1517398195.jpg', 2),
(66, 22, '', '1517398215.jpg', 3),
(67, 23, '', '1517910000.jpg', 3),
(68, 23, '', '1517910039.jpeg', 3),
(69, 23, '', '1517910050.jpg', 3),
(70, 23, '', '1517910062.jpg', 3),
(71, 24, '', '1524064267.png', 3),
(72, 26, '', '1524064337.png', 3),
(73, 27, '', '1524064385.png', 3);

-- --------------------------------------------------------

--
-- Структура таблицы `product_option`
--

CREATE TABLE `product_option` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  `parent_product_option_value_id` int(11) NOT NULL,
  `sort` int(11) NOT NULL DEFAULT '5'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `product_option`
--

INSERT INTO `product_option` (`id`, `product_id`, `option_id`, `parent_product_option_value_id`, `sort`) VALUES
(39, 20, 1, 0, 5);

-- --------------------------------------------------------

--
-- Структура таблицы `product_option_value`
--

CREATE TABLE `product_option_value` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_option_id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  `option_value_id` int(11) NOT NULL,
  `quantity` int(11) DEFAULT '999',
  `price` decimal(10,2) DEFAULT '0.00',
  `sort` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `product_option_value`
--

INSERT INTO `product_option_value` (`id`, `product_id`, `product_option_id`, `option_id`, `option_value_id`, `quantity`, `price`, `sort`) VALUES
(93, 23, 39, 1, 3, 999, '0.00', 5),
(94, 20, 39, 1, 4, 999, '0.00', 5);

-- --------------------------------------------------------

--
-- Структура таблицы `settings`
--

CREATE TABLE `settings` (
  `id` int(11) NOT NULL,
  `key` varchar(255) NOT NULL,
  `value` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `settings`
--

INSERT INTO `settings` (`id`, `key`, `value`) VALUES
(1, 'default_currency', '1'),
(2, 'default_order_status_id', '1'),
(3, 'default_shipping', '1'),
(4, 'admin_inform_emails', 'patison.pon.pon@mail.ru'),
(5, 'default_pack_weight', '50');

-- --------------------------------------------------------

--
-- Структура таблицы `shipping`
--

CREATE TABLE `shipping` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `shipping`
--

INSERT INTO `shipping` (`id`, `name`) VALUES
(1, 'Доставка Ж/Д'),
(2, 'Доставка морем'),
(3, 'Доставка Авто(БРЕНД)'),
(4, 'Доставка Китай АВИА(БРЕНД)');

-- --------------------------------------------------------

--
-- Структура таблицы `shipping_price`
--

CREATE TABLE `shipping_price` (
  `id` int(11) NOT NULL,
  `supply_id` int(11) NOT NULL,
  `shipping_id` int(11) NOT NULL,
  `weight_price` decimal(10,2) NOT NULL DEFAULT '0.00',
  `pack_weight` decimal(10,2) NOT NULL DEFAULT '0.00',
  `pack_price` decimal(10,2) NOT NULL DEFAULT '0.00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `shipping_price`
--

INSERT INTO `shipping_price` (`id`, `supply_id`, `shipping_id`, `weight_price`, `pack_weight`, `pack_price`) VALUES
(1, 1, 1, '2.50', '50.00', '5.00'),
(2, 2, 1, '3.00', '100.00', '20.00'),
(4, 1, 2, '8.00', '100.00', '30.00'),
(5, 2, 2, '16.00', '100.00', '50.00'),
(6, 3, 1, '0.00', '0.00', '0.00'),
(7, 3, 2, '0.00', '0.00', '0.00'),
(8, 1, 3, '0.00', '0.00', '0.00'),
(9, 2, 3, '0.00', '0.00', '0.00'),
(10, 3, 3, '0.00', '0.00', '0.00'),
(11, 1, 4, '0.00', '0.00', '0.00'),
(12, 2, 4, '0.00', '0.00', '0.00'),
(13, 3, 4, '0.00', '0.00', '0.00'),
(14, 1, 5, '0.00', '0.00', '0.00'),
(15, 2, 5, '0.00', '0.00', '0.00'),
(16, 3, 5, '0.00', '0.00', '0.00');

-- --------------------------------------------------------

--
-- Структура таблицы `unit`
--

CREATE TABLE `unit` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `additional_product_name` varchar(255) DEFAULT NULL,
  `additional_product_price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Дамп данных таблицы `unit`
--

INSERT INTO `unit` (`id`, `name`, `additional_product_name`, `additional_product_price`) VALUES
(1, 'м³', 'Поддон', 5),
(2, 'тонн', NULL, 0),
(3, 'шт', NULL, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `role` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `user`
--

INSERT INTO `user` (`id`, `username`, `auth_key`, `password_hash`, `password_reset_token`, `email`, `status`, `role`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'EKNSu9QR6X4FzqHyscsdCshksRqeYgvB', '$2y$13$GMo1uWpZ.xCHhuUgOpQxYeA64d3p1htovIUauHGF7DfgKtjYlWnb.', NULL, 'admin@mail.ru', 10, 'ADMINISTRATOR', 1509619100, 1509619100);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `auth_assignment`
--
ALTER TABLE `auth_assignment`
  ADD PRIMARY KEY (`item_name`,`user_id`),
  ADD KEY `auth_assignment_user_id_idx` (`user_id`);

--
-- Индексы таблицы `auth_item`
--
ALTER TABLE `auth_item`
  ADD PRIMARY KEY (`name`),
  ADD KEY `rule_name` (`rule_name`),
  ADD KEY `idx-auth_item-type` (`type`);

--
-- Индексы таблицы `auth_item_child`
--
ALTER TABLE `auth_item_child`
  ADD PRIMARY KEY (`parent`,`child`),
  ADD KEY `child` (`child`);

--
-- Индексы таблицы `auth_rule`
--
ALTER TABLE `auth_rule`
  ADD PRIMARY KEY (`name`);

--
-- Индексы таблицы `belonging`
--
ALTER TABLE `belonging`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `call_me`
--
ALTER TABLE `call_me`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `currency`
--
ALTER TABLE `currency`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `password_reset_token` (`password_reset_token`);

--
-- Индексы таблицы `info`
--
ALTER TABLE `info`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `manager`
--
ALTER TABLE `manager`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `migration`
--
ALTER TABLE `migration`
  ADD PRIMARY KEY (`version`);

--
-- Индексы таблицы `option`
--
ALTER TABLE `option`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `option_value`
--
ALTER TABLE `option_value`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `order`
--
ALTER TABLE `order`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `order_history`
--
ALTER TABLE `order_history`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `order_product`
--
ALTER TABLE `order_product`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `order_status`
--
ALTER TABLE `order_status`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `order_sum`
--
ALTER TABLE `order_sum`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `product_carousel`
--
ALTER TABLE `product_carousel`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `product_carousel_product`
--
ALTER TABLE `product_carousel_product`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `product_image`
--
ALTER TABLE `product_image`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `product_option`
--
ALTER TABLE `product_option`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `product_option_value`
--
ALTER TABLE `product_option_value`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `shipping`
--
ALTER TABLE `shipping`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `shipping_price`
--
ALTER TABLE `shipping_price`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `unit`
--
ALTER TABLE `unit`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `password_reset_token` (`password_reset_token`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `belonging`
--
ALTER TABLE `belonging`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблицы `call_me`
--
ALTER TABLE `call_me`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT для таблицы `currency`
--
ALTER TABLE `currency`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблицы `customer`
--
ALTER TABLE `customer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT для таблицы `info`
--
ALTER TABLE `info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблицы `manager`
--
ALTER TABLE `manager`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `option`
--
ALTER TABLE `option`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблицы `option_value`
--
ALTER TABLE `option_value`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT для таблицы `order`
--
ALTER TABLE `order`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT для таблицы `order_history`
--
ALTER TABLE `order_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT для таблицы `order_product`
--
ALTER TABLE `order_product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT для таблицы `order_status`
--
ALTER TABLE `order_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `order_sum`
--
ALTER TABLE `order_sum`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT для таблицы `product`
--
ALTER TABLE `product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT для таблицы `product_carousel`
--
ALTER TABLE `product_carousel`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `product_carousel_product`
--
ALTER TABLE `product_carousel_product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT для таблицы `product_image`
--
ALTER TABLE `product_image`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=74;
--
-- AUTO_INCREMENT для таблицы `product_option`
--
ALTER TABLE `product_option`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;
--
-- AUTO_INCREMENT для таблицы `product_option_value`
--
ALTER TABLE `product_option_value`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=95;
--
-- AUTO_INCREMENT для таблицы `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT для таблицы `shipping`
--
ALTER TABLE `shipping`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT для таблицы `shipping_price`
--
ALTER TABLE `shipping_price`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT для таблицы `unit`
--
ALTER TABLE `unit`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблицы `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `auth_assignment`
--
ALTER TABLE `auth_assignment`
  ADD CONSTRAINT `auth_assignment_ibfk_1` FOREIGN KEY (`item_name`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `auth_item`
--
ALTER TABLE `auth_item`
  ADD CONSTRAINT `auth_item_ibfk_1` FOREIGN KEY (`rule_name`) REFERENCES `auth_rule` (`name`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `auth_item_child`
--
ALTER TABLE `auth_item_child`
  ADD CONSTRAINT `auth_item_child_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `auth_item_child_ibfk_2` FOREIGN KEY (`child`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
