/*Товары +- для модального окна с корзиной*/
$('#cart-modal').on('click', '.product-minus', function (e) {
    e.preventDefault();
    var key = $(this).data('key');
    var modal = 1;
    console.log(key);
    $.ajax({
        url: '/cart/minus',
        data: {key: key},
        type: 'GET',
        success: function (res) {
            if (!res){
                alert('Error!');
            }else{
                showCart(res, modal);
            }
        },
        error: function () {
            alert('Error!');
        }
    });
});

$('#cart-modal').on('click', '.product-plus', function (e) {
    e.preventDefault();
    var key = $(this).data('key');
    var modal = 1;
    console.log(key);
    $.ajax({
        url: '/cart/plus',
        data: {key: key},
        type: 'GET',
        success: function (res) {
            if (!res){
                alert('Error!');
            }else{
                showCart(res, modal);
            }
        },
        error: function () {
            alert('Error!');
        }
    });
});

/*Товары +- для страницы заказа*/
$('#cart-checkout').on('click', '.product-minus', function (e) {
    e.preventDefault();
    var key = $(this).data('key');
    var modal = 0;
    console.log(key);
    $.ajax({
        url: '/cart/minus',
        data: {key: key},
        type: 'GET',
        success: function (res) {
            if (!res){
                alert('Error!');
            }else{
                showCart(res, modal);
            }
        },
        error: function () {
            alert('Error!');
        }
    });
});

$('#cart-checkout').on('click', '.product-plus', function (e) {
    e.preventDefault();
    var key = $(this).data('key');
    var modal = 0;
    console.log(key);
    $.ajax({
        url: '/cart/plus',
        data: {key: key},
        type: 'GET',
        success: function (res) {
            if (!res){
                alert('Error!');
            }else{
                showCart(res, modal);
            }
        },
        error: function () {
            alert('Error!');
        }
    });
});


/*Добавить товар в корзину, для главной страницы после использования аякса*/
$('#product-price').on('click', '.add-to-cart', function (e) {
    e.preventDefault();
    var id = $(this).data('id');
    var quantity = $(".product-quantity").val();
    var modal = 1;
    $.ajax({
        url: '/cart/add',
        data: {id: id, quantity: quantity},
        type: 'GET',
        success: function (res) {
            if (!res){
                alert('Error!');
            }else{
                showCart(res, modal);
                window.stop();
            }
        },
        error: function () {
            alert('Error!');
        }
    });
});

/*Добавить товар в корзину, для страницы товара и других работающих без аякса*/
$('.add-into-cart').on('click', function (e) {
    e.preventDefault();
    var $id = $(this).data('id');

    //Проверяем опции
    var $options = {};
    var $errors = {};
    var $quantity;

    $('#option-container').find ('select').each(function() {

        //если не получили значение опции, пишем ее в обьект с ошибками
        if (!$(this).val()){
            $errors[this.name] = {
                'parent_product_option_value_id': $(this).data('parent_product_option_value_id'),
                'product_option_id': $(this).data('product_option_id'),
                'option_name': $(this).data('option_name')
            };
        }else{
            $options[this.name] = {
                'parent_product_option_value_id': $(this).data('parent_product_option_value_id'),
                'product_option_id': $(this).data('product_option_id'),
                'value': $(this).val()
            };
        }
    });

    //если пустой обьект с ошибками добавляем товар в корзину
    if ($.isEmptyObject($errors)) {
        console.log($options);
        if ($(".product-quantity").val()){
            $quantity = $(".product-quantity").val();
        }else{
            $quantity = $(this).data('quantity');
        }
        var modal = 1;
        $.ajax({
            url: '/cart/add',
            data: {id: $id, quantity: $quantity, options: $options},
            type: 'GET',
            success: function (res) {
                if (!res){
                    alert('Error!');
                }else{
                    showCart(res, modal);
                }
            },
            error: function () {
                alert('Error!');
            }
        });
    }

    //если получили ошибки опций, выводим инфу юзеру
    else{
        jQuery.each($errors, function($key, $value) {
             $('.option-select-'+ $key).addClass('has-error');
             $('.option-select-'+ $key + ' .help-block').html('Выберите "' + $value.option_name + '"');
        });
    }
});

/*Показали корзину, в модалке или на странице заказа*/
function showCart(cart, modal) {
    if (modal){
        $('#cart-modal .modal-body').html(cart);
        $('#cart-modal').modal();
    }else{
        $('#cart-checkout').html(cart);
    }
}

/*Полностью очитстили корзину*/
function clearCart() {
    var modal = 1;
    $.ajax({
        url: '/cart/clear',
        type: 'GET',
        success: function (res) {
            if (!res){
                alert('Error!');
            }else{
                showCart(res, modal);
            }
        },
        error: function () {
            alert('Error!');
        }
    });
}

/*Удалили позицию, для модалки*/
$('#cart-modal .modal-body').on('click', '.del-product', function () {
    var key = $(this).data('key');
    var modal = 1;
    $.ajax({
        url: '/cart/del-product',
        data: {key: key},
        type: 'GET',
        success: function (res) {
            if (!res){
                alert('Error!');
            }else{
                showCart(res, modal);
            }
        },
        error: function () {
            alert('Error!');
        }
    });
});

/*Удалили позицию, для страницы заказа*/
$('#cart-checkout').on('click', '.del-product', function () {
    var key = $(this).data('key');
    var modal = 0;
    $.ajax({
        url: '/cart/del-product',
        data: {key: key},
        type: 'GET',
        success: function (res) {
            if (!res){
                alert('Error!');
            }else{
                showCart(res);
            }
        },
        error: function () {
            alert('Error!');
        }
    });
});

/*получили данные по корзине*/
function getCart(modal) {
    $.ajax({
        url: '/cart/show',
        type: 'GET',
        success: function (res) {
            if (!res){
                alert('Error!');
            }else{
                showCart(res, modal);
            }
        },
        error: function () {
            alert('Error!');
        }
    });
}

/*

function DoubleScroll(element) {
    var scrollbar= document.createElement('div');
    scrollbar.appendChild(document.createElement('div'));
    scrollbar.style.overflow= 'auto';
    scrollbar.style.overflowY= 'hidden';
    scrollbar.firstChild.style.width= element.scrollWidth+'px';
    scrollbar.firstChild.style.paddingTop= '1px';
    scrollbar.firstChild.appendChild(document.createTextNode('\xA0'));
    scrollbar.onscroll= function() {
        element.scrollLeft= scrollbar.scrollLeft;
    };
    element.onscroll= function() {
        scrollbar.scrollLeft= element.scrollLeft;
    };
    element.parentNode.insertBefore(scrollbar, element);
}
DoubleScroll(document.getElementById('doublescroll'));*/
