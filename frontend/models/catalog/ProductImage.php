<?php
/**
 * Created by PhpStorm.
 * User: patison
 * Date: 08.11.17
 * Time: 17:11
 */

namespace frontend\models\catalog;

use Yii;
use yii\imagine\Image;
use yii\helpers\Json;
use Imagine\Image\Box;
use Imagine\Image\Point;



class ProductImage extends \common\tables\ProductImage
{
    public $image;
    public $crop_info;
    public $image_name;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id', 'main'], 'safe'],
            [
                'image',
                'image',
                'extensions' => ['jpg', 'jpeg', 'png', 'gif'],
                'mimeTypes' => ['image/jpeg', 'image/pjpeg', 'image/png', 'image/gif'],
            ],
            ['crop_info', 'safe'],
            ['src', 'safe'],
            ['image_name', 'safe'],
        ];
    }

    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

    public function afterSave($insert, $changedAttributes){

        if($this->crop_info){
            // open image
            $image = Image::getImagine()->open($this->image->tempName);
            // rendering information about crop of ONE option
            $cropInfo = Json::decode($this->crop_info)[0];
            $cropInfo['dWidth'] = (int)$cropInfo['dWidth']; //new width image
            $cropInfo['dHeight'] = (int)$cropInfo['dHeight']; //new height image
            $cropInfo['x'] = $cropInfo['x']; //begin position of frame crop by X
            $cropInfo['y'] = $cropInfo['y']; //begin position of frame crop by Y

            //saving thumbnail
            $newSizeThumb = new Box($cropInfo['dWidth'], $cropInfo['dHeight']);
            $cropSizeThumb = new Box(400, 400); //frame size of crop
            $cropPointThumb = new Point($cropInfo['x'], $cropInfo['y']);
            $pathThumbImage = Yii::getAlias('@frontend') . '/web/image/catalog/Product/' . $this->product_id . '/'
                . 'thumb_'
                . $this->image_name
                . '.'
                . $this->image->getExtension();

            $image->resize($newSizeThumb)
                ->crop($cropPointThumb, $cropSizeThumb)
                ->save($pathThumbImage, ['quality' => 100]);

            //saving original

            $this->image->saveAs(
                Yii::getAlias('@frontend') . '/web/image/catalog/Product/' . $this->product_id . '/'
                . $this->image_name
                . '.'
                . $this->image->getExtension()
            );
        }
    }


}