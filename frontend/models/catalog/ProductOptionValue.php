<?php
/**
 * Created by PhpStorm.
 * User: patison
 * Date: 23.02.18
 * Time: 13:46
 */

namespace frontend\models\catalog;


class ProductOptionValue extends \common\tables\ProductOptionValue
{
    public function getDescription()
    {
        return $this->hasOne(OptionValue::className(), ['id' => 'option_value_id']);
    }
}