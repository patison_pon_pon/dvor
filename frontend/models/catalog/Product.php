<?php

namespace frontend\models\catalog;


class Product extends \common\tables\Product
{
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }
    public function getBelonging()
    {
        return $this->hasOne(Belonging::className(), ['id' => 'belonging_id']);
    }
    public function getUnit()
    {
        return $this->hasOne(Unit::className(), ['id' => 'unit_id']);
    }
    public function getImage()
    {
        return $this->hasOne(ProductImage::className(), ['product_id' => 'id'])->orderBy(['sort' => SORT_ASC]);
    }
    public function getImages()
    {
        return $this->hasMany(ProductImage::className(), ['product_id' => 'id'])->orderBy(['sort' => SORT_ASC]);
    }
    public function getOptions()
    {
        return $this->hasMany(ProductOption::className(), ['product_id' => 'id'])->where(['parent_product_option_value_id' => '0'])->orderBy(['sort' => SORT_ASC]);
    }

}