<?php
/**
 * Created by PhpStorm.
 * User: patison
 * Date: 23.02.18
 * Time: 13:46
 */

namespace frontend\models\catalog;


class ProductOption extends \common\tables\ProductOption
{
    public function getDescription()
    {
        return $this->hasOne(Option::className(), ['id' => 'option_id']);
    }

}