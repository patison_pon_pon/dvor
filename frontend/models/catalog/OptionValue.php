<?php
/**
 * Created by PhpStorm.
 * User: patison
 * Date: 22.02.18
 * Time: 10:22
 */

namespace frontend\models\catalog;


class OptionValue extends \common\tables\OptionValue
{
    public function getOption()
    {
        return $this->hasOne(Option::className(), ['id' => 'option_id']);
    }

}