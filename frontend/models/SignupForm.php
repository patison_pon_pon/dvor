<?php
namespace frontend\models;

use common\models\Customer;
use yii\base\Model;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $username;
    public $email;
    public $phone;
    public $manager_id;
    public $password;
    public $business;
    public $geo;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['username', 'trim'],
            ['username', 'required'],
            ['username', 'string', 'min' => 2, 'max' => 255],

            ['phone', 'trim'],
            ['phone', 'string', 'min' => 2, 'max' => 255],

            ['business', 'string'],

            ['geo', 'string'],

            ['manager_id', 'trim'],
            ['manager_id', 'required'],
            ['manager_id', 'integer'],

            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => 'frontend\models\shop\Customer', 'message' => 'Этот E-mail уже зарегистрирован.'],

            ['password', 'required'],
            ['password', 'string', 'min' => 6],
        ];
    }

    /**
     * Signs customer up.
     *
     * @return Customer|null the saved model or null if saving fails
     */
    public function signup()
    {
        if (!$this->validate()) {
            return null;
        }
        
        $customer = new Customer();
        $customer->username = $this->username;
        $customer->email = $this->email;
        $customer->phone = $this->phone;
        $customer->manager_id  = $this->manager_id;
        $customer->business = $this->business;
        $customer->geo = $this->geo;
        $customer->setPassword($this->password);
        $customer->generateAuthKey();
        
        return $customer->save() ? $customer : null;
    }
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'ФИО',
            'email' => 'E-mail',
            'phone' => 'Телефон',
            'password' => 'Пароль',
            'business' => 'Товарная группа (наибоелее интересная вам товарная группа)',
            'geo' => 'Территория бизнеса (Основная терриятория вашего бизнеса)',
        ];
    }
}
