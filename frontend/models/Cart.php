<?php

namespace frontend\models;


use frontend\models\catalog\ProductOption;
use frontend\models\catalog\ProductOptionValue;
use Yii;
use yii\db\ActiveRecord;

class Cart extends ActiveRecord
{
    public function addToCart($product, $quantity = 1, $product_options){
        $options = '';
        $key = $product->id;

        if ($product_options){
            foreach ($product_options as $product_option) {
                $option = ProductOption::findOne($product_option['product_option_id']);
                $option_value = ProductOptionValue::findOne($product_option['value']);
                $options[$option->id] = [
                    'product_option_id' => $option->id,
                    'product_option_name' => $option->description->name,
                    'product_option_value_id' => $option_value->id,
                    'product_option_value_name' => $option_value->description->name,
                ];
                $key .= $option->id . $option_value->id;
            }
        }

        if (isset($_SESSION['cart'][$key])){
            $_SESSION['cart'][$key]['quantity'] += $quantity;
        }else{
            $_SESSION['cart'][$key] = [
                'id' => $product->id,
                'key' => $key,
                'url' => $product->url,
                'belonging_id' => $product->belonging_id,
                'belonging' => $product->belonging->name,
                'unit_id' => $product->unit_id,
                'unit' => $product->unit->name,
                'name' => $product->name,
                'quantity' => $quantity,
                'weight' => $product->weight,
                'minimum' => $product->minimum,
                'options' => $options,
                'price' => $product->price,
                'image' => $product->image->src
            ];
        }

        Yii::$app->session['cart.quantity'] = (
        isset(Yii::$app->session['cart.quantity'])
            ? Yii::$app->session['cart.quantity'] + $quantity
            : $quantity
        );

        Yii::$app->session['cart.sum'] = (
        isset(Yii::$app->session['cart.sum'])
            ? Yii::$app->session['cart.sum'] + $quantity * $product->price
            : $quantity * $product->price
        );

        Yii::$app->session['cart.weight'] = (
        isset(Yii::$app->session['cart.weight'])
            ? Yii::$app->session['cart.weight'] + $product->weight * $product->price
            : $product->weight * $product->price
        );
    }

    public function recalculate($key){
        if (isset($_SESSION['cart'][$key])){
            $qtyMinus = $_SESSION['cart'][$key]['quantity'];
            $sumMinus = $_SESSION['cart'][$key]['quantity'] * $_SESSION['cart'][$key]['price'];
            $weightMinus = $_SESSION['cart'][$key]['quantity'] * $_SESSION['cart'][$key]['weight'];
            $_SESSION['cart.quantity'] -= $qtyMinus;
            $_SESSION['cart.sum'] -= $sumMinus;
            $_SESSION['cart.weight'] -= $weightMinus;
            unset($_SESSION['cart'][$key]);
        }else{
            return false;
        }
    }

    public function minus($key){
        if ($_SESSION['cart'][$key]['quantity'] > 1){
            if ($_SESSION['cart'][$key]['quantity'] == $_SESSION['cart'][$key]['minimum'])
                return true;
            $_SESSION['cart'][$key]['quantity'] -= 1;
            $qtyMinus = 1;
            $weightMinus = $_SESSION['cart'][$key]['weight'];
            $sumMinus = $_SESSION['cart'][$key]['price'];
            $_SESSION['cart.quantity'] -= $qtyMinus;
            $_SESSION['cart.sum'] -= $sumMinus;
            $_SESSION['cart.weight'] -= $weightMinus;
        }
    }

    public function plus($key){
        if ($_SESSION['cart'][$key]['quantity'] >= 1){
            $_SESSION['cart'][$key]['quantity'] += 1;
            $qtyPlus = 1;
            $sumPlus = $_SESSION['cart'][$key]['price'];
            $_SESSION['cart.quantity'] += $qtyPlus;
            $_SESSION['cart.sum'] += $sumPlus;
        }
    }

}