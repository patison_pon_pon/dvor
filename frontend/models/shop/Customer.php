<?php
/**
 * Created by PhpStorm.
 * User: patison
 * Date: 01.12.17
 * Time: 17:36
 */

namespace frontend\models\shop;


class Customer extends \common\tables\Customer
{

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'ФИО',
            'email' => 'E-mail',
            'phone' => 'Телефон',
            'business' => 'Товарная группа',
            'geo' => 'Территория бизнеса',
            'password' => 'Пароль',
        ];
    }
}