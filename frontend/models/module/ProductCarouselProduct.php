<?php
/**
 * Created by PhpStorm.
 * User: patison
 * Date: 25.12.17
 * Time: 15:25
 */

namespace frontend\models\module;


use frontend\models\catalog\Product;

class ProductCarouselProduct extends \common\tables\ProductCarouselProduct
{
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }
}