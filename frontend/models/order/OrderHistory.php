<?php
/**
 * Created by PhpStorm.
 * User: patison
 * Date: 24.11.17
 * Time: 17:58
 */

namespace frontend\models\order;


class OrderHistory extends \common\tables\OrderHistory
{

    public function getStatus()
    {
        return $this->hasOne(OrderStatus::className(), ['id' => 'order_status_id']);
    }
}