<?php
/**
 * Created by PhpStorm.
 * User: patison
 * Date: 24.11.17
 * Time: 17:58
 */

namespace frontend\models\order;


use frontend\models\catalog\Product;
use frontend\models\catalog\ProductImage;

class OrderProduct extends \common\tables\OrderProduct
{
    public function getImage()
    {
        return $this->hasOne(ProductImage::className(), ['product_id' => 'product_id'])->orderBy(['sort' => SORT_ASC]);
    }
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }
}