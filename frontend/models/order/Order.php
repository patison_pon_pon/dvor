<?php
/**
 * Created by PhpStorm.
 * User: patison
 * Date: 24.11.17
 * Time: 17:54
 */

namespace frontend\models\order;


use frontend\models\catalog\Supply;
use frontend\models\shop\Currency;

class Order extends \common\tables\Order
{
    public function getSupply()
    {
        return $this->hasOne(Supply::className(), ['id' => 'supply_id']);
    }
    public function getStatus()
    {
        return $this->hasOne(OrderStatus::className(), ['id' => 'order_status_id']);
    }

    public function getCurrency()
    {
        return $this->hasOne(Currency::className(), ['id' => 'currency_id']);
    }
}