<?php

namespace frontend\widgets\customer;

use common\models\CustomerLoginForm;
use Yii;
use yii\base\Widget;

class LoginFormWidget extends Widget {

    public function run() {
        if (Yii::$app->customer->isGuest) {
            $model = new CustomerLoginForm();
            return $this->render('loginForm', [
                'model' => $model,
            ]);
        } else {
            return ;
        }
    }

}