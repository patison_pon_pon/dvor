<?php

use rmrevin\yii\fontawesome\FA;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Modal;

echo Html::a(
    FA::icon('user') . '&nbsp;Войти',
    '#',
    [
        'data-toggle' => 'modal',
        'data-target' => '#login-modal',
        'class' => 'btn btn-ytos'
    ]
);
echo Html::a('Зарегистрироваться', ['/site/register'], ['class' => 'btn btn-tos']);

Modal::begin([
    //'header' => false,
    'id' => 'login-modal',
]);
?>

    <?php $form = ActiveForm::begin([
        'id' => 'login-form',
        'enableAjaxValidation' => true,
        'action' => ['/site/login'],
    ]);
    echo $form->field($model, 'email')->textInput();
    echo $form->field($model, 'password')->passwordInput();
    ?>
    <br>

    <div class="form-group">
        <div class="text-center">

            <?php
            echo Html::submitButton('Войти', ['class' => 'btn btn-ytos', 'name' => 'login-button']);
            echo "&nbsp;&nbsp;или&nbsp;&nbsp;";
            echo Html::a('Зарегистрироваться', ['/site/register'], ['class' => 'btn btn-tos']);
            ?>

        </div>
    </div>

<?php
ActiveForm::end();
Modal::end();