
<?php

use yii\bootstrap\Modal;
use yii\helpers\Html;

Modal::begin([
    'id' => 'cart-modal',
    'size' => 'modal-lg',
    'header' => "<h3 class='text-center'>Корзина</h3>",
    'footer' =>
        Html::button(
            'Продолжить покупки',
            [
                'class' => 'btn btn-tos',
                'data-dismiss' => 'modal'
            ]
        )
        /*. Html::button(
            'Очистить корзину',
            [
                'class' => 'btn btn-danger',
                'onclick' => 'clearCart()'
            ]
        )*/
        . Html::a(
            'Оформить заказ',
            ['/cart/checkout'],
            [
                'class' => 'btn btn-ytos'
            ]
        )
]);
Modal::end();