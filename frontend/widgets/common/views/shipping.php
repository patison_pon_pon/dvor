<?php
use yii\helpers\Html;
?>
<div id="shipping">

        <?php

        $shipments = Yii::$app->my->shipments();
        echo Html::dropDownList(
                'currency',
                Yii::$app->session->get('shipping'),
                \yii\helpers\ArrayHelper::map($shipments, 'id', 'name'),
                [
                    'class' => 'shipping-input',
                    'onchange' => "changeShipping($(this).val())"
                ]
            );
        ?>
</div>