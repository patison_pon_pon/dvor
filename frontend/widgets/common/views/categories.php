<?php
$script = <<< JS
    $('#category-tree li:has("ul")').find('span:first').append('<i class="marker fa fa-chevron-left"></em>');
    // вешаем событие на клик по ссылке
    $('#category-tree li span').click(function () {
        // снимаем выделение предыдущего узла
        $('a.current').removeClass('current');
        var a = $('a:first',this.parentNode);
        // Выделяем выбранный узел
        //было a.hasClass('current')?a.removeClass('current'):a.addClass('current');
        a.toggleClass('current');
        var li=$(this.parentNode);
        /* если это последний узел уровня, то соединительную линию к следующему
          рисовать не нужно */
        if (!li.next().length) {
            /* берем корень разветвления <li>, в нем находим поддерево <ul>,
             выбираем прямых потомков ul > li, назначаем им класс 'last' */
            li.find('ul:first > li').addClass('last');
        }
        // анимация раскрытия узла и изменение состояния маркера
        var ul=$('ul:first',this.parentNode);// Находим поддерево
        if (ul.length) {// поддерево есть
            ul.slideToggle(100); //свернуть или развернуть
            // Меняем сосотояние маркера на закрыто/открыто
            var em=$('i:last',this.parentNode);// this = 'li span'
            // было em.hasClass('open')?em.removeClass('open'):em.addClass('open');
            em.addClass('open');
        }
    });
JS;
$this->registerJs($script, yii\web\View::POS_READY);

if (isset($_GET['category_id'])){
    $script = <<< JS
    //$(".item_{$_GET['category_id']}").parents('li').find('> span').click();
    $("#category-tree .active").parents('li').find('> span').click();
JS;
    $this->registerJs($script, yii\web\View::POS_READY);
}
?>

<div id="category-tree">
    <h3>Категории</h3>
    <?= $categories?>
</div>
