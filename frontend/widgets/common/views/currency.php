<?php
use yii\helpers\Html;
?>
<div id="currency">
    <div class="btn-group" data-toggle="buttons">

        <?php
        $currencies = Yii::$app->my->currencies();
        foreach ($currencies as $currency) {
            echo Html::label(
                Html::input(
                    'radio',
                    'currency',
                    $currency['id'],
                    [
                        'class' => 'form-control',
                        'autocomplete' => 'off',
                        'onchange' => "changeCurrency('{$currency['id']}')"
                    ]
                )
                . $currency['code'],
                '',
                [
                    'class' => (
                    $currency['id'] == Yii::$app->session->get('currency')
                        ? 'btn btn-tos active'
                        : 'btn btn-tos'
                    )
                ]

            );
        }
        ?>
    </div>
</div>