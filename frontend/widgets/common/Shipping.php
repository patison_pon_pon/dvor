<?php

namespace frontend\widgets\common;

use yii\base\Widget;


class Shipping extends Widget {

    public function run() {
        return $this->render('shipping');
    }
}