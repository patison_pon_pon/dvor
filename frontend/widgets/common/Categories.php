<?php

namespace frontend\widgets\common;

use frontend\models\catalog\Category;
use yii\base\Widget;
use yii\helpers\Html;

class Categories extends Widget {

    public $categories;
    public $current;
    public $belonging_id;

    public function run() {
        if (isset($_GET['category_id'])){
            $this->current = $_GET['category_id'];
        }

        $this->getList();
        return $this->render('categories', [
            'categories' => $this->categories,
        ]);
    }

    public function getList($parent_id = NULL){
        $a = Category::find()
            ->where(['parent_id' => $parent_id, 'belonging_id' => $this->belonging_id])
            ->orderBy(['name' => SORT_ASC])
            ->asArray()
            ->all();
        if ($a){
            $this->categories = '<ul>';
            foreach ($a as $c) {
                $this->categories .= '<li class="item_' . $c['id'] . '"><span>'
                    . Html::a($c['name'], ['/belonging/index', 'belonging_id' => $this->belonging_id, 'category_id' => $c['id']], ['class' => ($this->current == $c['id'] ? 'active' : '')])
                    . '</span>'
                    . $this->getChild($c['id'])
                    . '</li>'
                ;
            }
            $this->categories .= '</ul>';
        }
    }

    public function getChild($parent_id = NULL){
        $a = Category::find()
            ->where(['parent_id' => $parent_id])
            ->orderBy(['name' => SORT_ASC])
            ->asArray()
            ->all();
        if ($a){
            $r = '<ul>';
            foreach ($a as $c) {
                $r .= '<li class="item_' . $c['id'] . '"><span>'
                    . Html::a($c['name'], ['/belonging/index', 'belonging_id' => $this->belonging_id, 'category_id' => $c['id']], ['class' => ($this->current == $c['id'] ? 'active' : '')])
                    . '</span>'
                    . $this->getChild($c['id'])
                    . '</li>'
                ;
            }
            $r .= '</ul>';
            return $r;
        }
    }
}