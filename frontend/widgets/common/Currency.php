<?php

namespace frontend\widgets\common;

use yii\base\Widget;

class Currency extends Widget {

    public function run() {
        return $this->render('currency');
    }
}