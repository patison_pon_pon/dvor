<?php

namespace frontend\widgets\common;

use yii\base\Widget;

class Cart extends Widget {

    public function run() {
        return $this->render('cart');
    }
}