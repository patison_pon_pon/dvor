<?php

namespace frontend\widgets\module;

use yii\base\Widget;

class CallMe extends Widget {

    public function run() {
        $model = new \frontend\models\module\CallMe();
        return $this->render('call_me',[
            'model' => $model
        ]);
    }
}