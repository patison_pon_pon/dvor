<?php

namespace frontend\widgets\module;

use frontend\models\module\ProductCarouselProduct;
use yii\base\Widget;
use yii\data\ActiveDataProvider;

class ProductCarousel extends Widget {

    public $carousel_id;

    public function run() {
        $carousel = \frontend\models\module\ProductCarousel::findOne($this->carousel_id);
        $products = new ActiveDataProvider([
            'query' => ProductCarouselProduct::find()->where(['product_carousel_id' => $this->carousel_id])->orderBy(['sort' => SORT_ASC]),
        ]);
        return $this->render('product_carousel',[
            'carousel' => $carousel,
            'products' => $products,
        ]);
    }
}