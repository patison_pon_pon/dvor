<?php

use rmrevin\yii\fontawesome\FA;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Modal;

echo Html::a(
    FA::icon('phone'),
    '#',
    [
        'data-toggle' => 'modal',
        'data-target' => '#call-me-modal',
        'title' => 'Перезвонить мне',
        'class' => 'btn btn-ytos',
        'id' => 'call-me-button'
    ]
);

Modal::begin([
    'header' => 'Оставьте ваш номер телефона и вам перезвонят',
    'id' => 'call-me-modal',
]);
?>

<?php $form = ActiveForm::begin([
    'id' => 'call-me-form',
    'class' => 'text-left',
    //'enableAjaxValidation' => true,
    'action' => ['/site/call-me'],
]);
    echo $form->field($model, 'phone')->input('text', ['class' => 'form-control', 'placeholder' => 'Введите номер телефона'])->label(false);
?>

    <div class="form-group">
        <div class="text-center">

            <?php
            echo Html::submitButton('Отправить', ['class' => 'btn btn-ytos', 'name' => 'call-me-button']);
            ?>

        </div>
    </div>

<?php
ActiveForm::end();
Modal::end();