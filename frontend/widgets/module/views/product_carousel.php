<?php
use rmrevin\yii\fontawesome\FA;
use yii\widgets\ListView;

?>
<script type="text/javascript">
    $(document).ready(function() {
        $("#product_carousel_<?=$carousel->id?>").owlCarousel({
            navigation: true,
            items: <?=$carousel->item_limit?>,
            itemsDesktop: [1369, 3],
            itemsDesktopSmall: [1099, 2],
            itemsTablet: [768, 2],
            itemsTabletSmall: [739, 1],
            autoPlay: true,
            autoplayHoverPause: true,
            autoplayTimeout: 10000,
            navigationText: [

                '<?=FA::icon('chevron-circle-left')?>',
                '<?=FA::icon('chevron-circle-right')?>',
            ]
        });
    });
</script>
<h2 class="main-header"><?=$carousel->description?></h2>

<?php

echo ListView::widget([
    'dataProvider' => $products,
    'itemView' => 'product_carousel_list',
    'itemOptions' => [
        'tag' => 'div',
        'class' => 'product',
    ],
    'options' => [
        'tag' => 'div',
        'class' => 'products product-carousel',
        'id' => 'product_carousel_' . $carousel->id
    ],

    'layout' => "{pager}\n{items}\n{pager}",
    'summary' => 'Показано {count} из {totalCount}',
    'summaryOptions' => [
        'tag' => 'div',
        'class' => 'summary'
    ],
]);
?>