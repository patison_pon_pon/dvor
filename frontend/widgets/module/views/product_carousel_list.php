<?php
use yii\helpers\Html;
?>
<div class="image">
    <?=Html::a(
        Html::img('/image/catalog/Product/' . $model->product->id . '/thumb_' . $model->product->image->src),
        ['/product/index', 'product_id' => $model->product->id]

    )?>
</div>
<div class="product-body">
    <div class="name"><?=$model->product->name?></div>
    <div class="info">
        <table class="table">
            <tbody>
            <tr>
                <td>Принадлежность: </td>
                <td><?=$model->product->belonging->name?></td>
            </tr>
            <tr>
                <td>Мин. заказ: </td>
                <td><?=$model->product->minimum?></td>
            </tr>
            <tr>
                <td>Вес: </td>
                <td><?=$model->product->weight?></td>
            </tr>
            <tr>
                <td>Цена: </td>
                <td><b class="text-danger price"><?=Yii::$app->my->conversion($model->product->price)?></b></td>
            </tr>
            </tbody>
        </table>
    </div>
    <div class="buy">
        <?=Html::a(
            'Подробнее',
            ['/product/index', 'product_id' => $model->product->id],
            [
                'class' => 'btn btn-tos pull-left'
            ]

        )?>
        <?=Html::a(
            "<i class=\"glyphicon glyphicon-shopping-cart\"></i>&nbsp;Купить",
            ['/cart/add', 'id' => $model->product->id, 'quantity' => $model->product->minimum],
            [
                'class' => 'btn btn-ytos add-into-cart pull-right',
                'data-id' => $model->product->id,
                'data-quantity' => $model->product->minimum,
            ]
        )?>
        <div class="clear"></div>
    </div>
</div>