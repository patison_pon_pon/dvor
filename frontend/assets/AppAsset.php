<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/lightbox.css',
        'css/owl-carousel.css',
        'css/site.css',
        'css/768.css',
    ];
    public $js = [
        'js/main.js',
        'js/lightbox.js',
        'js/jquery.zoom.js',
        'js/owl.carousel.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'rmrevin\yii\fontawesome\AssetBundle'
    ];
}
