<?php

use yii\helpers\Html;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = $model->meta_t;
$this->registerMetaTag([
    'name' => 'keywords',
    'content' => $model->meta_k,
]);
$this->registerMetaTag([
    'name' => 'description',
    'content' => $model->meta_d,
]);
$this->params['breadcrumbs'][] = $this->title;
$this->params['main-header'] = $model->name;
?>
<div class="info-index">
    <?=$model->description?>
</div>
