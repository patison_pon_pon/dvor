<?php
use rmrevin\yii\fontawesome\FA;
use yii\bootstrap\Modal;
use yii\helpers\Html;
?>
<footer id="footer">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 footer-contacts">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 left">
                        <p><?=FA::icon('mobile')?><span> +380 00 000 00 00</span></p>
                        <p><?=FA::icon('mobile')?><span> +380 00 000 00 00</span></p>
                        <p><?=FA::icon('mobile')?><span> +380 00 000 00 00</span></p>
                        <p><?=FA::icon('envelope')?><span>dvor@dvor.com</span></p>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
            	<p>dvor.com <?=date('Y')?></p>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
            	<p class="text-right">Разработка: <b>patison.pon.pon@gmail.com</b></p>
            </div>
        </div>
    </div>
</footer>

<?=\frontend\widgets\common\Cart::widget()?>
<?=\frontend\widgets\module\CallMe::widget()?>
