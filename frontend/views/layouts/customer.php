<?php

/* @var $this \yii\web\View */
/* @var $content string */

use frontend\assets\AppAsset;
use kartik\sidenav\SideNav;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<?=$this->render('include/head');?>
<body>
<?php $this->beginBody() ?>

<!-- Подключаем header -->
<?= $this->render('/layouts/header') ?>
<!-- /Подключаем header -->

<div class="container">
    <div id="content">
        <div class="row">
            <div class="col-xs-12 col-sm-4 col-md-3 col-lg-3 leftbar">
                <?php
                echo SideNav::widget([
                    'type' => SideNav::TYPE_DEFAULT,
                    'heading' => 'Личный кабинет',
                    'items' => [
                        ['label' => 'Личная информация', 'icon'=>'user', 'url'=>'/customer/info'],
                        ['label' => 'Заказы', 'icon'=>'book', 'url'=>'/customer/order-list'],
                        ['label' => 'Изменить пароль', 'icon'=>'lock', 'url'=>'/customer/password'],
                        ['label' => 'Выйти', 'icon'=>'remove-circle', 'url'=>['/site/quit'], ['data-method' => 'post', 'class' => 'btn btn-danger btn-flat']],
                    ],
                ]);
                ?>
            </div>
            <div class="col-xs-12 col-sm-8 col-md-9 col-lg-9">
                <?=(isset($this->params['main-header']) ? '<h1 class="main-header">'.$this->params['main-header'].'</h1>' : '') ?>
                <?= $content ?>
            </div>
        </div>
    </div>
</div>

<!-- Подключаем footer -->
<?= $this->render('/layouts/footer') ?>
<!-- /Подключаем footer -->

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
