<?php

/* @var $this \yii\web\View */
/* @var $content string */

use frontend\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<?=$this->render('include/head');?>
<body>
<?php $this->beginBody() ?>

<!-- Подключаем header -->
<?= $this->render('/layouts/header') ?>
<!-- /Подключаем header -->

<div class="container">
    <div id="content">
        <?=(isset($this->params['main-header']) ? '<h1 class="main-header">'.$this->params['main-header'].'</h1>' : '') ?>
        <?= $content ?>
    </div>
</div>

<!-- Подключаем footer -->
<?= $this->render('/layouts/footer') ?>
<!-- /Подключаем footer -->

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
