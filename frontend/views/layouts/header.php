<?php


use common\widgets\Alert;
use frontend\widgets\common\Currency;
use frontend\widgets\customer\LoginFormWidget;
use frontend\widgets\common\Shipping;
use rmrevin\yii\fontawesome\FA;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;

$path = Url::to(["/site/change-currency"]);
$script = <<< JS
    
    function changeCurrency(id) {
        $.ajax({
            url: '$path',
            cache: false,
            type: "GET",
            data: {'id': id},
            success: function (data) {
            }
        });
    };
JS;
$this->registerJs($script, yii\web\View::POS_HEAD);

$path = Url::to(["/site/change-shipping"]);
$script = <<< JS
    
    function changeShipping(id) {
        $.ajax({
            url: '$path',
            cache: false,
            type: "GET",
            data: {'id': id},
            success: function (data) {
            }
        });
    };
JS;
$this->registerJs($script, yii\web\View::POS_HEAD);
?>
<div class="a"></div>
<header id="header">
    <div class="container">
        <div class="row header-choice">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 left">
                <?=Currency::widget()?>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 right">
                <?php
                if(Yii::$app->customer->isGuest) {
                    echo LoginFormWidget::widget([]);
                }else {
                    echo Html::a('Личный кабинет', '/customer/index', ['class' => 'btn btn-tos']);
                }
                ?>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 header-contacts">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 left">
                        <p><?=FA::icon('mobile')?><span> +380 00 000 00 00</span></p>
                        <p><?=FA::icon('mobile')?><span> +380 00 000 00 00</span></p>
                        <p><?=FA::icon('mobile')?><span> +380 00 000 00 00</span></p>
                        <p><?=FA::icon('envelope')?><span>dvor@dvor.com</span></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row logo">
            <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8 left">
            </div>
            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 right">
                <?=Html::button(
                    "<i class='glyphicon glyphicon-shopping-cart showTrash'></i>&nbsp; Корзина",
                    [
                        'class' => 'cart-button btn btn-ytos',
                        'onclick' => 'getCart(1)',
                    ]
                )?>
            </div>
        </div>
    </div>

    <?php

    NavBar::begin([
        'brandLabel' => 'Главное меню',
        //'brandUrl' => Yii::$app->homeUrl,
        'brandOptions' => ['class' => 'visible-xs'],
        'options' => [
            'class' => 'navbar-inverse',
            'id' => 'main-menu',
        ],
    ]);
    $menuItems = [
        ['label' => 'Главная', 'url' => ['/site/index']],
        ['label' => 'Каталог', 'url' => ['/site/catalog']],
        //['label' => 'Прайс', 'url' => ['/site/price']],
        //['label' => 'Прайс', 'url' => ['/site/price']],
        ['label' => 'О нас', 'url' => ['/info/index', 'id' => 1]],
        ['label' => 'Доставка и оплата', 'url' => ['/info/index', 'id' => 2]],
        ['label' => 'Контакты', 'url' => ['/info/index', 'id' => 3]],
    ];
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav'],
        'items' => $menuItems,
    ]);
    NavBar::end();
    ?>

</header>

<div class="container">
    <?= Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
    ]) ?>
    <?= Alert::widget() ?>
</div>