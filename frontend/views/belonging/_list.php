<?php
use yii\helpers\Html;
?>
<div class="image">
    <?=
    (isset($model->image->src)
        ? Html::a(
            Html::img('/image/catalog/Product/' . $model->id . '/thumb_' . $model->image->src),
            ['/product/index', 'product_id' => $model->id]

        )
        : Html::a(
            Html::img(Yii::getAlias('@mysite') . '/image/design/no-product-image.jpg'),
            ['/product/index', 'product_id' => $model->id]

        )
    )
    ?>
</div>
<div class="product-body">
    <div class="name"><?=$model->name?></div>
    <div class="info">
        <table class="table">
            <tbody>
            <tr>
                <td>Принадлежность: </td>
                <td><?=$model->belonging->name?></td>
            </tr>
            <tr>
                <td>Мин. заказ: </td>
                <td><?=$model->minimum?></td>
            </tr>
            <tr>
                <td>Вес: </td>
                <td><?=$model->weight?></td>
            </tr>
            <tr>
                <td>Цена: </td>
                <td><b class="text-danger price"><?=Yii::$app->my->conversion($model->price, $model->belonging_id, $model->weight)?></b></td>
            </tr>
            </tbody>
        </table>
    </div>
    <div class="buy">
        <?=Html::a(
            'Подробнее',
            ['/product/index', 'product_id' => $model->id],
            [
                'class' => 'btn btn-tos pull-left'
            ]

        )?>
        <?=($model->stock
            ? ($model->options
                ? Html::a(
                    "<i class=\"glyphicon glyphicon-shopping-cart\"></i>&nbsp;Купить",
                    ['/product/index', 'product_id' => $model->id],
                    [
                        'class' => 'btn btn-ytos pull-right',
                        'data-id' => $model->id,
                        'data-quantity' => $model->minimum,
                    ]
                )
                : Html::a(
                "<i class=\"glyphicon glyphicon-shopping-cart\"></i>&nbsp;Купить",
                    ['/cart/add', 'id' => $model->id, 'quantity' => $model->minimum],
                    [
                        'class' => 'btn btn-ytos add-into-cart pull-right',
                        'data-id' => $model->id,
                        'data-quantity' => $model->minimum,
                    ]
                )
            )
            : Html::button(
                "<i class=\"glyphicon glyphicon-shopping-cart\"></i>&nbsp;Нет в наличии",
                //['/cart/add', 'id' => $model->id, 'quantity' => $model->minimum],
                [
                    'class' => 'btn btn-ytos pull-right',
                    'data-id' => $model->id,
                    'data-quantity' => $model->minimum,
                    'disabled' => 1,
                ]
            )
        )?>
        <div class="clear"></div>
    </div>
</div>