<?php

use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

if (!$category){
    $this->title = $belonging->meta_t;
    $this->registerMetaTag([
        'name' => 'keywords',
        'content' => $belonging->meta_k,
    ]);
    $this->registerMetaTag([
        'name' => 'description',
        'content' => $belonging->meta_d,
    ]);
    $this->params['breadcrumbs'][] = ['label' => 'Каталог', 'url' => ['/site/catalog']];
    $this->params['breadcrumbs'][] = $this->title;
    $this->params['main-header'] = $belonging->meta_h1;
}else{
    $this->title = $category->meta_t;
    $this->registerMetaTag([
        'name' => 'keywords',
        'content' => $category->meta_k,
    ]);
    $this->registerMetaTag([
        'name' => 'description',
        'content' => $category->meta_d,
    ]);
    $this->params['breadcrumbs'][] = ['label' => 'Каталог', 'url' => ['/site/catalog']];
    $this->params['breadcrumbs'][] = ['label' => $belonging->name, 'url' => ['/belonging/index', 'belonging_id' => $belonging->id]];
    if ($parents){
        foreach ($parents as $parent) {
            $this->params['breadcrumbs'][] = ['label' => $parent['name'], 'url' => ['', 'belonging_id' => $belonging->id, 'category_id' => $parent['id']]];
        }
    }
    $this->params['breadcrumbs'][] = $this->title;
    $this->params['main-header'] = $category->meta_h1;
}
?>
<div class="belonging-index">
    <?php

    echo ListView::widget([
        'dataProvider' => $dataProvider,
        'itemView' => '_list',
        'itemOptions' => [
            'tag' => 'div',
            'class' => 'product',
        ],
        'options' => [
            'tag' => 'div',
            'class' => 'products',
        ],

        'layout' => "{items}\n{pager}",
        'summary' => 'Показано {count} из {totalCount}',
        'summaryOptions' => [
            'tag' => 'div',
            'class' => 'summary'
        ],
    ]);
    ?>

</div>
