<?php
use yii\helpers\Html;
?>

    <?php if (!empty($session['cart'])): ?>
    <div class="table-responsive cart-table">
        <table class="table table-stripped">
            <thead>
            <tr>
                <th>Фото</th>
                <th>Имя</th>
                <th width="130">Количество</th>
                <th>Цена</th>
                <th width="100">Сумма</th>
                <th width="100" class="text-center"><i class="glyphicon glyphicon-remove"></i></th>
            </tr>
            </thead>
            <tbody>
            <?php
            $cart = $session['cart'];
            uasort($cart, function($a, $b){
                return strcmp($a["unit_id"], $b["unit_id"]);
            });

            $unit = ['name' => '', 'id' => 0];
            $sum = [];
            $weight = [];
            $pack_price = '';

            foreach ($cart as $key => $item):
                if ($unit['name'] != $item['unit']){

                    if ($unit['name']){
                        Yii::$app->my->drawTotals($sum, $weight, $unit['id']);
                    }

                    $unit = ['name' => $item['unit'], 'id' => $item['unit_id']];
                    $weight[$item['unit_id']]['value'] = 0;
                    $sum[$item['unit_id']]['value'] = 0;
                    echo "<tr><td colspan='7' class='text-center text-upper'>{$unit['name']}</td></tr>";
                }

                $s = $sum[$item['unit_id']]['value'];
                $sum[$item['unit_id']]['value'] = $s + ($item['price'] * $item['quantity']);

                $w = $weight[$item['unit_id']]['value'];
                $weight[$item['unit_id']]['value'] = $w + ($item['quantity'] * $item['weight']);
            ?>
                <tr>
                    <td>
                        <?=Html::a(
                            Html::img(
                                '/image/catalog/Product/' . $item['id'] . '/thumb_' . $item['image'],
                                [
                                    'class' => 'img-thumbnail',
                                    'width' => 100
                                ]
                            ),
                            ['/product/index', 'product_id' => $item['id']],
                            [
                                'target' => '_blank',
                            ]
                        )?>
                    </td>
                    <td>
                        <?=Html::a($item['name'],['/product/index', 'product_id' => $item['id']],['target' => '_blank']);?>

                        <?php
                        if(is_array($item['options'])){
                            foreach ($item['options'] as $option):?>
                            <br><small><i><?=$option['product_option_name']?>: <b><?=$option['product_option_value_name']?></b></i></small>
                        <?php
                            endforeach;
                        }
                        ?>
                    </td>
                    <td>
                        <div class="btn-group">
                            <?=Html::a(
                                "<i class='glyphicon glyphicon-minus'></i>",
                                ['/cart/minus', 'key' => "{$key}"],
                                [
                                    'class' => 'btn btn-info btn-sm product-minus',
                                    'data-key' => "{$key}",
                                ]
                            )?>
                            <?=Html::button(
                                $item['quantity'],
                                [
                                    'class' => 'btn btn-default btn-sm product-quantity',
                                    'readonly' => 1
                                ]
                            )?>
                            <?=Html::a(
                                "<i class='glyphicon glyphicon-plus'></i>",
                                ['/cart/minus', 'key' => "{$key}"],
                                [
                                    'class' => 'btn btn-info btn-sm product-plus',
                                    'data-key' => "{$key}",
                                ]
                            )?>
                        </div>
                        &nbsp;<span><?=$item['unit']?></span>
                    </td>
                    <td><?=Yii::$app->my->conversion($item['price'])?></td>
                    <td><?=Yii::$app->my->conversion(($item['price']*$item['quantity']))?></td>
                    <td class="text-center"><i class="glyphicon glyphicon-remove text-danger del-product" data-key="<?=$key?>"></i></td>
                </tr>
            <?php endforeach; ?>
            <?php Yii::$app->my->drawTotals($sum, $weight, $unit['id']); ?>
            </tbody>
        </table>
    </div>
<?php else: ?>
    <h3 class="text-center">Корзина пуста</h3>
<?php endif; ?>
