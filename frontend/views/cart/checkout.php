<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Оформление заказа';
$this->params['breadcrumbs'][] = $this->title;
$this->params['main-header'] = 'Оформление заказа';
?>
<?php if ($session['cart']): ?>
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
            <div id="cart-checkout">
                <script>
                    $(document).ready(function () {
                        getCart(0);
                        $('.cart-button').attr("disabled","disabled");
                    })
                </script>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
            <?php if(Yii::$app->customer->isGuest):?>

                <p>Чтоб оформить заказ вы можете <?=Html::a('Войти', '#',['data-toggle' => "modal", 'data-target' => "#login-modal"])?> или зарегистрироваться, заполнив поля ниже:</p>
                <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>

                <?= $form->field($register, 'username')->textInput(['autofocus' => true]) ?>

                <?= $form->field($register, 'email')->input('email') ?>

                <?= $form->field($register, 'phone') ?>

                <?= $form->field($register, 'password')->passwordInput() ?>

                <div class="form-group">
                    <?= Html::submitButton('Зарегистрироваться', ['class' => 'btn btn-primary', 'name' => 'signup-button']) ?>
                </div>

                <?php ActiveForm::end(); ?>
            <?php else: ?>

                <?php $form = ActiveForm::begin(); ?>

                <?= $form->field($order, 'name')->textInput([
                    'maxlength' => true,
                    'required' => true,
                ])->label('Получатель') ?>

                <?= $form->field($order, 'phone')->textInput([
                    'maxlength' => true,
                    'required' => true,
                ]) ?>


                <?= $form->field($order, 'region')->textInput([
                    'maxlength' => true,
                ]) ?>

                <?= $form->field($order, 'city')->textInput([
                    'maxlength' => true,
                    'required' => true,
                ]) ?>

                <?= $form->field($order, 'address')->textarea([
                    'maxlength' => true,
                    'required' => true,
                ]) ?>

                <?= $form->field($order, 'comment')->textarea([
                    'maxlength' => true,
                ]) ?>

                <div class="form-group text-center">
                    <?= Html::submitButton('Оформить заказ', ['class' => 'btn-lg btn btn-success']) ?>
                </div>

                <?php ActiveForm::end(); ?>

            <?php endif; ?>

        </div>
    </div>

<?php endif; ?>
