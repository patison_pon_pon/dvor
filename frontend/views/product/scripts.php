<?php

use yii\helpers\Url;
//опции
$option_path = Url::to(["/product/option"]);
$script = <<< JS
    
    function showOption(product_id, product_option_id, product_option_value_id) {
    console.log(product_option_id);
    console.log(product_option_value_id);
    
        $('.option-select-'+ product_option_id).removeClass('has-error');
        $('.option-select-'+ product_option_id + ' .help-block').html('');
        $.ajax({
            url: '$option_path',
            cache: false,
            type: "GET",
            data: { 'product_id': product_id, 'product_option_value_id': product_option_value_id},
            success: function (data) {
                if (product_option_value_id == 0){
                    $("#option-container").html(data);
                }else{
                    console.log('.option-' + product_option_id);
                    $('.option-' + product_option_id).html(data);
                }
            }
        });
    };
JS;
$this->registerJs($script, yii\web\View::POS_HEAD);