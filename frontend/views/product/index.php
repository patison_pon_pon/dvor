<?php

use yii\helpers\Html;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $product  */
/* @var $parents  */

$this->title = $product->name;

$this->params['breadcrumbs'][] = ['label' => 'Каталог', 'url' => ['/site/catalog']];
$this->params['breadcrumbs'][] = ['label' => $product->belonging->name, 'url' => ['/belonging/index', 'belonging_id' => $product->belonging->id]];
if ($parents){
    foreach ($parents as $parent) {
        $this->params['breadcrumbs'][] = ['label' => $parent['name'], 'url' => ['/belonging/index', 'belonging_id' => $product->belonging->id, 'category_id' => $parent['id']]];
    }
}
$this->params['breadcrumbs'][] = ['label' => $product->category->name, 'url' => ['/belonging/index', 'belonging_id' => $product->belonging->id, 'category_id' => $product->category->id]];
$this->params['breadcrumbs'][] = $this->title;
$this->params['main-header'] = $product->name;
$this->params['belonging_id'] = $product->belonging->id;
include('scripts.php');
?>
<div class="product-index">
    <div class="row">
        <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">

            <ul id="gallery" class="list-group-flex-center">
                <?php $i = 0; foreach ($product->images as $image): ?>
                    <li>
                        <?php
                        echo Html::a(
                            Html::img(
                                Yii::getAlias('@image') . '/catalog/Product/' . $product->id . '/thumb_' . $image->src,
                                [
                                    'class' => 'img-thumbnail scheme-image',
                                    'width' => (!$i ? 400 : 100)
                                ]
                            ),
                            Yii::getAlias('@image') . '/catalog/Product/' . $product->id . '/' . $image->src,
                            [
                                'data-lightbox' => 'products',
                                'data-title' => '',
                                'data-desc' => '',
                            ]
                        );
                        ?>
                    </li>
                    <?php $i++; endforeach; ?>
            </ul>
        </div>
        <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
            <table class="table">
                <tbody>
                <tr>
                    <td>Поставщик: </td>
                    <td><?=$product->belonging->name?></td>
                </tr>
                <tr>
                    <td>Артикул: </td>
                    <td><?=$product->vendor_code?></td>
                </tr>
                <tr>
                    <td>Мин. заказ: </td>
                    <td><?=$product->minimum?></td>
                </tr>
                <tr>
                    <td>Вес: </td>
                    <td><?=$product->weight?></td>
                </tr>
                <tr>
                    <td>Наличие: </td>
                    <td><?=($product->stock ? '<span class="text-success">В наличии</span>' : '<span class="text-danger">Нет в наличии</span>')?></td>
                </tr>
                <tr>
                    <td colspan="2" class="price">Цена: <span><?=Yii::$app->my->conversion($product->price, $product->belonging_id, $product->weight)?></span></td>
                </tr>
                <?php if ($product->options): ?>
                <tr>
                    <td colspan="2">
                        <div id="option-container">
                            <script>showOption('<?=$product->id?>', '0', '0')</script>
                        </div>
                        <?php
                        /*foreach ($product->options AS $option){
                            var_dump($option->description);
                        }*/
                        ?>
                    </td>
                </tr>
                <?php endif; ?>
                <tr>
                    <td colspan="2" class="text-center">
                        <div class="btn-group">
                            <?=($product->stock
                                ? Html::input(
                                    'number',
                                    'spare-quantity',
                                    20,
                                    [
                                        'class' => 'btn btn-default product-quantity',
                                        'min' => $product->minimum,
                                    ]
                                )
                                . Html::a(
                                    "<i class=\"glyphicon glyphicon-shopping-cart\"></i>&nbsp;Купить",
                                    ['/cart/add', 'id' => $product->id],
                                    [
                                        'class' => 'btn btn-ytos add-into-cart',
                                        'data-id' => $product->id,
                                    ]
                                )
                                : Html::button(
                                    "<i class=\"glyphicon glyphicon-shopping-cart\"></i>&nbsp;Нет в наличии",
                                    [
                                        'class' => 'btn btn-ytos pull-right',
                                        'data-id' => $product->id,
                                        'data-quantity' => $product->minimum,
                                        'disabled' => 1,
                                    ]
                                )
                            )?>
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <?php if($product->description): ?>
                <h3 class="text-center">Описание</h3>
                <p>
                    <?=$product->description?>
                </p>
            <?php endif; ?>
        </div>
    </div>
</div>
