<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\catalog\Product */

$this->title = 'Личный кабинет';
$this->params['breadcrumbs'][] = ['label' => 'Личный кабинет', 'url' => ['/customer/index']];
$this->params['breadcrumbs'][] = 'Изменить пароль';
$this->params['main-header'] = 'Изменить пароль';
?>
<div class="product-view">

    <?php $form = ActiveForm::begin(); ?>


    <div class="form-group">
        <?= Html::label('Введите новый пароль', '', ['class' => 'control-label'])?>
        <?= Html::input('password', 'Customer[password]', '', ['class' => 'form-control', 'required' => 1]) ?>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
