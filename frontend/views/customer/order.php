<?php

use yii\helpers\Html;

$this->title = 'Личный кабинет';
$this->params['breadcrumbs'][] = ['label' => 'Личный кабинет', 'url' => ['/customer/index']];
$this->params['breadcrumbs'][] = ['label' => 'История заказов', 'url' => ['/customer/order-list']];
$this->params['breadcrumbs'][] = 'Заказ №' . $order->id;
$this->params['main-header'] = 'Заказ №' . $order->id;
?>

<!-- TAB NAVIGATION -->
<ul class="nav nav-tabs nav-justified" role="tablist">
    <li class="active"><a href="#tab1" role="tab" data-toggle="tab">Товары</a></li>
    <li><a href="#tab2" role="tab" data-toggle="tab">История заказа</a></li>
</ul>
<!-- TAB CONTENT -->
<div class="tab-content">
    <div class="active tab-pane fade in" id="tab1">
        <h2>Товары в заказе</h2>
        <div class="table-responsive">
        	<table class="table">
        		<thead>
        			<tr>
        				<th>Фото</th>
        				<th>Имя</th>
        				<th>Цена</th>
        				<th>Количество</th>
        				<th>Сумма</th>
        			</tr>
        		</thead>
        		<tbody>
                <?php foreach ($products as $product):?>
        			<tr>
        				<td>
                            <?=
                            Html::a(
                                Html::img(Yii::getAlias('@mysite') . '/image/catalog/Product/' . $product->product_id . '/thumb_' . $product->image->src, ['class' => 'img-thumbnail', 'width' => '100']),
                                ['/product/index', 'product_id' => $product->product_id]
                            )
                            ;?>
                        </td>
        				<td>
                            <?=$product->product->name?>

                            <?php foreach (unserialize($product->options) as $option){?>
                                <br><small><i><?=$option['product_option_name']?>: <b><?=$option['product_option_value_name']?></b></i></small>
                            <?php } ?>
                        </td>
        				<td><?=Yii::$app->my->nf($product->price) . '&nbsp;' . $order->currency->symbol?></td>
                        <td><?=$product->quantity?></td>
        				<td><?=Yii::$app->my->nf($product->sum) . '&nbsp;' . $order->currency->symbol?></td>
        			</tr>
                <?php endforeach; ?>
                    <tr>
                        <td colspan="4" class="text-right">Итого:</td>
                        <td><b><?=Yii::$app->my->nf($order->sum) . '&nbsp;' . $order->currency->symbol?></b></td>
                    </tr>
        		</tbody>
        	</table>
        </div>
    </div>
    <div class="tab-pane fade" id="tab2">
        <h2>История заказа</h2>
        <div class="table-responsive">
            <table class="table">
                <thead>
                <tr>
                    <th>Статус</th>
                    <th>Комментарий</th>
                    <th>Дата</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($histories as $history):?>
                    <tr>
                        <td><?=$history->status->name?></td>
                        <td><?=$history->comment?></td>
                        <td><?=Yii::$app->formatter->asDatetime($history->created_at)?></td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

