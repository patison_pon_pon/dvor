<?php
/**
 * Created by PhpStorm.
 * User: patison
 * Date: 04.12.17
 * Time: 18:19
 */

use yii\grid\GridView;
use yii\helpers\Html;

$this->title = 'Личный кабинет';
$this->params['breadcrumbs'][] = ['label' => 'Личный кабинет', 'url' => ['/customer/index']];
$this->params['breadcrumbs'][] = 'История заказов';
$this->params['main-header'] = 'История заказов';
?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [

        //'customer_id',
        [
            'attribute' => 'id',
            'label' => 'Заказ №',
            'format' => 'raw',
            'value' => function ($model) {
                return $model->id;
            },
        ],
        [
            'attribute' => 'supply_id',
            'label' => 'Поставка',
            'format' => 'raw',
            'value' => function ($model) {
                return $model->supply->name;
            },
        ],
        'sum',
        // 'currency',
        [
            'attribute' => 'order_status_id',
            'label' => 'Статус',
            'format' => 'raw',
            'value' => function ($model) {
                return $model->status->name;
            },
        ],
        [
            'attribute' => 'sum',
            'label' => 'Сумма',
            'format' => 'raw',
            'value' => function ($model) {
                return Yii::$app->my->nf($model->sum) . '&nbsp;' . $model->currency->symbol;
            },
        ],
        'created_at:datetime',
        [
            'class' => 'yii\grid\ActionColumn',
            'header'=>'Действия',
            'headerOptions' => ['width' => '90'],
            'template' => '{view}',
            'buttons' => [
                'view' => function($url, $model){
                    return Html::a('<span class="fa fa-eye"></span> Подробнее', ['order', 'order_id' => $model->id], [
                        'class' => 'btn btn-success btn-sm',
                        'title' => 'Подробнее',
                    ]);
                },
            ],
        ],
    ],
]); ?>
