<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\catalog\Product */

$this->title = 'Личный кабинет';
$this->params['breadcrumbs'][] = ['label' => 'Личный кабинет', 'url' => ['/customer/index']];
$this->params['breadcrumbs'][] = 'Личная информация';
$this->params['main-header'] = 'Личная информация';
?>
<div class="product-view">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'business')->dropDownList(\yii\helpers\ArrayHelper::map($groups, 'name', 'name'), ['prompt' => 'Выберите товарную группу']) ?>

    <?= $form->field($model, 'geo')->textarea(['rows' => '6']) ?>
    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
