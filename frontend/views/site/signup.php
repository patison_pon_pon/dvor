<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Регистрация покупателя';
$this->params['breadcrumbs'][] = $this->title;
$this->params['main-header'] = 'Регистрация покупателя';
?>
<div class="site-signup">


    <div class="row">
        <div class="sol-xs-12 col-sm-offset-3 col-sm-6">
            <p>Пожалуйста, заполните все поля:</p>
            <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>

                <?= $form->field($model, 'username')->textInput(['autocomplete' => "off"]) ?>

                <?= $form->field($model, 'email')->input('email', ['autocomplete' => "off"]) ?>

                <?= $form->field($model, 'phone')->textInput(['autocomplete' => "off"]) ?>

                <?= $form->field($model, 'manager_id')->dropDownList(\yii\helpers\ArrayHelper::map($managers, 'id', 'name'), ['prompt' => 'Выберите вашего личного менедера']) ?>

                <?= $form->field($model, 'business')->dropDownList(\yii\helpers\ArrayHelper::map($groups, 'name', 'name'), ['prompt' => 'Выберите товарную группу']) ?>

                <?= $form->field($model, 'geo')->textarea(['rows' => '6']) ?>

                <?= $form->field($model, 'password')->passwordInput() ?>

                <div class="form-group">
                    <?= Html::submitButton('Зарегистрироваться', ['class' => 'btn btn-tos', 'name' => 'signup-button']) ?>
                </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
