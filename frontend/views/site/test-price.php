<?php

use rmrevin\yii\fontawesome\FA;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\catalog\ProductSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Прайс товаров';
$this->params['breadcrumbs'][] = $this->title;
$this->params['main-header'] = 'Прайс товаров';
?>

<?php //echo $this->render('_search', ['model' => $searchModel]); ?>

<div class="product-index table-responsive">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'options' => [
            'id' => 'product-price'
        ],
        'columns' => [

            [
                'label' => 'Фото',
                'headerOptions' => ['class' => 'hidden'],
                'filterOptions' => ['class' => 'hidden'],
                'format' => 'raw',
                'value' => function ($model) {
                    return (isset($model->image->src)
                        ? Html::a(
                            Html::img(
                                Yii::getAlias('@mysite') . '/image/catalog/Product/' . $model->id . '/thumb_' . $model->image->src,
                                [
                                    'class' => 'img-thumbnail',
                                    'width' => '180'
                                ]
                            ),
                            ['/product/index', 'product_id' => $model->id]
                        )
                        : Html::a(
                            Html::img(
                                Yii::getAlias('@mysite') . '/image/design/no-product-image.jpg',
                                [
                                    'class' => 'img-thumbnail',
                                    'width' => '180'
                                ]
                            ),
                            ['/product/index', 'product_id' => $model->id]
                        )
                    );
                },
            ],

            [
                'attribute' => 'supply_id',
                'format' => 'raw',
                'headerOptions' => ['style' => 'width: 180px'],
                'contentOptions' => ['colspan' => '4'],
                'value' => function ($model) {
                    return "<div class='row'>"
                        . "<div class='col-xs-12'><h3 class='text-center'>{$model->name}</h3></div>"
                        . "<div class='col-xs-6'>"
                        . "Поставка: <b>" . $model->supply->name . "</b><br>"
                        . "Принадлежность: <b>" . $model->belonging->name . "</b><br>"
                        . "Категория: <b>" . $model->category->name . "</b>"
                        . "</div>"
                        . "<div class='col-xs-6'>"
                        . "Цена: <b class='text-danger'>" . Yii::$app->my->conversion($model->price, $model->supply_id, $model->weight) . "</b><br>"
                        . "Вес: <b class='text-warning'>" . Yii::$app->my->nf($model->weight) . " кг</b><br>"
                        . "Добавлен: <b class='text-primary'>" . Yii::$app->my->showDate($model->created_at) . "</b><br>"
                        . "</div>"
                        . "<div class='col-xs-12'><br>"
                        . Html::a(
                            'Подробнее',
                            ['/product/index', 'product_id' => $model->id],
                            ['class' => 'btn btn-tos']
                        )
                        . '&nbsp;&nbsp;'
                        .($model->stock
                            ? Html::a(
                                "<i class=\"glyphicon glyphicon-shopping-cart\"></i>&nbsp;Купить",
                                ['/cart/add', 'id' => $model->id, 'quantity' => $model->minimum],
                                [
                                    'class' => 'btn btn-ytos add-into-cart',
                                    'data-id' => $model->id,
                                    'data-quantity' => $model->minimum,
                                ]
                            )
                            : Html::button(
                                "<i class=\"glyphicon glyphicon-shopping-cart\"></i>&nbsp;Нет в наличии",
                                //['/cart/add', 'id' => $model->id, 'quantity' => $model->minimum],
                                [
                                    'class' => 'btn btn-ytos',
                                    'data-id' => $model->id,
                                    'data-quantity' => $model->minimum,
                                    'disabled' => 1,
                                ]
                            )
                        )
                        . "</div>"
                        . "</div>"
                    ;
                },
                'filter' => ArrayHelper::map($supplies, 'id', 'name'),
            ],
            [
                'attribute' => 'belonging_id',
                'value' => function ($model) {
                    return $model->belonging->name;
                },
                'filter' => ArrayHelper::map($belongings, 'id', 'name'),
                'contentOptions' => ['class' => 'hidden'],
            ],
            [
                'attribute' => 'category_id',
                'value' => function ($model) {
                    return $model->category->name;
                },
                'filter' => $categories,
                'contentOptions' => ['class' => 'hidden'],
            ],
            [
                'attribute' => 'name',
                'headerOptions' => ['class' => 'hidden'],
                'filterOptions' => ['class' => 'hidden'],
                'contentOptions' => ['class' => 'hidden'],
                'format' => 'raw',
                'value' => function ($model) {
                    return $model->name;
                },
            ],
            [
                'attribute' => 'price',
                'contentOptions' => ['class' => 'hidden'],
                'format' => 'raw',
                'value' => function ($model) {
                    return Yii::$app->my->conversion($model->price, $model->supply_id, $model->weight);
                },
                'filter' => '',
            ],
            // 'quantity',
            // 'minimum',
            // 'weight',
            // 'color:ntext',
            // 'size:ntext',
            [
                'attribute' => 'created_at',
                'headerOptions' => ['class' => 'hidden'],
                'filterOptions' => ['class' => 'hidden'],
                'contentOptions' => ['class' => 'hidden'],
                'label' => 'Добавлен',
                'format' => 'raw',
                'value' => function ($model) {
                    return Yii::$app->my->showDate($model->created_at);
                },
                'filter' => '',
            ],
        ],
    ]); ?>
</div>