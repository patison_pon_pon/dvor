<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Каталог';
$this->params['breadcrumbs'][] = $this->title;
$this->params['main-header'] = 'Выберите тип продукции';
?>
<div class="belongings">
    <?php foreach ($belongings as $belonging): ?>
    <div class="belonging">
        <?=Html::a(
            "<span>{$belonging->name}</span>",
            ['/belonging/index/', 'belonging_id' => $belonging->id]
        )?>
    </div>
    <?php endforeach; ?>
</div>
