<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\catalog\ProductSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Прайс товаров';
$this->params['breadcrumbs'][] = $this->title;
$this->params['main-header'] = 'Прайс товаров';
?>
<div class="product-index table-responsive">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [

            [
                'label' => 'Фото',
                'headerOptions' => ['width' => '200'],
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::a(
                        Html::img(
                            Yii::getAlias('@mysite') . '/image/catalog/Product/' . $model->id . '/thumb_' . $model->image->src,
                            ['class' => 'img-thumbnail']
                        ),
                        ['/product/index', 'product_id' => $model->id]
                    );
                },
            ],

            [
                'attribute' => 'supply_id',
                'format' => 'raw',/*
                'contentOptions' =>function ($model, $key, $index, $column){
                    return ['colspan' => '3'];
                },*/
                'value' => function ($model) {
                    return $model->supply->name;
                },
                'filter' => ArrayHelper::map($supplies, 'id', 'name'),
            ],
            [
                'attribute' => 'belonging_id',
                'value' => function ($model) {
                    return $model->belonging->name;
                },
                'filter' => ArrayHelper::map($belongings, 'id', 'name'),
            ],
            [
                'attribute' => 'category_id',
                'value' => function ($model) {
                    return $model->category->name;
                },
                'filter' => $categories,
            ],
            'name',
            // 'description:ntext',
            [
                'attribute' => 'price',
                'format' => 'raw',
                'value' => function ($model) {
                    return Yii::$app->my->conversion($model->price, $model->supply_id, $model->weight);
                },
                'filter' => '',
            ],
            // 'quantity',
            // 'minimum',
            // 'weight',
            // 'color:ntext',
            // 'size:ntext',
            [
                'attribute' => 'created_at',
                'label' => 'Добавлен',
                'format' => 'raw',
                'value' => function ($model) {
                    return Yii::$app->my->showDate($model->created_at);
                },
                'filter' => '',
            ],
        ],
    ]); ?>
</div>
<script>
    /*$(document).ready(function () {
        $('#w0 tbody tr td:eq(3)').remove();
        $('#w0 tbody tr td:eq(4)').remove();
    })*/
</script>