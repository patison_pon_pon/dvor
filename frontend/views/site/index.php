<?php

/* @var $this yii\web\View */

use frontend\widgets\module\ProductCarousel;

$this->title = 'DVOR';
?>
<div class="site-index">
    <?=ProductCarousel::widget(['carousel_id' => 2])?>
</div>
