<?php

namespace frontend\controllers;

use frontend\models\catalog\Belonging;
use frontend\models\catalog\Category;
use frontend\models\catalog\ProductSearch;
use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;

/**
 * BelongingController implements the CRUD actions for Belonging model.
 */
class BelongingController extends Controller
{
    public $parents;
    public $children;
    public $layout = 'category';
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Belonging models.
     * @return mixed
     */
    public function actionIndex($belonging_id, $category_id = 0, $sort = 'created_at'){
        $belonging = Belonging::findOne($belonging_id);
        $belongings = Belonging::find()->all();
        $searchModel = new ProductSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->where(['belonging_id' => $belonging_id, 'status' => '1']);
        $dataProvider->sort->enableMultiSort = true;
        $dataProvider->setSort([
            'defaultOrder' => [
                'stock' => SORT_DESC,
                $sort => SORT_DESC
            ]
        ]);
        $dataProvider->pagination->pageSize = 21;

        if ($category_id){
            $category = Category::findOne($category_id);

            if ($category->parent_id){
                $this->findCategoryParents($category->parent_id);
                $parents = array_reverse($this->parents);
            }else{
                $parents = false;
            }
            $this->findCategoryChildren($category_id);
            $query = "(`category_id` = '{$category_id}' AND `belonging_id` = '{$belonging_id}')";
            if ($this->children){
                foreach ($this->children as $child){
                    $query .= " OR (`category_id` = '" . $child['id'] . "' AND `belonging_id` = '{$belonging_id}')";
                }
            }

            $dataProvider->query->where($query);
        }else{
            $category = false;
            $parents = false;
        }

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'belonging' => $belonging,
            'belongings' => $belongings,
            'category' => $category,
            'parents' => $parents,
            'children' => $this->children,
        ]);
    }
    public function findCategoryParents($c){
        $category = Category::findOne($c);
        $this->parents[] = [
            'id' => $category->id,
            'name' => $category->name,
            'meta_h1' => $category->meta_h1,
            'meta_t' => $category->meta_t,
            'url' => $category->url,
        ];
        if ($category->parent_id)
            $this->findCategoryParents($category->parent_id);
    }
    public function findCategoryChildren($c){
        $child = Category::findOne(['parent_id' => $c]);
        if (!is_null($child)){
            $this->children[] = [
                'id' => $child->id,
                'name' => $child->name,
                'meta_h1' => $child->meta_h1,
                'meta_t' => $child->meta_t,
                'url' => $child->url,
            ];
            $this->findCategoryChildren($child->id);
        }
    }

}
