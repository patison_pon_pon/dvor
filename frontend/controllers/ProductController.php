<?php

namespace frontend\controllers;

use frontend\models\catalog\Category;
use frontend\models\catalog\Option;
use frontend\models\catalog\Product;
use frontend\models\catalog\ProductOption;
use frontend\models\catalog\ProductOptionValue;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\Controller;


class ProductController extends Controller
{
    public $parents;
    public $layout = 'product';

    public function actionIndex($product_id)
    {
        $product = Product::find()->where(['id' => $product_id])->with('category', 'belonging', 'images', 'options')->one();

        if ($product->category->parent_id){
            $this->findCategoryParents($product->category->parent_id);
            $parents = array_reverse($this->parents);
        }else{
            $parents = false;
        }

        return $this->render('index', [
            'product' => $product,
            'parents' => $parents,
        ]);
    }

    public function actionOption($product_id = 0, $product_option_value_id = 0)
    {

        if (Yii::$app->request->isAjax) {
            $product_id = $_GET['product_id'];
            $product_option_value_id = $_GET['product_option_value_id'];
        }

        $web = '';

        if($product_option_value_id){
            if (($product_option = ProductOption::find()->where(['product_id' => $product_id, 'parent_product_option_value_id' => $product_option_value_id])->with('description')->one()) != null){
                $web .= $this->actionDrawOption($product_option);
            }
        }else{
            if (($product_options = ProductOption::find()->where(['product_id' => $product_id, 'parent_product_option_value_id' => $product_option_value_id])->with('description')->orderBy(['sort' => SORT_ASC])->all()) != null){
                foreach ($product_options as $product_option) {
                    $web .= $this->actionDrawOption($product_option);
                    $web .= '<hr>';
                }
            }

        }
        return $web;
    }

    public function actionDrawOption($product_option)
    {
        $product_option_values = ProductOptionValue::find()->where(['product_option_id' => $product_option->id])->all();
        $web = '';
        $web .= "<div class='form-group option option-select-{$product_option->id}'>";
        $web .= Html::label($product_option->name, '', ['class' => 'control-label']);
        $web .= Html::dropDownList(
            $product_option->id,
            '',
            ArrayHelper::map($product_option_values, 'id', 'name'),
            [
                'class' => 'form-control',
                'data-parent_product_option_value_id' => $product_option->parent_product_option_value_id,
                'data-product_option_id' => $product_option->id,
                'data-option_name' => $product_option->name,
                'prompt' => 'Выберите '  . $product_option->name,
                'onchange' => "showOption({$product_option->product_id}, {$product_option->id}, $(this).val())",
            ]
        );
        $web .= '<div class="help-block"></div>';
        $web .= '</div>';
        $web .= "<div class='option-{$product_option->id}'>";
        $web .= '</div>';
        /*foreach ($product_option_values as $product_option_value) {
            if (($daughter_product_option = ProductOption::findOne(['product_id' => $product_option->product_id, 'parent_product_option_value_id' => $product_option_value->id])) !== null){
                $web .= $this->actionDrawOption($daughter_product_option);
            }
        }*/
        return $web;
    }

    public function findCategoryParents($c){
        $category = Category::findOne($c);
        $this->parents[] = [
            'id' => $category->id,
            'name' => $category->name,
            'meta_h1' => $category->meta_h1,
            'meta_t' => $category->meta_t,
            'url' => $category->url,
        ];
        if ($category->parent_id)
            $this->findCategoryParents($category->parent_id);
    }
}
