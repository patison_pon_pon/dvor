<?php

namespace frontend\controllers;

use frontend\models\catalog\Product;
use frontend\models\catalog\Category;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\filters\VerbFilter;

/**
 * CategoryController implements the CRUD actions for Category model.
 */
class CategoryController extends Controller
{
    public $layout = 'catalog';
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Category models.
     * @return mixed
     */
    public function actionView($category_id)
    {
        var_dump($_GET);
        $category = Category::findOne($category_id);
        $dataProvider = new ActiveDataProvider([
            'query' => Product::find()->where(['category_id' => $category_id])->andWhere(['status' => 1])
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'category' => $category,
        ]);
    }
}
