<?php

namespace frontend\controllers;

use frontend\models\catalog\Category;
use frontend\models\order\Order;
use frontend\models\order\OrderHistory;
use frontend\models\order\OrderProduct;
use Yii;
use frontend\models\shop\Customer;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CustomerController implements the CRUD actions for Customer model.
 */
class CustomerController extends Controller
{
    public $layout = 'customer';
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Customer models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Customer::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionOrderList()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Order::find()->where(['customer_id' => Yii::$app->customer->id])->orderBy(['created_at' => SORT_DESC]),
        ]);

        return $this->render('order-list', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionOrder($order_id)
    {
        $order = Order::findOne($order_id);
        if ($order->customer_id != Yii::$app->customer->id)
            return $this->goHome();
        $products = OrderProduct::find()->where(['order_id' => $order_id])->with('image')->all();
        $histories = OrderHistory::find()->where(['order_id' => $order_id])->orderBy(['created_at' => SORT_DESC])->all();

        return $this->render('order', [
            'order' => $order,
            'products' => $products,
            'histories' => $histories,
        ]);
    }

    public function actionPassword()
    {
        $model = $this->findModel(Yii::$app->customer->id);

        if ($model->load(Yii::$app->request->post())) {
            $model->password_hash = md5(Yii::$app->request->post('Customer')['password']);
            $model->save();
            $success = 'Изменения сохранены';
            Yii::$app->session->setFlash('success', $success);
            return $this->refresh();
        } else {
            return $this->render('password', [
                'model' => $model,
            ]);
        }
    }
    public function actionInfo()
    {
        $model = $this->findModel(Yii::$app->customer->id);
        $groups = Category::find()->where(['parent_id' => 0])->orWhere(['parent_id' => null])->orderBy(['name' => SORT_ASC])->all();

        if ($model->load(Yii::$app->request->post())) {
            if (!$model->save()){
                var_dump($model->errors);
                die();
            }else{
                $success = 'Изменения сохранены';
                Yii::$app->session->setFlash('success', $success);
            }
            return $this->refresh();
        } else {
            return $this->render('info', [
                'model' => $model,
                'groups' => $groups,
            ]);
        }
    }
    protected function findModel($id)
    {
        if (($model = Customer::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
