<?php
namespace frontend\controllers;

use backend\classes\SortList;
use common\models\CustomerLoginForm;
use frontend\models\catalog\Belonging;
use frontend\models\catalog\Category;
use frontend\models\catalog\ProductSearch;
use frontend\models\catalog\Supply;
use frontend\models\module\CallMe;
use frontend\models\shop\Currency;
use frontend\models\shop\Manager;
use frontend\models\shop\Shipping;
use frontend\models\SignupForm;
use Yii;
use yii\base\InvalidParamException;
use yii\helpers\ArrayHelper;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use yii\web\HttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * Site controller
 */
class SiteController extends Controller
{
    public $layout = 'main';
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->render('index');
    }


    public function actionLogin() {
        if (Yii::$app->request->isAjax) {
            $model = new CustomerLoginForm();
            if ($model->load(Yii::$app->request->post())) {
                if ($model->login()) {
                    $return_url = Yii::$app->request->referrer;

                    if($return_url!=NULL){
                        return $this->redirect($return_url);
                    }
                    else{
                        return $this->goBack();
                    }
                } else {
                    Yii::$app->response->format = Response::FORMAT_JSON;
                    return ActiveForm::validate($model);
                }
            }
        } else {
            throw new HttpException(404 ,'Page not found');
        }
    }

    public function actionQuit()
    {
        Yii::$app->customer->logout();

        return $this->goHome();
    }


    public function actionCatalog()
    {
        //$this->layout = 'catalog';
        $belongings = Belonging::find()->all();
        return $this->render('catalog',[
            'belongings' => $belongings
        ]);
    }

    public function actionPrice($sort = 'created_at')
    {
        $supplies = Supply::find()->all();
        $belongings = Belonging::find()->all();
        $categories = $this->getList();
        $searchModel = new ProductSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->sort->enableMultiSort = true;
        $dataProvider->setSort([
            'defaultOrder' => [
                'stock' => SORT_DESC,
                $sort => SORT_DESC
            ]
        ]);

        return $this->render('test-price', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'supplies' => $supplies,
            'belongings' => $belongings,
            'categories' => $categories,
        ]);
    }

    public static function getList()
    {
        $data = Category::find()
            ->select(['id', 'parent_id', 'name'])
            ->orderBy(['parent_id' => SORT_ASC, 'name' => SORT_ASC])
            ->asArray()
            ->all();

        $sort = new SortList([
            'data' => $data,
            'prefix' => '------',
        ]);
        $sortList = ArrayHelper::map($sort->getList(), 'id', 'name');
        return $sortList;
    }

    public function actionRegister()
    {
        if(!Yii::$app->customer->isGuest) {
            return $this->goHome();
        }

        $model = new SignupForm();
        $managers = Manager::find()->all();
        $groups = Category::find()->where(['parent_id' => 0])->orWhere(['parent_id' => null])->orderBy(['name' => SORT_ASC])->all();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user)) {
                    Yii::$app->myMail->MailAdminInform(
                        $user,
                        'new_customer_admin_inform',
                        'Регистрация нового пользователя'
                    );
                    /*Yii::$app->my->MailCustomer(
                        $user,
                        'new_customer_welcome',
                        'Регистрация на сайте TOS Опт'
                    );*/
                    return $this->goHome();
                }
            }
        }

        return $this->render('signup', [
            'model' => $model,
            'groups' => $groups,
            'managers' => $managers,
        ]);
    }


    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for the provided email address.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }

    public function actionChangeCurrency(){
        if (Yii::$app->request->isAjax) {
            $currency_id = $_GET['id'];
            $currency = Currency::findOne($currency_id);
            Yii::$app->my->start($currency->id);
            return Yii::$app->request->referrer
                ? $this->redirect(Yii::$app->request->referrer)
                : $this->goHome();
        }
    }

    public function actionChangeShipping(){
        if (Yii::$app->request->isAjax) {
            $shipping_id = $_GET['id'];
            $shipping = Shipping::findOne($shipping_id);
            Yii::$app->my->start(0, $shipping->id);
            return Yii::$app->request->referrer
                ? $this->redirect(Yii::$app->request->referrer)
                : $this->goHome();
        }
    }

    public function actionCallMe() {
        $model = new CallMe();
        if ($model->load(Yii::$app->request->post())) {
            $model->answered = 0;
            $model->created_at = date('U');

            if ($model->save()) {
                Yii::$app->session->setFlash('success', 'Спасибо! В ближайшее время с вами свяжется менеджер');

            } else {
                Yii::$app->session->setFlash('error', 'Упс... Произошла ошибка, попробуйте еще раз');
            }
            Yii::$app->myMail->MailAdminInform($model, 'call_me_admin_inform', 'Заявка на перезвон');

            $return_url = Yii::$app->request->referrer;
            if($return_url!=NULL){
                return $this->redirect($return_url);
            }
            else{
                return $this->goBack();
            }
        }
    }
}
