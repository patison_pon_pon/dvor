<?php

namespace frontend\controllers;

use frontend\models\catalog\Product;
use frontend\models\order\Order;
use frontend\models\order\OrderProduct;
use frontend\models\order\OrderHistory;
use frontend\models\order\OrderSum;
use Yii;
use yii\helpers\Url;
use yii\web\Controller;
use frontend\models\Cart;
use frontend\models\SignupForm;

class CartController extends Controller
{
    public function actionAdd(){
        $id = Yii::$app->request->get('id');
        $quantity = (int)Yii::$app->request->get('quantity');
        $quantity = !$quantity ? 1 : $quantity;
        $options = Yii::$app->request->get('options');

        $product = Product::findOne($id);
        if (empty($product)){
            return false;
        }else{
            $session = Yii::$app->session;
            $session->open();
            if ($quantity < $product->minimum)
                $quantity = $product->minimum;
            $cart = new Cart();
            $cart->addToCart($product, $quantity, $options);
            if (!Yii::$app->request->isAjax){
                return $this->redirect(Yii::$app->request->referrer);
            }
            return $this->renderPartial('cart-modal', compact('session'));
        }
    }

    public function actionMinus(){
        $key = Yii::$app->request->get('key');

        $session = Yii::$app->session;
        $session->open();

        $cart = new Cart();
        $cart->minus($key);

        return $this->renderPartial('cart-modal', compact('session'));
    }

    public function actionPlus(){
        $key = Yii::$app->request->get('key');

        $session = Yii::$app->session;
        $session->open();

        $cart = new Cart();
        $cart->plus($key);

        return $this->renderPartial('cart-modal', compact('session'));
    }

    public function actionClear(){
        $session = Yii::$app->session;
        $session->open();
        $session->remove('cart');
        $session->remove('cart.quantity');
        $session->remove('cart.sum');
        $session->remove('cart.weight');
        return $this->renderPartial('cart-modal', compact('session'));
    }

    public function actionDelProduct(){
        $key = Yii::$app->request->get('key');
        $session = Yii::$app->session;
        $session->open();
        $cart = new Cart();
        $cart->recalculate($key);
        return $this->renderPartial('cart-modal', compact('session'));
    }

    public function actionShow(){
        $session = Yii::$app->session;
        $session->open();
        return $this->renderPartial('cart-modal', compact('session'));
    }

    public function actionCheckout(){
        $session = Yii::$app->session;
        $session->open();
        $order = new Order();

        $register = new SignupForm();
        if ($register->load(Yii::$app->request->post())) {
            if ($user = $register->signup()) {
                if (Yii::$app->getUser()->login($user)) {
                    return $this->refresh();
                }
            }
        }

        if ($order->load(Yii::$app->request->post())){
            $products = $session['cart'];
            uasort($products, function($a, $b){
                return strcmp($a["unit_id"], $b["unit_id"]);
            });
            $order_id = 0;
            $unit = 0;
            $q = 0;
            $s = 0;
            $w = 0;
            foreach ($products as $key => $product){
                if ($product['unit_id'] != $unit){
                    if ($order_id){
                        $this->updateSum($order_id, $unit, $q, $s, $w);
                        $q = 0;
                        $s = 0;
                        $w = 0;
                    }
                    $unit = $product['unit_id'];
                    $order_id = $this->saveOrder($unit);
                }
                $q += $product['quantity'];
                $s += ($product['price'] * $product['quantity']);
                $w += ($product['weight'] * $product['quantity']);
                if ($order_id)
                    $this->saveOrderProduct($product, $order_id);
            }
            $this->updateSum($order_id, $unit, $q, $s, $w);
            $session->remove('cart');
            $session->remove('cart.quantity');
            $session->remove('cart.sum');
            return $this->redirect(Url::to(['/cart/success', 'order_id' => $order_id]));
        }
        return $this->render('checkout', [
            'session' => $session,
            'order' => $order,
            'register' => $register,
        ]);
    }

    protected function updateSum($order_id, $unit_id, $q, $s, $w){

        $pack = Yii::$app->my->puckPrice($w, $unit_id);
        if (isset($pack['price']) && $pack['price'])
            $pack_price = Yii::$app->my->conversion(($pack['price']), 0, 0, 0);
        else
            $pack_price = 0;

        $order = Order::findOne($order_id);
        $order->quantity = $q;
        $order->weight = $w;
        $order->sum = (Yii::$app->my->conversion($s, 0, 0) + $pack_price);
        $order->save();

        $total = new OrderSum();
        $total->order_id = $order_id;
        $total->action = 'plus';
        $total->description = 'Сумма';
        $total->value = Yii::$app->my->conversion($s, 0, 0);
        $total->created_at = date('U');
        if (!$total->save()){
            var_dump($total->errors);
        }
        unset($total);

        if ($pack_price){
            $total = new OrderSum();
            $total->order_id = $order_id;
            $total->action = 'plus';
            $total->description = $pack['name'] . "(x" . $pack['quantity'] . ")";
            $total->value = $pack_price;
            $total->created_at = date('U');
            if (!$total->save()){
                var_dump($total->errors);
            }
        }

        $history = new OrderHistory();
        $history->order_id = $order_id;
        $history->order_status_id = Yii::$app->my->settings()['default_order_status_id'];
        $history->comment = 'Заказ оформлен';
        $history->created_at = date('U');
        $history->save();
    }

    protected function saveOrder($unit_id){
        $order = new Order();
        $order->load(Yii::$app->request->post());
        $order->customer_id = Yii::$app->customer->id;
        $order->unit_id = $unit_id;
        $order->quantity = 0;
        $order->sum = 0;
        $order->currency_id = Yii::$app->session->get('currency');
        $order->order_status_id = Yii::$app->my->settings()['default_order_status_id'];
        $order->created_at = date('U');
        if(!$order->save()){
            var_dump($order->errors);
            return $order->primaryKey;
        }else{
            return $order->primaryKey;
        }
    }

    protected function saveOrderProduct($product, $order_id){
        $order_product = new OrderProduct();
        $order_product->order_id = $order_id;
        $order_product->product_id = $product['id'];
        $order_product->price = Yii::$app->my->conversion($product['price'], 0, 0);
        $order_product->quantity = $product['quantity'];
        $order_product->sum = Yii::$app->my->conversion(($product['price'] * $product['quantity']), 0, 0);
        $order_product->options = serialize($product['options']);
        if (!$order_product->save()){
            var_dump($order_product->errors);
        }
    }

    public function actionSuccess($order_id){
        if (isset($order_id)){
            return $this->render('success');
        }else{
            return $this->redirect(Url::to('/site'));
        }
    }
}