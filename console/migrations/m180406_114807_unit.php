<?php

use yii\db\Migration;

/**
 * Class m180406_114807_unit
 */
class m180406_114807_unit extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('unit', [
            'id' => $this->primaryKey(),
            'name' => $this->string('255')->notNull(),
            'additional_product_id' => $this->integer('11')->null()->defaultValue('0'),
        ]);

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m180406_114807_unit cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180406_114807_unit cannot be reverted.\n";

        return false;
    }
    */
}
