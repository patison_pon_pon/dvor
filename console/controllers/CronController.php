<?php

namespace console\controllers;

use backend\models\catalog\Product;
use Yii;
use yii\console\Controller;

class CronController extends Controller
{
    public function actionOffProductFromStockOdessa(){
        $term = time() - (3600*24*14);
        var_dump($term);
        $products = Product::find()->where("created_at < '$term'")->andWhere(['belonging_id' => 3])->all();
        foreach ($products as $product) {
            $product->status = 0;
            $product->save();
        }
    }
}
